package com.github.snarechops.renew.common

import org.bukkit.block.Block

enum class Axis {
    X,
    Y,
    Z,
}

fun Block.getRelative(axis: Axis): Block = when (axis) {
    Axis.X -> this.getRelative(-1, 0, 0)
    Axis.Y -> this.getRelative(0, -1, 0)
    Axis.Z -> this.getRelative(0, 0, -1)
}

fun Block.planeScan(
    direction: Direction,
    axis: Axis,
    width: Int,
    distance: Int,
    until: (Block) -> Boolean
): List<Block> {
    val results = mutableListOf<Block>()
    var next: Block? = this
    var dist = width
    while (true) {
        if (next == null) break
        if (dist == 0) break
        results.addAll(next.linearScan(direction, distance, until))
        next = next.getRelative(axis)
        dist--
    }
    return results
}
