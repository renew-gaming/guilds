package com.github.snarechops.renew.common

import com.github.snarechops.renew.Renew
import com.github.snarechops.renew.features.commands.CommandFeature
import com.github.snarechops.renew.features.commands.extractHelp
import org.bukkit.command.CommandExecutor
import org.bukkit.command.TabCompleter

fun CommandExecutor.register(name: String, completion: TabCompleter) =
    Renew.getCommand(name)?.let {
        it.setExecutor(this)
        it.tabCompleter = completion
        this.extractHelp()?.let { help -> CommandFeature.pages.add(help) }
    }
