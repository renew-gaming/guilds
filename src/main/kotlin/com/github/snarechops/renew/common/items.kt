package com.github.snarechops.renew.common

import com.github.snarechops.renew.features.crates.name
import org.bukkit.Bukkit
import org.bukkit.Material
import org.bukkit.entity.Player
import org.bukkit.inventory.Inventory
import org.bukkit.inventory.ItemStack
import org.bukkit.util.io.BukkitObjectInputStream
import org.bukkit.util.io.BukkitObjectOutputStream
import org.yaml.snakeyaml.external.biz.base64Coder.Base64Coder
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream

private fun outputStream(fn: (BukkitObjectOutputStream) -> Unit): String {
    return try{
        val stream = ByteArrayOutputStream()
        val output = BukkitObjectOutputStream(stream)
        fn(output)
        output.close()
        Base64Coder.encodeLines(stream.toByteArray())
    } catch(e: Exception) {
        throw IllegalStateException("Unable serialize items", e)
    }
}

fun newInventory(items: String, size: Int, title: String): Inventory {
    val inventory = Bukkit.createInventory(null, size, title)
    items.decodeItemStacks().forEach { inventory.addItem(it) }
    return inventory
}

fun Air() =
    ItemStack(Material.AIR)

fun String.decodeItemStacks(): List<ItemStack> =
    if (this.isBlank()) listOf()
    else {
        val stream = ByteArrayInputStream(Base64Coder.decodeLines(this))
        val input = BukkitObjectInputStream(stream)
        val items = Array(input.readInt()) { input.readObject() as ItemStack? }
        input.close()
        items.filterNotNull()
    }

fun ItemStack.encodeString(): String =
    outputStream { stream -> stream.writeObject(this) }
fun String.decodeItemStack(): ItemStack? =
    if (this.isBlank()) null
    else {
        val stream = ByteArrayInputStream(Base64Coder.decodeLines(this))
        val input = BukkitObjectInputStream(stream)
        val item = input.readObject() as ItemStack?
        input.close()
        item
    }

fun List<ItemStack>.encodeString(): String = outputStream { stream ->
    stream.writeInt(this.size)
    this.forEach { stream.writeObject(it) }
}

fun Array<ItemStack>.encodeString(): String = this.toList().encodeString()

fun ItemStack.give(player: Player): Boolean =
    player.inventory.addItem(this).let {
        println("Giving ${this.name} to ${player.name}")
        if (it.isEmpty()) ok(player, RenewTag, green("Received"), *this.name.colorize())
        else {
            player.world.dropItem(player.location, this)
            ok(player, RenewTag, *this.name.colorize(), yellow("dropped at your feet"))
        }
    }

fun CustomItem(material: Material, name: String = "", lore: List<String> = listOf(), amount: Int = 1): ItemStack {
    val item = ItemStack(material, amount)
    item.itemMeta = item.itemMeta?.let { meta ->
        if (name != "") meta.setDisplayName(name)
        if (lore.isNotEmpty()) {
            val l = mutableListOf<String>()
            l.addAll(lore)
            meta.lore = l
        }
        meta
    }
    return item
}