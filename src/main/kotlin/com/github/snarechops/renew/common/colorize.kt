package com.github.snarechops.renew.common

import net.md_5.bungee.api.ChatColor
import net.md_5.bungee.api.chat.TextComponent

sealed class Format {
    class Color(val value: String): Format()
    object Obfuscated : Format()
    object Bold : Format()
    object Strikethrough : Format()
    object Underline : Format()
    object Italic : Format()
    object Reset : Format()
}

private data class Part(var format: List<Format>, var text: String)
private data class Model(var isSpecial: Boolean, var formatComplete: Boolean, var parts: List<Part>)
private fun initModel() = Model(isSpecial = false, formatComplete = false, parts = listOf(Part(listOf(), "")))

fun colored(color: String, text: String) = TextComponent("$text ").let { it.color = ChatColor.of(color); it }
fun black(text: String) = colored("#000000", text)
fun darkBlue(text: String) = colored("#0000aa", text)
fun darkGreen(text: String) = colored("#00aa00", text)
fun darkAqua(text: String) = colored("#00aaaa", text)
fun darkRed(text: String) = colored("#aa0000", text)
fun darkPurple(text: String) = colored("#aa00aa", text)
fun gold(text: String) = colored("#ffaa00", text)
fun gray(text: String) = colored("#aaaaaa", text)
fun darkGray(text: String) = colored("#555555", text)
fun blue(text: String) = colored("#5555ff", text)
fun green(text: String) = colored("#55ff55", text)
fun aqua(text: String) = colored("#55ffff", text)
fun red(text: String) = colored("#ff5555", text)
fun lightPurple(text: String) = colored("#ff55ff",text)
fun yellow(text: String) = colored("#ffff55", text)
fun white(text: String) = colored("#ffffff", text)
fun obfuscated(text: String) = TextComponent(text).let { it.isObfuscated = true; it }
fun bold(text: String) = TextComponent(text).let { it.isBold = true; it }
fun strike(text: String) = TextComponent(text).let { it.isStrikethrough = true; it }
fun underline(text: String) = TextComponent(text).let { it.isUnderlined = true; it }
fun italic(text: String) = TextComponent(text).let { it.isItalic = true; it }
fun reset(text: String) = TextComponent(text)
fun pluginTag(text: String): TextComponent {
    val start = gray("[")
    val middle = lightPurple(text)
    val end = gray("] ")
    start.addExtra(middle)
    middle.addExtra(end)
    return start
}

fun String.colorize() =
    render(parse(this)).toTypedArray()

private fun parse(message: String): Model =
    initModel().let {
        message.toList().forEach { char ->
            if (it.isSpecial) {
                it.parts = addFormatToLastPart(formatForChar(char), it.parts)
                it.isSpecial = false
                return@forEach
            }
            if (char == '&' || char == '§') {
                if (it.formatComplete) {
                    it.parts += Part(listOf(), "")
                    it.formatComplete = false
                }
                it.isSpecial = true
                return@forEach
            }
            it.formatComplete = true
            it.parts = addCharToLastPart(char, it.parts)
        }
        it
    }

private fun addFormatToLastPart(format: Format, parts: List<Part>): List<Part> =
    parts.mapIndexed { index, part -> if (index == parts.lastIndex) addFormatToPart(format, part) else part }

private fun addFormatToPart(format: Format, part: Part): Part =
    part.format.let { part.format += format; part}

private fun addCharToLastPart(char: Char, parts: List<Part>): List<Part> =
    parts.mapIndexed { index, part -> if (index == parts.lastIndex) addCharToPart(char, part) else part }

private fun addCharToPart(char: Char, part: Part): Part =
    part.text.let { part.text += char; part }

private fun formatForChar(char: Char): Format =
    when (char) {
        '0' -> Format.Color("#000000")
        '1' -> Format.Color("#0000aa")
        '2' -> Format.Color("#00aa00")
        '3' -> Format.Color("#00aaaa")
        '4' -> Format.Color("#aa0000")
        '5' -> Format.Color("#aa00aa")
        '6' -> Format.Color("#ffaa00")
        '7' -> Format.Color("#aaaaaa")
        '8' -> Format.Color("#555555")
        '9' -> Format.Color("#5555ff")
        'a' -> Format.Color("#55ff55")
        'b' -> Format.Color("#55ffff")
        'c' -> Format.Color("#ff5555")
        'd' -> Format.Color("#ff55ff")
        'e' -> Format.Color("#ffff55")
        'f' -> Format.Color("#ffffff")
        'k' -> Format.Obfuscated
        'l' -> Format.Bold
        'm' -> Format.Strikethrough
        'n' -> Format.Underline
        'o' -> Format.Italic
        else -> Format.Reset
    }

private fun render(model: Model): List<TextComponent> =
    model.parts.map { component(it) }

private fun component(part: Part): TextComponent {
    var text = TextComponent(part.text)
    part.format.forEach {
        when (it) {
            is Format.Color -> text.color = ChatColor.of(it.value)
            is Format.Obfuscated -> text.isObfuscated = true
            is Format.Bold -> text.isBold = true
            is Format.Strikethrough -> text.isStrikethrough = true
            is Format.Underline -> text.isUnderlined = true
            is Format.Italic -> text.isItalic = true
            is Format.Reset -> text = TextComponent(part.text)
        }
    }
    return text
}
