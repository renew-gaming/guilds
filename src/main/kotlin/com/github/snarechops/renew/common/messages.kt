package com.github.snarechops.renew.common

import net.md_5.bungee.api.chat.BaseComponent
import org.bukkit.Bukkit
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player

val RenewTag = pluginTag("Renew")

fun err(sender: CommandSender, tag: BaseComponent, message: String): Boolean =
    err(sender, tag, red(message))
fun err(sender: CommandSender, vararg message: BaseComponent): Boolean =
    err { sender.spigot().sendMessage(*message) }
private fun err(f: () -> Unit): Boolean {
    f()
    return false
}

fun ok(sender: CommandSender, tag: BaseComponent, message: String) =
    ok(sender, tag, green(message))
fun ok(sender: CommandSender, vararg message: BaseComponent): Boolean =
    ok { sender.spigot().sendMessage(*message) }
private fun ok(f: () -> Unit): Boolean {
    f()
    return true
}

fun broadcast(message: String) =
    broadcast(RenewTag, green(message))
fun broadcast(vararg message: BaseComponent) =
    Bukkit.spigot().broadcast(*message)

fun CommandSender.message(vararg components: BaseComponent) {
    this.spigot().sendMessage(*components)
}

@Suppress("FunctionName")
fun NotAllowed(sender: CommandSender, tag: BaseComponent): Boolean =
    err(sender, tag, "Insufficient Permission")

@Suppress("FunctionName")
fun MustBePlayer(sender: CommandSender, tag: BaseComponent): Boolean =
    err(sender, tag, "Must be a player to perform this command")

@Suppress("FunctionName")
fun PlayerNotOnline(sender: CommandSender, tag: BaseComponent): Boolean =
    err(sender, tag, "Player is not online")

@Suppress("FunctionName")
fun CannotCreateSign(sender: Player, tag: BaseComponent, name: String) =
    sender.spigot().sendMessage(tag, red("Cannot create"), gold(name), red("signs"))

@Suppress("FunctionName")
fun CannotDestroySign(sender: Player, tag: BaseComponent, name: String) =
    sender.spigot().sendMessage(tag, red("Cannot destroy"), gold(name), red("signs"))

@Suppress("FunctionName")
fun MissingSubCommand(sender: CommandSender, tag: BaseComponent) =
    err(sender, tag, "Missing sub command")

@Suppress("FunctionName")
fun InvalidCommand(sender: CommandSender, tag: BaseComponent) =
    err(sender, tag, "Missing sub command")

@Suppress("FunctionName")
fun NoSpace(sender: CommandSender, tag: BaseComponent) =
    err(sender, tag, "Not enough space in inventory")