package com.github.snarechops.renew.common

import java.time.Instant
import java.time.temporal.ChronoUnit

const val black: String = "§0"
const val darkBlue: String = "§1"
const val darkGreen: String = "§2"
const val darkAqua: String = "§3"
const val darkRed: String = "§4"
const val darkPurple: String = "§5"
const val gold: String = "§6"
const val gray: String = "§7"
const val darkGray: String = "§8"
const val blue: String = "§9"
const val green: String = "§a"
const val aqua: String = "§b"
const val red: String = "§c"
const val lightPurple: String = "§d"
const val yellow: String = "§e"
const val white: String = "§f"
const val obfuscated: String = "§k"
const val bold: String = "§l"
const val strike: String = "§m"
const val underline: String = "§n"
const val italic: String = "§o"
const val reset: String = "§r"

fun formatExpires(expires: Instant): String{
    if(expires.isBefore(Instant.now())) return "$red(Expired)"
    val difference = ChronoUnit.HOURS.between(Instant.now(), expires).toInt()
    val color = when {
        difference > 4 -> green
        difference > 2 -> darkGray
        else -> red
    }
    if (difference < 1) return "$color(Expires in less than an hour)"
    return "$color(Expires in $difference hours)"
}
