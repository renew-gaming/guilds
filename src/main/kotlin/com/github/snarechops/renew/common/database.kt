package com.github.snarechops.renew.common

import com.mongodb.client.MongoClient
import org.litote.kmongo.KMongo

lateinit var Database: MongoClient

fun connectDatabase(): Boolean =
    try { Database = KMongo.createClient(Config.database); true }
    catch (e: Exception) { println(e.message); false }
