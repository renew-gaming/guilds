package com.github.snarechops.renew.common

import org.bukkit.block.Block

fun Block.linearScan(direction: Direction, distance: Int, until: (Block) -> Boolean): List<Block>{
    val results = mutableListOf<Block>()
    var next: Block? = this
    var dist = distance
    while(true) {
        if (next == null) break
        if (dist == 0) break
        if (until(next)) break
        results.add(next)
        next = next.getRelative(direction.x, direction.y, direction.z)
        dist--
    }
    return results
}

