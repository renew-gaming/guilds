package com.github.snarechops.renew.common

import org.bukkit.block.Block

fun Block.above(): Block = this.getRelative(0, 1, 0)

fun Block.floodScan(limit: Int, condition: (block: Block) -> Boolean): List<Block> {
    val result = mutableListOf<Block>()
    val queue = mutableListOf<Block>()
    queue.add(this)
    while (queue.isNotEmpty() && result.size < limit) {
        val block = queue.first()
        queue.removeAt(0)
        if (!result.contains(block) && condition(block)) {
            result.add(block)
            queue.add(block.getRelative(1, 0, 0))
            queue.add(block.getRelative(0, 0, 1))
            queue.add(block.getRelative(-1, 0, 0))
            queue.add(block.getRelative(0, 0, -1))
        }
    }
    return result
}