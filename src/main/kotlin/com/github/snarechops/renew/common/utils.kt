package com.github.snarechops.renew.common

import org.bukkit.Bukkit
import org.bukkit.Material
import org.bukkit.block.Block
import org.bukkit.inventory.ItemStack

fun nop(f: () -> Unit): Unit = f()

fun commandAsConsole(command: String) = Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), command)

fun centered(text: String): String {
    val len = text.length - ((text.count { c -> c == '§' }) * 2)
    if (len >= 19) return text
    val padding = (19 - len) / 2
    return text.padStart(padding, ' ') + "\n"
}

fun isSword(material: Material): Boolean = when(material){
    Material.WOODEN_SWORD -> true
    Material.GOLDEN_SWORD -> true
    Material.DIAMOND_SWORD -> true
    Material.NETHERITE_SWORD -> true
    else -> false
}

fun String.toProperCase(): String = this[0].uppercaseChar() + this.substring(1).lowercase()

fun String.pagify(): List<String>{
    var start = 0
    val page = mutableListOf<String>()

    while(start < this.length){
        var end = start+19
        if (end >= this.length) end = this.length-1
        var line = this.slice(start..end)
        if(line.contains('\n')) {
            line = line.slice(0..line.indexOf('\n'))
        }
        page += line
        start +=line.length
    }
    val pages = mutableListOf<String>()
    while(page.size > 0){
        val end = if(page.size >= 14) 14 else page.size
        pages += page.slice(0 until end).joinToString("")
        for(i in 0 until end){
            page.removeAt(0)
        }
    }

    return pages.toList()
}

fun Block.isSign(): Boolean = listOf(
    Material.ACACIA_SIGN,
    Material.ACACIA_WALL_SIGN,
    Material.BIRCH_SIGN,
    Material.BIRCH_WALL_SIGN,
    Material.CRIMSON_SIGN,
    Material.CRIMSON_WALL_SIGN,
    Material.DARK_OAK_SIGN,
    Material.DARK_OAK_WALL_SIGN,
    Material.JUNGLE_SIGN,
    Material.JUNGLE_WALL_SIGN,
    Material.OAK_SIGN,
    Material.OAK_WALL_SIGN,
    Material.SPRUCE_SIGN,
    Material.SPRUCE_WALL_SIGN,
    Material.WARPED_SIGN,
    Material.WARPED_WALL_SIGN,
).contains(this.type)

fun ItemStack.isAir(): Boolean = this.type == Material.AIR || this.type == Material.CAVE_AIR

fun ItemStack.isHelmet(): Boolean = listOf(
    Material.LEATHER_HELMET,
    Material.CHAINMAIL_HELMET,
    Material.GOLDEN_HELMET,
    Material.IRON_HELMET,
    Material.DIAMOND_HELMET,
    Material.TURTLE_HELMET,
    Material.NETHERITE_HELMET,
).contains(this.type)

fun ItemStack.isChestplate(): Boolean = listOf(
    Material.LEATHER_CHESTPLATE,
    Material.CHAINMAIL_CHESTPLATE,
    Material.GOLDEN_CHESTPLATE,
    Material.IRON_CHESTPLATE,
    Material.DIAMOND_CHESTPLATE,
    Material.NETHERITE_CHESTPLATE,
).contains(this.type)

fun ItemStack.isLeggings(): Boolean = listOf(
    Material.LEATHER_LEGGINGS,
    Material.CHAINMAIL_LEGGINGS,
    Material.GOLDEN_LEGGINGS,
    Material.IRON_LEGGINGS,
    Material.DIAMOND_LEGGINGS,
    Material.NETHERITE_LEGGINGS,
).contains(this.type)

fun ItemStack.isBoots(): Boolean = listOf(
    Material.LEATHER_BOOTS,
    Material.CHAINMAIL_BOOTS,
    Material.GOLDEN_BOOTS,
    Material.IRON_BOOTS,
    Material.DIAMOND_BOOTS,
    Material.NETHERITE_BOOTS,
).contains(this.type)

fun ItemStack.isSword(): Boolean = listOf(
    Material.WOODEN_SWORD,
    Material.STONE_SWORD,
    Material.GOLDEN_SWORD,
    Material.IRON_SWORD,
    Material.DIAMOND_SWORD,
    Material.NETHERITE_SWORD,
).contains(this.type)

fun ItemStack.isPickaxe(): Boolean = listOf(
    Material.WOODEN_PICKAXE,
    Material.STONE_PICKAXE,
    Material.GOLDEN_PICKAXE,
    Material.IRON_PICKAXE,
    Material.DIAMOND_PICKAXE,
    Material.NETHERITE_PICKAXE,
).contains(this.type)

fun ItemStack.isHoe(): Boolean = listOf(
    Material.WOODEN_HOE,
    Material.STONE_HOE,
    Material.GOLDEN_HOE,
    Material.IRON_HOE,
    Material.DIAMOND_HOE,
    Material.NETHERITE_HOE,
).contains(this.type)

fun ItemStack.isShovel(): Boolean = listOf(
    Material.WOODEN_SHOVEL,
    Material.STONE_SHOVEL,
    Material.GOLDEN_SHOVEL,
    Material.IRON_SHOVEL,
    Material.DIAMOND_SHOVEL,
    Material.NETHERITE_SHOVEL,
).contains(this.type)