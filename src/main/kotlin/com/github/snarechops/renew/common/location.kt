package com.github.snarechops.renew.common

import kotlinx.serialization.Serializable
import org.bukkit.Bukkit
import org.bukkit.Location

@Serializable
data class SimpleLocation(val world: String, val x: Int, val y: Int, val z: Int)
fun SimpleLocation.toLocation(): Location =
    Location(Bukkit.getWorld(this.world), this.x.toDouble(), this.y.toDouble(), this.z.toDouble())
fun Location.toSimpleLocation(): SimpleLocation =
    SimpleLocation(this.world?.name ?: "world", this.blockX, this.blockY, this.blockZ)

@Serializable
data class PreciseLocation(val world: String, val x: Double, val y: Double, val z: Double)
fun PreciseLocation.toLocation(): Location =
    Location(Bukkit.getWorld(this.world), this.x, this.y, this.z)
fun Location.toPreciseLocation(): PreciseLocation =
    PreciseLocation(this.world?.name ?: "world", this.x, this.y, this.z)

@Serializable
data class FullLocation(val world: String, val x: Double, val y: Double, val z: Double, val yaw: Float, val pitch: Float)
fun FullLocation.toLocation(): Location =
    Location(Bukkit.getWorld(this.world), this.x, this.y, this.z, this.yaw, this.pitch)
fun Location.toFullLocation(): FullLocation =
    FullLocation(this.world?.name ?: "world", this.x, this.y, this.z, this.yaw, this.pitch)
