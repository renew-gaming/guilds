package com.github.snarechops.renew.common

import com.github.snarechops.renew.Renew
import org.bukkit.Bukkit

fun debug(message: String) =
    if (Config.debug) println(message) else Unit

fun delay(ticks: Long, task: () -> Unit) =
    Bukkit.getScheduler().runTaskLater(Renew, Runnable { task() }, ticks)

fun nextTick(task: () -> Unit) =
    delay(1, task)

fun repeatingTask(delay: Long, period: Long, task: () -> Unit) =
    Bukkit.getScheduler().runTaskTimer(Renew, Runnable { task() }, delay, period)

fun async(task: () -> Unit) =
    Bukkit.getScheduler().runTaskAsynchronously(Renew, Runnable { task() })

fun async(task: () -> Unit, await: () -> Unit) =
    Bukkit.getScheduler().runTaskAsynchronously(Renew, Runnable { task(); run { await() } })