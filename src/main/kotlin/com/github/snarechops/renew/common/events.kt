package com.github.snarechops.renew.common

import com.github.snarechops.renew.Renew
import org.bukkit.Bukkit
import org.bukkit.event.Event
import org.bukkit.event.HandlerList
import org.bukkit.event.Listener

abstract class CustomEvent : Event() {
    override fun getHandlers(): HandlerList {
        return Companion.handlers
    }

    companion object {
        private val handlers = HandlerList()

        @JvmStatic
        fun getHandlerList(): HandlerList {
            return handlers
        }
    }
}

fun emit(event: Event) = Bukkit.getServer().pluginManager.callEvent(event)

fun Listener.register(description: String = "$this") {
    debug("Registering Event Listeners for $description")
    Bukkit.getServer().pluginManager.registerEvents(this, Renew)
}
