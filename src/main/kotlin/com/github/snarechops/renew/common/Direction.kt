package com.github.snarechops.renew.common

import org.bukkit.Location
import org.bukkit.block.BlockFace

sealed class Direction(val x: Int, val y: Int, val z: Int) {
    class NORTH : Direction(0, 0, -1)
    class NORTH_UP : Direction(0, 1, -1)
    class NORTH_DOWN : Direction(0, -1, -1)
    class NORTH_EAST : Direction(1, 0, -1)
    class NORTH_EAST_UP : Direction(1, 1, -1)
    class NORTH_EAST_DOWN : Direction(1, -1, -1)
    class NORTH_WEST : Direction(-1, 0, -1)
    class NORTH_WEST_UP : Direction(-1, 1, -1)
    class NORTH_WEST_DOWN : Direction(-1, -1, -1)
    class SOUTH : Direction(0, 0, 1)
    class SOUTH_UP : Direction(0, 1, 1)
    class SOUTH_DOWN : Direction(0, -1, 1)
    class SOUTH_EAST : Direction(1, 0, 1)
    class SOUTH_EAST_UP : Direction(1, 1, 1)
    class SOUTH_EAST_DOWN : Direction(1, -1, 1)
    class SOUTH_WEST : Direction(-1, 0, 1)
    class SOUTH_WEST_UP : Direction(-1, 1, 1)
    class SOUTH_WEST_DOWN : Direction(-1, -1, 1)
    class EAST : Direction(1, 0, 0)
    class EAST_UP : Direction(1, 1, 0)
    class EAST_DOWN : Direction(1, -1, 0)
    class WEST : Direction(-1, 0, 0)
    class WEST_UP : Direction(-1, 1, 0)
    class WEST_DOWN : Direction(-1, -1, 0)
    class UP : Direction(0, 1, 0)
    class DOWN : Direction(0, -1, 0)
    class NONE : Direction(0, 0, 0)
}

fun BlockFace.toDirection(): Direction = when (this) {
    BlockFace.NORTH -> Direction.NORTH()
    BlockFace.NORTH_NORTH_EAST -> Direction.NORTH()
    BlockFace.NORTH_NORTH_WEST -> Direction.NORTH()
    BlockFace.NORTH_EAST -> Direction.NORTH_EAST()
    BlockFace.NORTH_WEST -> Direction.NORTH_WEST()
    BlockFace.SOUTH -> Direction.SOUTH()
    BlockFace.SOUTH_SOUTH_EAST -> Direction.SOUTH()
    BlockFace.SOUTH_SOUTH_WEST -> Direction.SOUTH()
    BlockFace.SOUTH_EAST -> Direction.SOUTH_EAST()
    BlockFace.SOUTH_WEST -> Direction.SOUTH_WEST()
    BlockFace.EAST -> Direction.EAST()
    BlockFace.EAST_NORTH_EAST -> Direction.EAST()
    BlockFace.EAST_SOUTH_EAST -> Direction.EAST()
    BlockFace.WEST -> Direction.WEST()
    BlockFace.WEST_NORTH_WEST -> Direction.WEST()
    BlockFace.WEST_SOUTH_WEST -> Direction.WEST()
    BlockFace.UP -> Direction.UP()
    BlockFace.DOWN -> Direction.DOWN()
    BlockFace.SELF -> Direction.NONE()
}

fun Location.isFacingUp() = this.pitch < -45
fun Location.isFacingDown() = this.pitch > 45
fun Location.isFacingNorth() = this.yaw > 112.5 || this.yaw < -112.5
fun Location.isFacingSouth() = this.yaw < 67.5 && this.yaw > -67.5
fun Location.isFacingEast() = this.yaw <=-22.5 && this.yaw >= -157.5
fun Location.isFacingWest() = this.yaw in 22.5..157.5

fun Location.toDirection(): Direction = when {
    this.isFacingNorth() && this.isFacingEast() && this.isFacingUp() -> Direction.NORTH_EAST_UP()
    this.isFacingNorth() && this.isFacingEast() && this.isFacingDown() -> Direction.NORTH_EAST_DOWN()
    this.isFacingNorth() && this.isFacingEast() -> Direction.NORTH_EAST()
    this.isFacingNorth() && this.isFacingWest() && this.isFacingUp() -> Direction.NORTH_WEST_UP()
    this.isFacingNorth() && this.isFacingWest() && this.isFacingDown() -> Direction.NORTH_WEST_DOWN()
    this.isFacingNorth() && this.isFacingWest() -> Direction.NORTH_EAST()
    this.isFacingNorth() && this.isFacingUp() -> Direction.NORTH_UP()
    this.isFacingNorth() && this.isFacingDown() -> Direction.NORTH_DOWN()
    this.isFacingNorth() -> Direction.NORTH()
    this.isFacingSouth() && this.isFacingEast() && this.isFacingUp() -> Direction.SOUTH_EAST_UP()
    this.isFacingSouth() && this.isFacingEast() && this.isFacingDown() -> Direction.SOUTH_EAST_DOWN()
    this.isFacingSouth() && this.isFacingEast() -> Direction.SOUTH_EAST()
    this.isFacingSouth() && this.isFacingWest() && this.isFacingUp() -> Direction.SOUTH_WEST_UP()
    this.isFacingSouth() && this.isFacingWest() && this.isFacingDown() -> Direction.SOUTH_WEST_DOWN()
    this.isFacingSouth() && this.isFacingWest() -> Direction.SOUTH_WEST()
    this.isFacingSouth() && this.isFacingUp() -> Direction.SOUTH_UP()
    this.isFacingSouth() && this.isFacingDown() -> Direction.SOUTH_DOWN()
    this.isFacingSouth() -> Direction.SOUTH()
    this.isFacingEast() && this.isFacingUp() -> Direction.EAST_UP()
    this.isFacingEast() && this.isFacingDown() -> Direction.EAST_DOWN()
    this.isFacingEast() -> Direction.EAST()
    this.isFacingWest() && this.isFacingUp() -> Direction.WEST_UP()
    this.isFacingWest() && this.isFacingDown() -> Direction.WEST_DOWN()
    this.isFacingWest() -> Direction.WEST()
    this.isFacingUp() -> Direction.UP()
    this.isFacingDown() -> Direction.DOWN()
    else -> Direction.NONE()
}

fun Location.toHorizontalDirection(): Direction = when {
    this.isFacingNorth() && this.isFacingEast() -> Direction.NORTH_EAST()
    this.isFacingNorth() && this.isFacingWest() -> Direction.NORTH_WEST()
    this.isFacingNorth() -> Direction.NORTH()
    this.isFacingSouth() && this.isFacingEast() -> Direction.SOUTH_EAST()
    this.isFacingSouth() && this.isFacingWest() -> Direction.SOUTH_WEST()
    this.isFacingSouth() -> Direction.SOUTH()
    this.isFacingEast() -> Direction.EAST()
    this.isFacingWest() -> Direction.WEST()
    else -> Direction.NONE()
}

fun Location.toSimpleDirection(): Direction = when {
    this.pitch < -60 -> Direction.UP()
    this.pitch > 60 -> Direction.DOWN()
    else -> this.toHorizontalDirection()
}

fun Direction.left45(): Direction = when(this) {
    is Direction.NORTH -> Direction.NORTH_WEST()
    is Direction.NORTH_WEST -> Direction.WEST()
    is Direction.WEST -> Direction.SOUTH_WEST()
    is Direction.SOUTH_WEST -> Direction.SOUTH()
    is Direction.SOUTH -> Direction.SOUTH_EAST()
    is Direction.SOUTH_EAST -> Direction.EAST()
    is Direction.EAST -> Direction.NORTH_EAST()
    is Direction.NORTH_EAST -> Direction.NORTH()

    is Direction.NORTH_UP -> Direction.NORTH_WEST_UP()
    is Direction.NORTH_WEST_UP -> Direction.WEST_UP()
    is Direction.WEST_UP -> Direction.SOUTH_WEST_UP()
    is Direction.SOUTH_WEST_UP -> Direction.SOUTH_UP()
    is Direction.SOUTH_UP -> Direction.SOUTH_EAST_UP()
    is Direction.SOUTH_EAST_UP -> Direction.EAST_UP()
    is Direction.EAST_UP -> Direction.NORTH_EAST_UP()
    is Direction.NORTH_EAST_UP -> Direction.NORTH_UP()

    is Direction.NORTH_DOWN -> Direction.NORTH_WEST_DOWN()
    is Direction.NORTH_WEST_DOWN -> Direction.WEST_DOWN()
    is Direction.WEST_DOWN -> Direction.SOUTH_WEST_DOWN()
    is Direction.SOUTH_WEST_DOWN -> Direction.SOUTH_DOWN()
    is Direction.SOUTH_DOWN -> Direction.SOUTH_EAST_DOWN()
    is Direction.SOUTH_EAST_DOWN -> Direction.EAST_DOWN()
    is Direction.EAST_DOWN -> Direction.NORTH_EAST_DOWN()
    is Direction.NORTH_EAST_DOWN -> Direction.NORTH_DOWN()

    else -> this
}

fun Direction.right45(): Direction = when (this) {
    is Direction.NORTH -> Direction.NORTH_EAST()
    is Direction.NORTH_EAST -> Direction.EAST()
    is Direction.EAST -> Direction.SOUTH_EAST()
    is Direction.SOUTH_EAST -> Direction.SOUTH()
    is Direction.SOUTH -> Direction.SOUTH_WEST()
    is Direction.SOUTH_WEST -> Direction.WEST()
    is Direction.WEST -> Direction.NORTH_WEST()
    is Direction.NORTH_WEST -> Direction.NORTH()

    is Direction.NORTH_UP -> Direction.NORTH_EAST_UP()
    is Direction.NORTH_EAST_UP -> Direction.EAST_UP()
    is Direction.EAST_UP -> Direction.SOUTH_EAST_UP()
    is Direction.SOUTH_EAST_UP -> Direction.SOUTH_UP()
    is Direction.SOUTH_UP -> Direction.SOUTH_WEST_UP()
    is Direction.SOUTH_WEST_UP -> Direction.WEST_UP()
    is Direction.WEST_UP -> Direction.NORTH_WEST_UP()
    is Direction.NORTH_WEST_UP -> Direction.NORTH_UP()

    is Direction.NORTH_DOWN -> Direction.NORTH_EAST_DOWN()
    is Direction.NORTH_EAST_DOWN -> Direction.EAST_DOWN()
    is Direction.EAST_DOWN -> Direction.SOUTH_EAST_DOWN()
    is Direction.SOUTH_EAST_DOWN -> Direction.SOUTH_DOWN()
    is Direction.SOUTH_DOWN -> Direction.SOUTH_WEST_DOWN()
    is Direction.SOUTH_WEST_DOWN -> Direction.WEST_DOWN()
    is Direction.WEST_DOWN -> Direction.NORTH_WEST_DOWN()
    is Direction.NORTH_WEST_DOWN -> Direction.NORTH_DOWN()

    else -> this
}

fun Direction.left90(): Direction = this.left45().left45()
fun Direction.right90(): Direction = this.right45().right45()
