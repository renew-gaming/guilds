package com.github.snarechops.renew.common

import com.charleskorn.kaml.Yaml
import com.github.snarechops.renew.PLUGIN_FOLDER
import com.github.snarechops.renew.Renew
import kotlinx.serialization.Serializable
import kotlinx.serialization.decodeFromString
import java.io.File

lateinit var Config: ConfigSchema

@Serializable
data class ConfigSchema(val debug: Boolean, val database: String, val api: String, val cult: CultConfig)

@Serializable
data class CultConfig(val min: SimpleLocation, val max: SimpleLocation)

fun fetchConfig(): Boolean {
    Renew.saveDefaultConfig()
    val file = File("$PLUGIN_FOLDER/config.yml")
    if (!file.exists() || !file.isFile) return false
    Config = Yaml.default.decodeFromString(file.readText())
    return true
}