package com.github.snarechops.renew.common

import org.bukkit.Material
import org.bukkit.inventory.ItemStack

fun NoPageButton(): ItemStack =
    CustomItem(Material.RED_STAINED_GLASS_PANE, "${red}Page End")

fun PageButton(page: Int): ItemStack =
    CustomItem(Material.LIME_STAINED_GLASS_PANE, "${gold}Page $page")

fun BackButton(): ItemStack =
    CustomItem(Material.RED_STAINED_GLASS_PANE, "${red}Back")