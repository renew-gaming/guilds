package com.github.snarechops.renew.features.cultshop

import com.github.snarechops.renew.Feature
import com.github.snarechops.renew.common.*
import com.github.snarechops.renew.features.enchantments.*
import com.github.snarechops.renew.features.upgradetokens.CustomUpgradeToken
import org.bukkit.Bukkit
import org.bukkit.block.Sign
import org.bukkit.entity.Player
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.block.BlockBreakEvent
import org.bukkit.event.block.SignChangeEvent
import org.bukkit.event.inventory.InventoryClickEvent
import org.bukkit.event.player.PlayerInteractEvent
import org.bukkit.inventory.ItemStack

const val CULT_SHOP_SIGN_TITLE = "[Cult Shop]"
const val CULT_SHOP_NAME = "${darkRed}Cult Shop"
const val CultAdminPermission = "cult.admin"
val CultShopTag = pluginTag("Cult Shop")

fun Token(enchant: CustomEnchant): (amount: Int) -> ItemStack =
    { amount -> CustomUpgradeToken(enchant).let { it.amount = amount; it } }

object CultShopFeature : Feature, Listener {
    private val stock = mapOf(
        "Cultist Helm" to ShopItem(::CultistHelm, 1, 100, 576),
        "Cultist Chest" to ShopItem(::CultistChest, 1, 101, 576),
        "Cultist Legs" to ShopItem(::CultistLegs, 1, 102, 576),
        "Cultist Boots" to ShopItem(::CultistBoots, 1, 103, 576),
        "Cultist Wings" to ShopItem(::CultistWings, 1, 104, 576),
        "Cultist Sword" to ShopItem(::CultistSword, 1, 105, 576),
        "Blood Armor Upgrade" to ShopItem(Token(BloodArmorEnchant), 1, 106, 576),
        "Blood Boost Upgrade" to ShopItem(Token(BloodBoostEnchant), 1, 107, 576),
        "Blood Lust Upgrade" to ShopItem(Token(BloodLustEnchant), 1, 108, 576),
        "Blood Regen Upgrade" to ShopItem(Token(BloodRegenEnchant), 1, 109, 576),
        "Cleansing Upgrade" to ShopItem(Token(CleansingEnchant), 1, 110, 576),
    )

    override fun onInit() {
        CultCommand.register("cult", CultCompletion)
        this.register()
    }

    override fun onDestroy() {}

    @EventHandler(ignoreCancelled = true)
    fun onSignInteract(event: PlayerInteractEvent) {
        val block = event.clickedBlock ?: return
        if (!block.isSign()) return
        val sign = block.state as Sign
        if (sign.getLine(0) != CULT_SHOP_SIGN_TITLE) return
        val inventory = Bukkit.createInventory(event.player, 54, CULT_SHOP_NAME)
        stock.values.forEach { inventory.addItem(it.displayItem()) }
        event.player.openInventory(inventory)
    }

    @EventHandler(ignoreCancelled = true)
    fun onPreventSignBreak(event: BlockBreakEvent){
        if (!event.block.isSign()) return
        val sign = event.block.state as Sign
        if (sign.getLine(0) != CULT_SHOP_SIGN_TITLE) return
        if (event.player.hasPermission(CultAdminPermission)) return
        event.isCancelled = true
    }

    @EventHandler(ignoreCancelled = true)
    fun onPreventSignCreate(event: SignChangeEvent){
        if (event.getLine(0) != CULT_SHOP_SIGN_TITLE) return
        if (event.player.hasPermission(CultAdminPermission)) return
        event.isCancelled = true
    }

    @EventHandler(ignoreCancelled = true)
    fun onInventoryClick(event: InventoryClickEvent) {
        if (event.view.title != CULT_SHOP_NAME) return
        event.isCancelled = true
        if (event.isLeftClick) {
            stock.values.find {
                event.currentItem?.itemMeta?.lore?.contains("${obfuscated}${it.id}") == true
            }?.let {
                purchase(event.whoClicked as Player, it.ctor(it.amount), it.cost)
            }
        }

    }

    private fun purchase(player: Player, item: ItemStack, cost: Int): Boolean {
        if (!player.inventory.containsAtLeast(CultistCoin(1), cost))
            return err(player, CultShopTag, "Insufficient coins for purchase")
        player.inventory.removeItem(CultistCoin(cost))
        player.inventory.addItem(item)
        return ok(player, CultShopTag, green("Successfully purchased"), *(item.itemMeta?.displayName ?: "").colorize())
    }
}