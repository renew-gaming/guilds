package com.github.snarechops.renew.features.guilds

import com.github.snarechops.renew.common.*
import com.github.snarechops.renew.common.NotAllowed
import com.github.snarechops.renew.common.err
import com.github.snarechops.renew.common.ok
import com.github.snarechops.renew.features.commands.Help
import com.github.snarechops.renew.features.commands.HelpTopic
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player

val GuildsTag = pluginTag("Guilds")

@HelpTopic("Guilds")
object GuildCommand : CommandExecutor {
    override fun onCommand(sender: CommandSender, command: Command, label: String, args: Array<out String>): Boolean =
        when {
            args.getOrNull(0) == "set" -> set(sender, args.drop(1))
            GuildNames.any { it.equals(args.getOrNull(0), ignoreCase = true) } -> teleport(sender, args[0])
            else -> err(sender, GuildsTag, "Missing guild name")
        }

    @Help(GuildsAdminPermission, "$darkBlue/guild set <name> $black- Sets the teleport location of the provided guild to your current location")
    private fun set(sender: CommandSender, args: List<String>): Boolean {
        if (sender !is Player) return NotAllowed(sender, GuildsTag)
        if (!sender.hasPermission(GuildsAdminPermission)) return NotAllowed(sender, GuildsTag)
        if (args.isEmpty()) return err(sender, GuildsTag, "Missing guild name")
        val guild = GuildFeature.getOne(args[0].uppercase()) ?: return err(sender, GuildsTag, "Invalid guild name")
        guild.location = LocationFromBukkit(sender.location)
        GuildFeature.save(guild)
        return ok(sender, GuildsTag, gold(guild.name), green("teleport location set"))
    }

    @Help("", "$darkBlue/guild <name> $black- Teleports you to the provided guild")
    private fun teleport(sender: CommandSender, name: String): Boolean {
        if (sender !is Player) return err(sender, GuildsTag, "Must be a player to teleport")
        val guild = GuildFeature.getOne(name.uppercase()) ?: return err(sender, GuildsTag, "Invalid Guild Name")
        if (!sender.hasPermission(GuildsTeleportBypassPermission) && !sender.hasPermission(GuildsTeleportPermission(guild.id))) {
            return NotAllowed(sender, GuildsTag)
        }
        val location = guild.location ?: return err(sender, GuildsTag, gold(name), red("has no assigned teleport location."))
        sender.teleport(location.toBukkitLocation())
        return true
    }
}