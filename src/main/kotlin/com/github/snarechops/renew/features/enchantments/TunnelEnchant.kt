package com.github.snarechops.renew.features.enchantments

import com.github.snarechops.renew.common.*
import org.bukkit.GameMode
import org.bukkit.Material
import org.bukkit.block.Block
import org.bukkit.entity.Player
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.block.BlockBreakEvent
import org.bukkit.inventory.ItemStack

object TunnelEnchant : Listener, CustomEnchant(
    "tunnel",
    "Tunnel",
    1,
    3,
    blue
) {
    var ignore = setOf<Player>()
    fun amount(level: Int): Int = 3 * level

    override fun description(level: Int): List<String> = listOf(
        "$gold- Mines a 3x3 tunnel ${amount(level)} blocks deep",
        "$gold- in the direction you're facing ${gray}Max: $max"
    )

    override fun canEnchantItem(item: ItemStack): Boolean = item.isPickaxe()

    @EventHandler(ignoreCancelled = true)
    fun onBlockBreak(event: BlockBreakEvent) {
        if (ignore.contains(event.player)) return
        if (event.player.gameMode != GameMode.SURVIVAL) return
        if (event.player.isSneaking) return
        if (!event.block.isTunnelable()) return
        val tool = event.player.inventory.itemInMainHand
        val level = has(tool)
        if (level == 0) return
        val location = event.player.location
        val direction = location.toSimpleDirection()
        val starts = startingBlocks(event.block, direction)
        val blocks = starts.map { it.linearScan(direction, amount(level)) { x -> !x.isTunnelable() } }.flatten()
        mine(event.player, blocks, tool)
    }

    private fun mine(player: Player, blocks: List<Block>, tool: ItemStack) {
        val speed = 6
        if (blocks.isEmpty()) return
        ignore = ignore + player
        blocks.subList(0, if (blocks.size < speed) blocks.size else speed).forEach {
            val event = BlockBreakEvent(it, player)
            emit(event)
            if (!event.isCancelled) it.breakNaturally(tool)
        }
        ignore = ignore - player
        nextTick { mine(player, blocks.drop(speed), tool) }
    }

    fun startingBlocks(root: Block, direction: Direction): List<Block> = when (direction) {
        is Direction.NORTH -> root.NSStart()
        is Direction.SOUTH -> root.NSStart()
        is Direction.NORTH_WEST -> root.NWStart()
        is Direction.SOUTH_WEST -> root.SWStart()
        is Direction.NORTH_EAST -> root.NEStart()
        is Direction.SOUTH_EAST -> root.SEStart()
        is Direction.EAST -> root.EWStart()
        is Direction.WEST -> root.EWStart()
        is Direction.UP -> root.UDStart()
        is Direction.DOWN -> root.UDStart()
        else -> listOf(root)
    }

    private fun Block.NSStart() = listOf(
        this.getRelative(-1, 1, 0),
        this.getRelative(-1, 0, 0),
        this.getRelative(-1, -1, 0),
        this.getRelative(0, 1, 0),
        this,
        this.getRelative(0, -1, 0),
        this.getRelative(1, 1, 0),
        this.getRelative(1, 0, 0),
        this.getRelative(1, -1, 0),
    )

    private fun Block.EWStart() = listOf(
        this.getRelative(0, 1, -1),
        this.getRelative(0, 0, -1),
        this.getRelative(0, -1, -1),
        this.getRelative(0, 1, 0),
        this,
        this.getRelative(0, -1, 0),
        this.getRelative(0, 1, 1),
        this.getRelative(0, 0, 1),
        this.getRelative(0, -1, 1),
    )

    private fun Block.NWSWStart() = listOf(
        this.getRelative(-1, 1, 1),
        this.getRelative(-1, 0, 1),
        this.getRelative(-1, -1, 1),
        this.getRelative(0, 1, 0),
        this,
        this.getRelative(0, -1, 0),
        this.getRelative(1, 1, -1),
        this.getRelative(1, 0, -1),
        this.getRelative(1, -1, -1),
    )

    private fun Block.NWStart() = NWSWStart().plus(
        listOf(
            this.getRelative(1, 1, 0),
            this.getRelative(1, 0, 0),
            this.getRelative(1, -1, 0),
            this.getRelative(0, 1, -1),
            this.getRelative(0, 0, -1),
            this.getRelative(0, -1, -1),
        )
    )

    private fun Block.SWStart() = NWSWStart().plus(
        listOf(
            this.getRelative(0, 1, 1),
            this.getRelative(0, 0, 1),
            this.getRelative(0, -1, 1),
            this.getRelative(-1, 1, 0),
            this.getRelative(-1, 0, 0),
            this.getRelative(-1, -1, 0),
        )
    )

    private fun Block.NESEStart() = listOf(
        this.getRelative(-1, 1, -1),
        this.getRelative(-1, 0, -1),
        this.getRelative(-1, -1, -1),
        this.getRelative(0, 1, 0),
        this,
        this.getRelative(0, -1, 0),
        this.getRelative(1, 1, 1),
        this.getRelative(1, 0, 1),
        this.getRelative(1, -1, 1),
    )

    private fun Block.NEStart() = NESEStart().plus(
        listOf(
            this.getRelative(0, 1, -1),
            this.getRelative(0, 0, -1),
            this.getRelative(0, -1, -1),
            this.getRelative(1, 1, 0),
            this.getRelative(1, 0, 0),
            this.getRelative(1, -1, 0),
        )
    )

    private fun Block.SEStart() = NESEStart().plus(listOf(
        this.getRelative(1, 1,0),
        this.getRelative(1,0,0),
        this.getRelative(1,-1,0),
        this.getRelative(0,1,1),
        this.getRelative(0,0,1),
        this.getRelative(0,-1,1),
    ))

    private fun Block.UDStart() = listOf(
        this.getRelative(1, 0, -1),
        this.getRelative(0, 0, -1),
        this.getRelative(-1, 0, -1),
        this.getRelative(1, 0, 0),
        this,
        this.getRelative(-1, 0, 0),
        this.getRelative(1, 0, 1),
        this.getRelative(0, 0, 1),
        this.getRelative(-1, 0, 1),
    )

    private fun Block.isTunnelable(): Boolean = listOf(
        Material.STONE,
        Material.GRANITE,
        Material.DIORITE,
        Material.ANDESITE,
        Material.DEEPSLATE,
        Material.CALCITE,
        Material.TUFF,
        Material.DRIPSTONE_BLOCK,
        Material.AMETHYST_BLOCK,
        Material.SANDSTONE,
        Material.NETHERRACK,
        Material.BASALT,
        Material.END_STONE,
        Material.BLACKSTONE,
    ).contains(this.type)
}

