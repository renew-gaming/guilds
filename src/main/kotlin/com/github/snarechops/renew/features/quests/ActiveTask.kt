package com.github.snarechops.renew.features.quests

import com.github.snarechops.renew.common.*
import org.bukkit.GameMode
import org.bukkit.Material
import org.bukkit.entity.Item
import org.bukkit.entity.Player
import org.bukkit.event.EventHandler
import org.bukkit.event.EventPriority
import org.bukkit.event.HandlerList
import org.bukkit.event.Listener
import org.bukkit.event.block.BlockBreakEvent
import org.bukkit.event.block.BlockPlaceEvent
import org.bukkit.event.entity.EntityDeathEvent
import org.bukkit.event.inventory.CraftItemEvent
import org.bukkit.event.player.PlayerFishEvent
import org.bukkit.event.player.PlayerShearEntityEvent

class ActiveTask(
    val quest: ActiveQuest,
    val action: Action,
    val item: Material,
    val total: Int,
    var progress: Int,
) : Listener {
    val complete: Boolean
        get() = progress >= total
    var notified = false

    constructor(quest: ActiveQuest, task: PlayerTask): this(quest, task.action, task.item, task.total, task.progress) {
        if (complete) notified = true
    }

    constructor(quest: ActiveQuest, task: Task): this(quest, task.action, task.item, task.total, 0) {
        if (complete) notified = true
    }

    val actionName: String
        get() = action.toString().toProperCase()

    val itemName: String
        get() = item.name.split("_")
            .joinToString(" ") { it[0].uppercaseChar() + it.substring(1).lowercase() }

    val description: String
        get() = "$actionName $total $itemName"

    fun credit(amount: Int){
        progress += amount
        if (progress > total) progress = total
        if (notified) return
        if (complete){
            emit(TaskCompletedEvent(quest, this))
            ok(quest.player, QuestsTag,"Task $description Complete")
            notified = true
        }
    }

    fun cleanup() = HandlerList.unregisterAll(this)

    @EventHandler(priority = EventPriority.LOW, ignoreCancelled = true)
    fun onBlockBreak(event: BlockBreakEvent) {
        if (event.player.gameMode != GameMode.SURVIVAL) return
        if (action != Action.BREAK && action != Action.OBTAIN) return
        if (event.player != quest.player) return
        event.block.getDrops(event.player.inventory.itemInMainHand).filter { it.type == item }.forEach { credit(it.amount) }
    }

//    @EventHandler(ignoreCancelled = true)
//    fun onHangingBreak(event: HangingBreakByEntityEvent){
//        if (event.remover !is Player) return
//        if ((event.remover as Player).gameMode != GameMode.SURVIVAL) return
//        if (!actions.contains(Action.BREAK)) return
//        if (event.remover != player) return
//        if (event.entity.type != item) return
//        credit(1)
//    }

    @EventHandler(ignoreCancelled = true)
    fun onBlockPlace(event: BlockPlaceEvent) {
        if (event.player.gameMode != GameMode.SURVIVAL) return
        if (action != Action.PLACE && action != Action.OBTAIN) return
        if (event.player != quest.player) return
        if (event.block.type != item) return
        credit(1)
    }

//    @EventHandler(ignoreCancelled = true)
//    fun onHangingPlace(event: HangingPlaceEvent){
//        if (event.player !is Player) return
//        if ((event.player as Player).gameMode != GameMode.SURVIVAL) return
//        if (!actions.contains(Action.PLACE)) return
//        if (event.player != player) return
//        if (event.entity.type != item) return
//        credit(1)
//    }

    @EventHandler(ignoreCancelled = true)
    fun onCraftItem(event: CraftItemEvent) {
        if (event.whoClicked.gameMode != GameMode.SURVIVAL) return
        if (action != Action.CRAFT && action != Action.OBTAIN) return
        if (event.whoClicked != quest.player) return
        if (event.recipe.result.type != item) return
        credit(event.recipe.result.amount)
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    fun onFish(event: PlayerFishEvent) {
        if (event.player.gameMode != GameMode.SURVIVAL) return
        if (action != Action.FISH && action != Action.OBTAIN) return
        if (event.player != quest.player) return
        if (event.state != PlayerFishEvent.State.CAUGHT_FISH) return
        if (event.caught !is Item) return
        if ((event.caught as Item).itemStack.type != item) return
        credit(1)
    }

    @EventHandler(ignoreCancelled = true)
    fun onSheer(event: PlayerShearEntityEvent) {
        if (event.player.gameMode != GameMode.SURVIVAL) return
        if (action != Action.SHEAR && action != Action.OBTAIN) return
        if (event.player != quest.player) return
        if (event.item.type != item) return
        credit(event.item.amount)
    }

    @EventHandler(ignoreCancelled = true)
    fun onEntityDeath(event: EntityDeathEvent) {
        if (event.entity.killer !is Player) return
        val killer = event.entity.killer as Player
        if (killer.gameMode != GameMode.SURVIVAL) return
        if (action != Action.OBTAIN) return
        event.drops.filter { it.type == item }.forEach {
            credit(it.amount)
        }
    }

    fun toPlayerTask(): PlayerTask = PlayerTask(
        this.action,
        this.item,
        this.total,
        this.progress,
    )
}

