package com.github.snarechops.renew.features.enchantments

import com.github.snarechops.renew.common.*
import com.github.snarechops.renew.features.commands.Help
import com.github.snarechops.renew.features.commands.HelpTopic
import org.bukkit.Material
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player

@HelpTopic("Custom Enchants")
object EnchantCommand : CommandExecutor {

    @Help(EnchantAdminPermission, "$darkBlue/ce <enchant> <level> $black- Applies the custom enchant to current item in hand")
    override fun onCommand(sender: CommandSender, command: Command, label: String, args: Array<out String>): Boolean {
        if (!sender.hasPermission(EnchantAdminPermission)) return NotAllowed(sender, EnchantTag)
        if (sender !is Player) return MustBePlayer(sender, EnchantTag)
        val name = args.getOrNull(0) ?: return err(sender, EnchantTag, "Missing enchantment name")
        val enchant = Enchants[name.lowercase()] ?: return err(sender, EnchantTag, "Unknown custom enchantment")
        val level = args.getOrNull(1)?.toIntOrNull() ?: 1
        val item = sender.inventory.itemInMainHand
        if (item.type == Material.AIR) return err(sender, EnchantTag, "Must be holding an item to enchant")
        sender.inventory.setItemInMainHand(enchant.enchant(item, level))
        return ok(sender, EnchantTag, green("Custom enchant"), *enchant.name.colorize(), green("added to item"))
    }
}