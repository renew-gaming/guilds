package com.github.snarechops.renew.features.reports

import com.github.snarechops.renew.Feature
import com.github.snarechops.renew.common.Config
import com.github.snarechops.renew.common.pluginTag
import com.github.snarechops.renew.common.register
import io.ktor.client.*
import io.ktor.client.engine.cio.*
import io.ktor.client.features.json.*
import io.ktor.client.features.json.serializer.*
import io.ktor.client.request.*
import io.ktor.http.*
import kotlinx.serialization.Serializable

val ReportsTag = pluginTag("Reports")

@Serializable
data class NewTask(val type: String, val title: String, val description: String, val author: String, val source: String)

fun NewBug(description: String, author: String) = NewTask("BUG", "In-Game Bug Report", description, author, "In-Game Report")
fun NewSuggestion(description: String, author: String) = NewTask("SUGGESTION", "Suggestion", description, author, "In-Game Report")

object ReportFeature: Feature {
    override fun onInit() {
        BugCommand.register("bug", BugCompletion)
        SuggestCommand.register("suggest", SuggestCompletion)
    }

    override fun onDestroy() {}

    private val http = HttpClient(CIO){
        install(JsonFeature){
            serializer = KotlinxSerializer()
        }
    }

    suspend fun createBug(player: String, location: List<Int>?, message: String) {
        saveTask(NewBug(message, player))
    }

    suspend fun createSuggestion(player: String, message: String) {
        saveTask(NewSuggestion(message, player))
    }

    private suspend fun saveTask(task: NewTask) {
        post("${Config.api}/kanban/bug", task)
    }

    private suspend fun post(url: String, content: Any){
        http.post<Unit>(url) {
            body = content
            contentType(ContentType.parse("application/json"))
        }
    }
}

