package com.github.snarechops.renew.features.upgradetokens

import org.bukkit.Bukkit
import org.bukkit.command.Command
import org.bukkit.command.CommandSender
import org.bukkit.command.TabCompleter
import org.bukkit.entity.Player

object TokenCompletion : TabCompleter {
    override fun onTabComplete(
        sender: CommandSender,
        command: Command,
        alias: String,
        args: Array<out String>
    ): MutableList<String> {
        if (sender !is Player) return mutableListOf()
        if (!sender.hasPermission(TokensAdminPermission)) return mutableListOf()
        return when {
            args.isEmpty() -> root("")
            args.size == 1 -> root(args[0])
            args.size == 2 -> players(args[1])
            else -> mutableListOf()
        }
    }

    private fun root(filter: String): MutableList<String> {
        val list = Tokens.keys.toMutableList()
        list.retainAll { it.startsWith(filter, ignoreCase = true) }
        list.sort()
        return list
    }

    private fun players(filter: String): MutableList<String> {
        val list = Bukkit.getOnlinePlayers().map { it.name }.toMutableList()
        list.retainAll { it.startsWith(filter, ignoreCase = true) }
        list.sort()
        return list
    }
}