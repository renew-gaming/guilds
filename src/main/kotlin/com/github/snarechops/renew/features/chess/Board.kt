package com.github.snarechops.renew.features.chess

import com.github.snarechops.renew.common.broadcast
import com.github.snarechops.renew.common.gold
import com.github.snarechops.renew.common.green
import com.github.snarechops.renew.common.register
import org.bukkit.Location
import org.bukkit.entity.Player

class Board(private val feature: ChessFeature, val origin: Location, val villagers: Player, val zombies: Player) {
    val spots: MutableList<MutableList<Spot>>
    var piece: Piece? = null
    var pieces: List<Piece> = listOf()
    var turn: Player = villagers

    init {
        spots = arrayOfNulls<MutableList<Spot>>(8).mapIndexed { x, _ ->
            arrayOfNulls<Spot>(8).mapIndexed { y, _ ->
                Spot(this, x, y)
            }.toMutableList()
        }.toMutableList()

        spots[0][0].piece = Rook(villagers, Team.WHITE, spots[0][0])
        spots[1][0].piece = Knight(villagers, Team.WHITE, spots[1][0])
        spots[2][0].piece = Bishop(villagers, Team.WHITE, spots[2][0])
        spots[3][0].piece = King(villagers, Team.WHITE, spots[3][0])
        spots[4][0].piece = Queen(villagers, Team.WHITE, spots[4][0])
        spots[5][0].piece = Bishop(villagers, Team.WHITE, spots[5][0])
        spots[6][0].piece = Knight(villagers, Team.WHITE, spots[6][0])
        spots[7][0].piece = Rook(villagers, Team.WHITE, spots[7][0])

        spots[0][7].piece = Rook(zombies, Team.BLACK, spots[0][7])
        spots[1][7].piece = Knight(zombies, Team.BLACK, spots[1][7])
        spots[2][7].piece = Bishop(zombies, Team.BLACK, spots[2][7])
        spots[3][7].piece = King(zombies, Team.BLACK, spots[3][7])
        spots[4][7].piece = Queen(zombies, Team.BLACK, spots[4][7])
        spots[5][7].piece = Bishop(zombies, Team.BLACK, spots[5][7])
        spots[6][7].piece = Knight(zombies, Team.BLACK, spots[6][7])
        spots[7][7].piece = Rook(zombies, Team.BLACK, spots[7][7])

        for (x in 0..7) {
            spots[x][1].piece = Pawn(villagers, Team.WHITE, spots[x][1])
            spots[x][6].piece = Pawn(zombies, Team.BLACK, spots[x][6])
        }
        broadcast("${green}A chess match between $gold${villagers.name} ${green}and $gold${zombies.name} ${green}has begun! ${gold}/warp dome ${green}to see the action!")
        spots.forEach {
            it.forEach { spot ->
                spot.piece?.spawn()
                spot.register()
                spot.piece?.register()
            }
        }
    }

    fun end() {
        spots.flatten().forEach { it.cleanup() }
        feature.board = null
    }

    fun resetSelection() = spots.flatten().forEach {
        it.piece?.highlight(false)
        it.highlight(false, Team.WHITE)
        this.piece = null
    }

    fun select(piece: Piece) {
        resetSelection()
        piece.moves().forEach { it.highlight(true, piece.team) }
        piece.highlight(true)
        this.piece = piece
    }

    fun select(spot: Spot) {
        if (piece == null) return
        if (!piece!!.move(spot)) return
        resetSelection()
        if (checkVictory()) return end()
        nextTurn()
    }

    fun check(team: Team): Boolean {
        // Remove any old check highlights
        spots.flatten().forEach { it.highlightCheck(false) }
        // Look for king in check (or escape early)
        val checkSpot = pieces.filter { it.team == team.opposite() }
            .flatMap { it.moves() }
            .filter { it.piece?.team == team }
            .find { it.piece is King } ?: return false
        // Highlight it
        checkSpot.highlightCheck(true)
        // Alert players
        return true
    }

    fun nextTurn() {
        // Get next player and team
        val (player, team) = when (turn) {
            villagers -> Pair(zombies, Team.BLACK)
            zombies -> Pair(villagers, Team.WHITE)
            else -> return
        }
        // Flip who's turn it is
        turn = player
        // Highlight if is in check
        check(team)
        // Highlight all team pieces to show turn has flipped
        pieces.filter { it.team == team }.forEach { it.highlight(true) }
    }

    fun checkVictory(): Boolean {
        fun announce(winner: Player, loser: Player) =
            broadcast("$gold${winner.name} ${green}has defeated $gold${loser.name} at chess!")

        if (!teamHasKing(Team.WHITE)) {
            announce(zombies, villagers)
            return true
        }
        if (!teamHasKing(Team.BLACK)) {
            announce(villagers, zombies)
            return true
        }
        return false
    }

    private fun teamHasKing(team: Team): Boolean =
        spots.flatten().mapNotNull { it.piece }.filter { it.team == team }.any { it is King }
}