package com.github.snarechops.renew.features.reports

import com.github.snarechops.renew.common.black
import com.github.snarechops.renew.common.darkBlue
import com.github.snarechops.renew.common.err
import com.github.snarechops.renew.common.ok
import com.github.snarechops.renew.features.commands.Help
import com.github.snarechops.renew.features.commands.HelpTopic
import kotlinx.coroutines.runBlocking
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player

@HelpTopic("Bug")
object BugCommand: CommandExecutor{

    @Help("", "$darkBlue/bug <message...> $black- Files a bug saving information about your current items, location, and alerts the server staff. Check the #bugs channel in discord for updates.")
    override fun onCommand(sender: CommandSender, command: Command, label: String, args: Array<out String>): Boolean {
        if (args.isEmpty()) return err(sender, ReportsTag, "Please describe the bug. /bug <message>")

        runBlocking {
            val player = if (sender is Player) sender.name else "Unknown"
            val location = getLocation(sender)
            val message = args.joinToString(separator = " ")
            ReportFeature.createBug(player, location, message)
        }
        return ok(sender, ReportsTag, "Bug report sent!")
    }

    private fun getLocation(sender: CommandSender): List<Int>?{
        if (sender is Player) {
            return listOf(sender.location.blockX, sender.location.blockY, sender.location.blockZ)
        }
        return null
    }
}