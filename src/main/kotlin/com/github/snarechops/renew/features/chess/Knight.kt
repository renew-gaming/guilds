package com.github.snarechops.renew.features.chess

import org.bukkit.entity.Player

class Knight(player: Player, team: Team, spot: Spot) : Piece(player, team, spot, when (team) {
    Team.WHITE -> ::WhiteKnight
    Team.BLACK -> ::BlackKnight
}) {
    override fun moves(): List<Spot> {
        return board.spots.flatten().map { Move(spot, it) }
            .filter { it.deltaX * it.deltaY == 2 }
            .filter { it.to.piece?.team != team }
            .map { it.to }
    }
}