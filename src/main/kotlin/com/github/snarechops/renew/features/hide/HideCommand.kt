package com.github.snarechops.renew.features.hide

import com.github.snarechops.renew.common.*
import com.github.snarechops.renew.features.commands.Help
import com.github.snarechops.renew.features.commands.HelpTopic
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player

@HelpTopic("Hide")
object HideCommand: CommandExecutor {
    override fun onCommand(sender: CommandSender, command: Command, label: String, args: Array<out String>): Boolean {
        return when (args.getOrNull(0)) {
            null -> hide(sender)
            "hide" -> hide(sender)
            "show" -> show(sender)
            else -> InvalidCommand(sender, HideTag)
        }
    }

    @Help(HidePermission, "$darkBlue/hide ${black}or $darkBlue/hide hide $black- Makes you appear as a normal player")
    private fun hide(sender: CommandSender): Boolean{
        if (!sender.hasPermission(HidePermission)) return NotAllowed(sender, HideTag)
        if (sender !is Player) return MustBePlayer(sender, HideTag);
        HideFeature.hide(sender)
        return ok(sender, HideTag, "You are now appearing as a normal player")
    }

    @Help(HidePermission, "$darkBlue/hide show $black- Reveals your actual prefix, reversing your hidden status")
    private fun show(sender: CommandSender): Boolean {
        if (!sender.hasPermission(HidePermission)) return NotAllowed(sender, HideTag)
        if (sender !is Player) return MustBePlayer(sender, HideTag);
        HideFeature.show(sender)
        return ok(sender, HideTag, "You no longer appear as a normal player")
    }
}