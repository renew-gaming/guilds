package com.github.snarechops.renew.features.announcements

import com.github.snarechops.renew.common.*
import com.github.snarechops.renew.features.commands.Help
import com.github.snarechops.renew.features.commands.HelpTopic
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import org.litote.kmongo.newId

@HelpTopic("Announcements")
object AnnouncementCommand: CommandExecutor {
    override fun onCommand(sender: CommandSender, command: Command, label: String, args: Array<out String>): Boolean =
        when {
            args.isEmpty() -> MissingSubCommand(sender, AnnouncerTag)
            args[0] == "list" -> list(sender)
            args[0] == "reload" -> reload(sender)
            args[0] == "create" -> create(sender, args.drop(1))
            args[0] == "set" -> set(sender, args.drop(1))
            args[0] == "delete" -> delete(sender, args.drop(1))
            args[0] == "trigger" -> trigger(sender, args.drop(1))
            else -> InvalidCommand(sender, AnnouncerTag)
        }

    @Help("", "$darkBlue/announce list $black- Lists all registered announcements")
    private fun list(sender: CommandSender): Boolean {
        val items = AnnouncementFeature.get()
        items.forEachIndexed { i, it -> sender.message(gold("$i -"), *it.message.colorize()) }
        return true
    }

    @Help(AnnouncementAdminPermission, "$darkBlue/announce reload $black- Reloads all announcements from the database")
    private fun reload(sender: CommandSender): Boolean {
        if (!sender.hasPermission(AnnouncementAdminPermission)) return NotAllowed(sender, AnnouncerTag)
        AnnouncementFeature.reload()
        return ok(sender, AnnouncerTag, "Announcements reloaded")
    }

    @Help(AnnouncementAdminPermission, "$darkBlue/announce trigger <index> $black- Trigger an announcement to display immediately")
    private fun trigger(sender: CommandSender, args: List<String>): Boolean {
        if (!sender.hasPermission(AnnouncementAdminPermission)) return NotAllowed(sender, AnnouncerTag)
        val idx = args.getOrNull(0)?.toIntOrNull() ?: return err(sender, AnnouncerTag, "Missing index")
        val announcement = AnnouncementFeature.announcements.getOrNull(idx) ?: return err(sender, AnnouncerTag, "Invalid index")
        AnnouncementFeature.announce(announcement)
        return true
    }

    @Help(AnnouncementAdminPermission, "$darkBlue/announce create <period in minutes> <message...> $black- Create a new announcement")
    private fun create(sender: CommandSender, args: List<String>): Boolean {
        if (!sender.hasPermission(AnnouncementAdminPermission)) return NotAllowed(sender, AnnouncerTag)
        val period = args.getOrNull(0)?.toIntOrNull() ?: return err(sender, AnnouncerTag, "Missing period")
        if (args.size < 2) return err(sender, AnnouncerTag, "Missing message")
        val message = args.drop(1).joinToString(" ")
        val announcement = Announcement(newId(), message, period, true)
        AnnouncementFeature.save(announcement)
        AnnouncementFeature.reload()
        return ok(sender, AnnouncerTag, "Announcement created")
    }

    @Help(AnnouncementAdminPermission, "$darkBlue/announce delete <index> $black- Delete an announcement")
    private fun delete(sender: CommandSender, args: List<String>): Boolean {
        if (!sender.hasPermission(AnnouncementAdminPermission)) return NotAllowed(sender, AnnouncerTag)
        val idx = args.getOrNull(0)?.toIntOrNull() ?: return err(sender, AnnouncerTag, "Missing index")
        val announcement = AnnouncementFeature.announcements.getOrNull(idx) ?: return err(sender, AnnouncerTag, "Invalid index")
        AnnouncementFeature.delete(announcement.id)
        return ok(sender, AnnouncerTag, "Announcement deleted")
    }

    private fun set(sender: CommandSender, args: List<String>): Boolean {
        if (!sender.hasPermission(AnnouncementAdminPermission)) return NotAllowed(sender, AnnouncerTag)
        val idx = args.getOrNull(0)?.toIntOrNull() ?: return err(sender, AnnouncerTag, "Missing id")
        val prop = args.getOrNull(1) ?: return err(sender, AnnouncerTag, "Missing property name")
        return when (prop) {
            "period" -> setPeriod(sender, idx, args.drop(2))
            "enabled" -> setEnabled(sender, idx, args.drop(2))
            else -> err(sender, AnnouncerTag, "Invalid property name")
        }
    }

    @Help(AnnouncementAdminPermission, "$darkBlue/announce set period <index> <period in minutes> $black- Set an announcements period")
    private fun setPeriod(sender: CommandSender, idx: Int, args: List<String>): Boolean {
        val period = args.getOrNull(0)?.toIntOrNull() ?: return err(sender, AnnouncerTag, "Missing period")
        val announcement = AnnouncementFeature.announcements.getOrNull(idx) ?: return err(sender, AnnouncerTag, "Invalid id")
        announcement.period = period
        AnnouncementFeature.save(announcement)
        AnnouncementFeature.reload()
        return ok(sender, AnnouncerTag, "Announcement period set")
    }

    @Help(AnnouncementAdminPermission, "$darkBlue/announce set enabled <index> <true|false> $black- Enable or disable an announcement")
    private fun setEnabled(sender: CommandSender, idx: Int, args: List<String>): Boolean {
        val enabled = args.getOrNull(0)?.lowercase()?.toBooleanStrictOrNull() ?: return err(sender, AnnouncerTag, "Missing or invalid boolean")
        val announcement = AnnouncementFeature.announcements.getOrNull(idx) ?: return err(sender, AnnouncerTag, "Invalid id")
        announcement.enabled = enabled
        AnnouncementFeature.save(announcement)
        AnnouncementFeature.reload()
        return ok(sender, AnnouncerTag, "Announcement ${if (enabled) "enabled" else "disabled"}")
    }
}