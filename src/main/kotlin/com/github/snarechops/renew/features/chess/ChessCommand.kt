package com.github.snarechops.renew.features.chess

import com.github.snarechops.renew.common.*
import com.github.snarechops.renew.features.commands.Help
import com.github.snarechops.renew.features.commands.HelpTopic
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player

@HelpTopic("Chess")
object ChessCommand: CommandExecutor {
    override fun onCommand(sender: CommandSender, command: Command, label: String, args: Array<out String>): Boolean {
        when (args.getOrNull(0)?.lowercase()) {
            "setup" -> setup(sender)
            "clear" -> clear(sender)
            "on" -> on(sender)
            "off" -> off(sender)
        }
        if (sender !is Player) return MustBePlayer(sender, ChessTag)
        return true
    }

    @Help(ChessAdminPermission, "$darkBlue/chess setup $black- Setup the location of the chess board. Sets to your current location.")
    private fun setup(sender: CommandSender): Boolean {
        if (!sender.hasPermission(ChessAdminPermission)) return NotAllowed(sender, ChessTag)
        if (sender !is Player) return MustBePlayer(sender, ChessTag)
        ChessFeature.setup(sender.location)
        return ok(sender, ChessTag, "Chess board setup")
    }

    @Help(ChessAdminPermission, "$darkBlue/chess clear $black- Clear and reset the currently running chess game.")
    private fun clear(sender: CommandSender): Boolean {
        if (!sender.hasPermission(ChessAdminPermission)) return NotAllowed(sender, ChessTag)
        ChessFeature.clear(true)
        return ok(sender, ChessTag, "Chess reset to a clear state")
    }

    @Help(ChessAdminPermission, "$darkBlue/chess on $black- Enables the chess feature")
    private fun on(sender: CommandSender): Boolean {
        if (!sender.hasPermission(ChessAdminPermission)) return NotAllowed(sender, ChessTag)
        if (!ChessFeature.enabled(true)) return err(sender, ChessTag, "Failed to enable Chess")
        return ok(sender, ChessTag, "Chess enabled!")
    }

    @Help(ChessAdminPermission, "$darkBlue/chess off $black- Disables the chess feature")
    private fun off(sender: CommandSender): Boolean {
        if (!sender.hasPermission(ChessAdminPermission)) return NotAllowed(sender, ChessTag)
        if (!ChessFeature.enabled(false)) return err(sender, ChessTag, "Failed to disable Chess")
        return ok(sender, ChessTag, "Chess disabled!")
    }
}