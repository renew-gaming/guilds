package com.github.snarechops.renew.features.deaths

import com.github.snarechops.renew.Feature
import com.github.snarechops.renew.common.*
import com.mongodb.client.MongoCollection
import org.bson.types.ObjectId
import org.bukkit.Bukkit
import org.bukkit.entity.Player
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.entity.PlayerDeathEvent
import org.bukkit.event.inventory.InventoryClickEvent
import org.litote.kmongo.*
import org.litote.kmongo.id.toId
import kotlin.math.ceil

const val DEATHS_PREFIX = "${lightPurple}Deaths: $gold"
const val DEATH_PREFIX = "${lightPurple}Death: $gold"
const val DeathAdminPermission = "death.admin"
val DeathTag = pluginTag("Deaths")

val DeathCollection: MongoCollection<Death>
    get() = Database.getDatabase("renew").getCollection("death", Death::class.java)

object DeathFeature : Feature, Listener {
    override fun onInit() {
        DeathCommand.register("deaths", DeathCompletion)
        this.register()
    }

    override fun onDestroy() {}

    private fun pages(uuid: String): Int =
        ceil((DeathCollection.countDocuments(Death::uuid eq uuid).toDouble()) / 45.0).toInt()

    fun get(uuid: String, page: Int): List<Death> =
        DeathCollection.find(Death::uuid eq uuid)
            .sort(descending(Death::time))
            .skip((page - 1) * 45)
            .limit(45)
            .toList()

    fun getOne(id: String?): Death? =
        DeathCollection.findOne(Death::id eq ObjectId(id).toId())

    fun save(death: Death) =
        DeathCollection.insertOne(death)

    fun ui(player: Player, target: Player, page: Int) {
        val inventory = Bukkit.createInventory(player, 54, "$DEATHS_PREFIX${target.name}")
        get(target.uniqueId.toString(), page).map { it.displayItem() }.forEach { inventory.addItem(it) }
        inventory.setItem(45, if (page <= 1) NoPageButton() else PageButton(page - 1))
        inventory.setItem(53, if (page >= pages(target.uniqueId.toString())) NoPageButton() else PageButton(page + 1))
        player.openInventory(inventory)
    }

    fun show(player: Player, target: String, id: String) {
        val death = getOne(id) ?: return
        val inventory = newInventory(death.inventory, 54, "$DEATH_PREFIX$target")
        inventory.setItem(53, BackButton())
        player.openInventory(inventory)
    }

    @EventHandler
    fun onPlayerDeath(e: PlayerDeathEvent) =
        save(Death(
            id = newId(),
            uuid = e.entity.uniqueId.toString(),
            name = e.entity.name,
            inventory = e.entity.inventory.contents.encodeString(),
            level = e.entity.level,
            location = Coordinate(
                x = e.entity.location.blockX,
                y = e.entity.location.blockY,
                z = e.entity.location.blockZ,
            ),
            cause = e.entity.lastDamageCause?.cause.toString()
        ))

    @EventHandler(ignoreCancelled = true)
    fun onDeathsView(event: InventoryClickEvent) {
        if (!event.view.title.startsWith(DEATHS_PREFIX)) return
        event.isCancelled = true
        val name = event.view.title.removePrefix(DEATHS_PREFIX)
        val target = Bukkit.getPlayer(name) ?: return
        for (page in 1..pages(target.uniqueId.toString())) {
            if (event.currentItem != PageButton(page)) continue
            return ui(event.whoClicked as Player, target, page)
        }
        val id = event.currentItem?.itemMeta?.lore?.first()?.removePrefix(darkGray) ?: return
        show(event.whoClicked as Player, target.name, id)
    }

    @EventHandler(ignoreCancelled = true)
    fun onDeathView(event: InventoryClickEvent) {
        if (!event.view.title.startsWith(DEATH_PREFIX)) return
        val name = event.view.title.removePrefix(DEATH_PREFIX)
        val target = Bukkit.getPlayer(name) ?: return
        if (event.currentItem == BackButton()) {
            event.isCancelled = true
            ui(event.whoClicked as Player, target, 1)
        }
    }
}