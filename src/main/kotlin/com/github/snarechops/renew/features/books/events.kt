package com.github.snarechops.renew.features.books

import com.github.snarechops.renew.common.CustomEvent
import org.bukkit.entity.Player
import org.bukkit.inventory.ItemStack

data class ShowBookEvent(val player: Player, val book: ItemStack) : CustomEvent()
