package com.github.snarechops.renew.features.chess

import org.bukkit.Location
import org.bukkit.entity.*

fun BlackPawn(location: Location): LivingEntity? {
    val pawn = location.world?.spawnEntity(location, EntityType.ZOMBIE) as Zombie? ?: return null
    pawn.setAI(false)
    pawn.setBaby()
    pawn.isSilent = true
    pawn.equipment?.setItemInMainHand(null)
    pawn.equipment?.helmet = null
    pawn.equipment?.chestplate = null
    pawn.equipment?.leggings = null
    pawn.equipment?.boots = null
    pawn.setRotation(180f, 0f)
    pawn.customName = "Pawn"
    return pawn
}

fun BlackRook(location: Location): LivingEntity? {
    val rook = location.world?.spawnEntity(location, EntityType.ZOMBIE) as Zombie? ?: return null
    rook.setAI(false)
    rook.setAdult()
    rook.isSilent = true
    rook.equipment?.setItemInMainHand(null)
    rook.equipment?.helmet = null
    rook.equipment?.chestplate = null
    rook.equipment?.leggings = null
    rook.equipment?.boots = null
    rook.setRotation(180f, 0f)
    rook.customName = "Rook"
    return rook
}

fun BlackKnight(location: Location): LivingEntity? {
    val knight = location.world?.spawnEntity(location, EntityType.ZOMBIE_HORSE) as ZombieHorse? ?: return null
    knight.setAI(false)
    knight.setAdult()
    knight.isSilent = true
    knight.setRotation(180f, 0f)
    knight.customName = "Knight"
    return knight
}

fun BlackBishop(location: Location): LivingEntity? {
    val bishop = location.world?.spawnEntity(location, EntityType.ZOMBIE_VILLAGER) as ZombieVillager? ?: return null
    bishop.setAI(false)
    bishop.setAdult()
    bishop.villagerProfession = Villager.Profession.CLERIC
    bishop.isSilent = true
    bishop.equipment?.setItemInMainHand(null)
    bishop.equipment?.helmet = null
    bishop.equipment?.chestplate = null
    bishop.equipment?.leggings = null
    bishop.equipment?.boots = null
    bishop.setRotation(180f, 0f)
    bishop.customName = "Bishop"
    return bishop
}

fun BlackQueen(location: Location): LivingEntity? {
    val queen = location.world?.spawnEntity(location, EntityType.DROWNED) as Drowned? ?: return null
    queen.setAI(false)
    queen.isSilent = true
    queen.equipment?.setItemInMainHand(null)
    queen.equipment?.helmet = null
    queen.equipment?.chestplate = null
    queen.equipment?.leggings = null
    queen.equipment?.boots = null
    queen.setRotation(180f, 0f)
    queen.customName = "Queen"
    return queen
}

fun BlackKing(location: Location): LivingEntity? {
    val king = location.world?.spawnEntity(location, EntityType.ZOMBIFIED_PIGLIN) as PigZombie? ?: return null
    king.setAI(false)
    king.setAdult()
    king.isSilent = true
    king.equipment?.setItemInMainHand(null)
    king.equipment?.helmet = null
    king.equipment?.chestplate = null
    king.equipment?.leggings = null
    king.equipment?.boots = null
    king.setRotation(180f, 0f)
    king.customName = "King"
    return king
}