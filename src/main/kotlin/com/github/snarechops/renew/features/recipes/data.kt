package com.github.snarechops.renew.features.recipes

import com.github.snarechops.renew.Renew
import kotlinx.serialization.Serializable
import org.bukkit.Material
import org.bukkit.NamespacedKey
import org.bukkit.inventory.*
import org.bukkit.plugin.java.JavaPlugin

@Serializable
data class CookingRecipe(
    val name: String,
    val type: String,
    val result: RecipeResult,
    val choices: List<Material>,
    val xp: Float,
    val ticks: Int,
)

@Serializable
data class MerchantRecipe(
    val name: String,
    val result: RecipeResult,
    val maxUses: Int,
)

@Serializable
data class SmithingRecipe(
    val name: String,
    val result: RecipeResult,
    val bases: List<Material>,
    val additions: List<Material>,
)

@Serializable
data class StonecuttingRecipe(
    val name: String,
    val result: RecipeResult,
    val source: Material,
)

@Serializable
data class ShapedRecipe(
    val name: String,
    val result: RecipeResult,
    val shape: String,
    val ingredients: List<ShapedIngredient>,
)

@Serializable
data class ShapedIngredient(
    val key: Char,
    val material: Material,
)

@Serializable
data class ShapelessRecipe(
    val name: String,
    val result: RecipeResult,
    val ingredients: List<Material>,
)

@Serializable
data class RecipeResult(val material: Material, val amount: Int)

fun RecipeResult.toItemStack() = ItemStack(this.material, this.amount)

fun CookingRecipe.toBukkitRecipe(): Recipe {
    val key = NamespacedKey(Renew, this.name)
    val result = this.result.toItemStack()
    val choice = RecipeChoice.MaterialChoice(this.choices)
    return when (this.type) {
        "blast" -> BlastingRecipe(key, result, choice, this.xp, this.ticks)
        "campfire" -> CampfireRecipe(key, result, choice, this.xp, this.ticks)
        "furnace" -> FurnaceRecipe(key, result, choice, this.xp, this.ticks)
        "smoker" -> SmokingRecipe(key, result, choice, this.xp, this.ticks)
        else -> throw Error("Invalid cooking recipe type: ${this.type}")
    }
}

fun MerchantRecipe.toBukkitRecipe() = MerchantRecipe(this.result.toItemStack(), this.maxUses)

fun ShapedRecipe.toBukkitRecipe(): Recipe =
    ShapedRecipe(NamespacedKey(Renew, this.name), this.result.toItemStack()).let {
        it.shape(this.shape.take(3), this.shape.take(6).drop(3), this.shape.take(9).drop(6))
        this.ingredients.forEach { x -> it.setIngredient(x.key, x.material) }
        return it
    }

fun ShapelessRecipe.toBukkitRecipe(): Recipe {
    val recipe = ShapelessRecipe(NamespacedKey(Renew, this.name), this.result.toItemStack())
    this.ingredients.forEach { recipe.addIngredient(it) }
    return recipe
}

fun SmithingRecipe.toBukkitRecipe(): Recipe =
    SmithingRecipe(
        NamespacedKey(Renew, this.name),
        this.result.toItemStack(),
        RecipeChoice.MaterialChoice(this.bases),
        RecipeChoice.MaterialChoice(this.additions),
    )

fun StonecuttingRecipe.toBukkitRecipe(): Recipe =
    StonecuttingRecipe(
        NamespacedKey(Renew, this.name),
        this.result.toItemStack(),
        this.source,
    )