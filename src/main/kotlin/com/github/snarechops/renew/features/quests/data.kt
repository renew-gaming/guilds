package com.github.snarechops.renew.features.quests

import com.github.snarechops.renew.common.toProperCase
import com.github.snarechops.renew.features.guilds.GuildName
import kotlinx.serialization.Contextual
import kotlinx.serialization.Serializable
import org.bukkit.Material
import java.time.Instant
import java.util.*

@Serializable
enum class Action {
    OBTAIN,
    CRAFT,
    BREAK,
    PLACE,
    FISH,
    SHEAR,
    PLANT;

    companion object {
        fun fromString(value: String?): Action? {
            if (value == null) return null
            return try {
                valueOf(value)
            } catch (e: IllegalArgumentException) {
                null
            }
        }
    }
}

@Serializable
data class Task(
    val action: Action,
    val item: Material,
    val total: Int,
)

@Serializable
data class Quest(
    val guild: GuildName,
    val tasks: List<Task>,
    val commands: List<String>,
)

fun Quest.name(): String = "${this.guild.toString().toProperCase()} Quest"

@Serializable
data class PlayerTask(
    val action: Action,
    val item: Material,
    val total: Int,
    val progress: Int,
)

@Serializable
data class PlayerQuest(
    val uuid: String,
    val name: String,
    val guild: GuildName,
    val tasks: List<PlayerTask>,
    val commands: List<String>,
    @Contextual
    val expires: Instant,
)

val Actions = Action.values().map { it.toString().lowercase() }
val Materials = Material.values().map { it.toString().lowercase() }
