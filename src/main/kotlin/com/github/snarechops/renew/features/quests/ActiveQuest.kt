package com.github.snarechops.renew.features.quests

import com.github.snarechops.renew.common.*
import com.github.snarechops.renew.common.centered
import com.github.snarechops.renew.common.toProperCase
import com.github.snarechops.renew.features.guilds.GuildName
import com.github.snarechops.renew.features.stats.StatIncrement
import org.bukkit.Bukkit
import org.bukkit.entity.Player
import org.bukkit.event.EventHandler
import org.bukkit.event.HandlerList
import org.bukkit.event.Listener
import java.time.Instant
import java.util.*

class ActiveQuest : Listener {
    val player: Player
    val uuid: UUID
    val guild: GuildName
    val tasks: List<ActiveTask>
    var expires: Instant
    var timeout: Instant = Instant.MIN
    var notified = false
    var commands: List<String> = listOf()
    val complete: Boolean
        get() = tasks.all { it.complete }

    constructor(quest: PlayerQuest) {
        uuid = UUID.fromString(quest.uuid)
        player = Bukkit.getPlayer(uuid)!!
        guild = quest.guild
        tasks = quest.tasks.map { ActiveTask(this, it) }
        commands = quest.commands
        expires = quest.expires
        if (complete) notified = true
    }

    constructor(player: Player, quest: Quest) {
        this.uuid = player.uniqueId
        this.player = player
        guild = quest.guild
        tasks = quest.tasks.map { ActiveTask(this, it) }
        commands = quest.commands
        expires = Instant.now().plusSeconds((6 * 60 * 60).toLong())
        if (complete) notified = true
    }

    val isExpired: Boolean
        get() = Instant.now().isAfter(expires)

    val name: String
        get() = "${guild.toString().toProperCase()} Quest"

    val canAbandon: Boolean
        get() = timeout > Instant.now()

    fun cleanup() {
        HandlerList.unregisterAll(this)
        tasks.forEach { it.cleanup() }
    }

    fun toPlayerQuest() = PlayerQuest(
        this.uuid.toString(),
        this.player.name,
        this.guild,
        this.tasks.map { it.toPlayerTask() },
        this.commands,
        this.expires,
    )

    @EventHandler
    fun onTaskComplete(event: TaskCompletedEvent) {
        if (event.quest != this) return
        if (!complete) return
        if (notified) return
        println(commands)
        commands.forEach { commandAsConsole(it.replace("{player}", player.name)) }
        emit(StatIncrement(player, "quests_complete_${guild.toString().lowercase()}"))
        emit(QuestCompletedEvent(player, this))
        ok(player, QuestsTag, gold(name), green("Complete! Your rewards have been received"))
    }

    fun bookPage(): String {
        val g = centered(gold + underline + bold +  guild)
        val t = tasks.joinToString(separator = "\n") {
            "$darkAqua- ${it.actionName} ${if(it.complete) darkAqua else lightPurple}${it.progress}$darkAqua/${it.total} ${it.itemName}"
        }
        return "$g\n${if (!complete) formatExpires(expires) else "$green(Complete)"}\n$t"
    }
}



