package com.github.snarechops.renew.features.recipes

import com.github.snarechops.renew.*
import com.github.snarechops.renew.common.Database
import com.github.snarechops.renew.common.debug
import org.bukkit.inventory.*

object RecipeFeature : Feature {

    override fun onInit() {
        debug("Registering Cooking recipes")
        load("recipe.cooking") { it: CookingRecipe -> it.toBukkitRecipe() }
        load("recipe.merchant") { it: MerchantRecipe -> it.toBukkitRecipe() }
        load("recipe.shaped") { it: ShapedRecipe -> it.toBukkitRecipe() }
        load("recipe.shapeless") { it: ShapelessRecipe -> it.toBukkitRecipe() }
        load("recipe.smithing") { it: SmithingRecipe -> it.toBukkitRecipe() }
        load("recipe.stonecutting") { it: StonecuttingRecipe -> it.toBukkitRecipe() }
    }

    override fun onDestroy() {}

    private inline fun <reified T> load(collection: String, f: (T) -> Recipe?) =
        Database.getDatabase("guilds").getCollection(collection, T::class.java)
            .find()
            .toList()
            .map { f(it) }
            .forEach { Renew.server.addRecipe(it) }
}
