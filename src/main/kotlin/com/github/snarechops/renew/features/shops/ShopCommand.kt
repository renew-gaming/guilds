package com.github.snarechops.renew.features.shops

import com.github.snarechops.renew.common.*
import org.bukkit.Material
import org.bukkit.block.Sign
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player

object ShopCommand : CommandExecutor {
    override fun onCommand(sender: CommandSender, command: Command, label: String, args: Array<out String>): Boolean =
        when {
            args.isEmpty() -> MissingSubCommand(sender, ShopsTag)
            args[0] == "product" -> product(sender, args.drop(1))
//            args[0] == "cost" -> cost(sender, args.drop(1))
//            args[0] == "find" -> find(sender, args.drop(1))
            else -> InvalidCommand(sender, ShopsTag)
        }

    private fun product(sender: CommandSender, args: List<String>): Boolean {
        if (sender !is Player) return MustBePlayer(sender, ShopsTag)
        val block = sender.rayTraceBlocks(5.0)?.hitBlock ?: return NoSignFound(sender)
        if (!block.isSign()) return NoSignFound(sender)
        val sign = block.blockData as Sign
        if (sign.getLine(0) != SHOP_SIGN_TITLE) return NoSignFound(sender)
        val coord = block.location.toSimpleLocation()
        val shop = ShopFeature.getShop(coord) ?: return NoSignFound(sender)
        if (shop.owner.name != sender.name && !sender.hasPermission(ShopsAdminPermission)) return NotYourShop(sender)
        val amount = args.getOrNull(0)?.toIntOrNull() ?: return MissingItemAmount(sender)
        val item = sender.inventory.itemInMainHand
        if (item.type == Material.AIR) return MustHaveItemInHand(sender)
        return ShopFeature.setShopProduct(sender, sign, shop, item, amount)
    }
}

fun NoSignFound(sender: CommandSender): Boolean =
    err(sender, ShopsTag, "No shop sign found where you're looking")

fun NotYourShop(sender: CommandSender): Boolean =
    err(sender, ShopsTag, "Cannot modify shops of other players")

fun MissingItemAmount(sender: CommandSender): Boolean =
    err(sender, ShopsTag, "Missing item amount")

fun MustHaveItemInHand(sender: CommandSender): Boolean =
    err(sender, ShopsTag, "Must be holding the item you wish to use")