package com.github.snarechops.renew.features.chess

import org.bukkit.entity.Player

class Rook(player: Player, team: Team, spot: Spot) : Piece(
    player, team, spot, when (team) {
        Team.WHITE -> ::WhiteRook
        Team.BLACK -> ::BlackRook
    }
) {
    var hasMoved: Boolean = false

    override fun moves() : List<Spot> {
        val result = mutableListOf<Spot>()
        result.addAll(walk(1, 0))
        result.addAll(walk(-1,0))
        result.addAll(walk(0, 1))
        result.addAll(walk(0, -1))
        return result
    }

    override fun move(end: Spot): Boolean {
        hasMoved = true
        return super.move(end)
    }
}
