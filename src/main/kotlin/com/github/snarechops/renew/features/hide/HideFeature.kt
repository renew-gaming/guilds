package com.github.snarechops.renew.features.hide

import com.github.snarechops.renew.Feature
import com.github.snarechops.renew.Renew
import com.github.snarechops.renew.common.pluginTag
import com.github.snarechops.renew.common.register
import org.bukkit.entity.Player

const val HidePermission = "hide.use"
val HideTag = pluginTag("Hide")

object HideFeature: Feature {
    override fun onInit() {
        HideCommand.register("hide", HideCompletion)
    }

    override fun onDestroy() {}

    fun hide(player: Player) =
        Renew.server.dispatchCommand(Renew.server.consoleSender, "lp user ${player.name} parent add incognito")

    fun show(player: Player) =
        Renew.server.dispatchCommand(Renew.server.consoleSender, "lp user ${player.name} parent remove incognito")
}