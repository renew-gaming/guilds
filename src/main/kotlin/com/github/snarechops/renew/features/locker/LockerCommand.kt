package com.github.snarechops.renew.features.locker

import com.github.snarechops.renew.common.*
import com.github.snarechops.renew.features.commands.Help
import com.github.snarechops.renew.features.commands.HelpTopic
import org.bukkit.Bukkit
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player

@HelpTopic("Locker")
object LockerCommand: CommandExecutor {

    @Help(LockerUsePermission, "$darkBlue/locker $black- Opens your locker")
    override fun onCommand(sender: CommandSender, command: Command, label: String, args: Array<out String>): Boolean {
        return when (args.size) {
            1 -> open(sender, args.drop(1))
            else -> open(sender, listOf())
        }
    }

    @Help(LockerAdminPermission, "$darkBlue/locker <name> $black- Opens the locker of the specified player")
    private fun open(sender: CommandSender, args: List<String>): Boolean {
        if (sender !is Player) return MustBePlayer(sender, LockerTag)
        if (!sender.hasPermission(LockerUsePermission)) return NotAllowed(sender, LockerTag)
        val name = if (sender.hasPermission(LockerAdminPermission)) args.getOrNull(0) ?: sender.name else sender.name
        @Suppress("DEPRECATION")
        val target = Bukkit.getOfflinePlayer(name)
        val locker = LockerFeature.getOne(target.uniqueId.toString()) ?: Locker(sender.uniqueId.toString(), sender.name, "")
        sender.openInventory(locker.toInventory())
        return true
    }

}