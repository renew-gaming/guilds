package com.github.snarechops.renew.features.valentines

import com.github.snarechops.renew.common.red
import kotlinx.serialization.Serializable
import org.bukkit.Material
import org.bukkit.enchantments.Enchantment
import org.bukkit.inventory.ItemStack

@Serializable
data class Valentine(val uuid: String, var parkourReward: Boolean)

fun ValentineWitherRose(): ItemStack {
    val result = ItemStack(Material.WITHER_ROSE, 1)
    result.addUnsafeEnchantment(Enchantment.MENDING, 1)
    val meta = result.itemMeta ?: return result
    meta.setDisplayName(VALENTINES_DAY_ROSE_NAME)
    val lore = mutableListOf(
        "${red}Holding this rose in your hand directs",
        "${red}love's flow through your body",
    )
    meta.lore = lore
    result.itemMeta = meta
    return result
}