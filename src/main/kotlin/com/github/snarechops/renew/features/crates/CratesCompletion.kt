package com.github.snarechops.renew.features.crates

import org.bukkit.Bukkit
import org.bukkit.command.Command
import org.bukkit.command.CommandSender
import org.bukkit.command.TabCompleter

object CratesCompletion : TabCompleter {
    private fun crateIds(filter: String?): List<String> =
        CratesFeature.crates.map { it.id }.filter { it.startsWith(filter ?: "", ignoreCase = true) }.sorted()


    override fun onTabComplete(
        sender: CommandSender,
        command: Command,
        alias: String,
        args: Array<out String>
    ): MutableList<String> {
        if (args.size < 2) return root(sender, args.getOrNull(0) ?: "")
        return when (args[0]) {
            "new" -> new(sender, args.drop(1))
            "delete" -> delete(sender, args.drop(1))
            "move" -> move(sender, args.drop(1))
            "add" -> add(sender, args.drop(1))
            "remove" -> remove(sender, args.drop(1))
            "view" -> view(sender, args.drop(1))
            "key" -> key(sender, args.drop(1))
            "manage" -> manage(sender, args.drop(1))
            "reload" -> mutableListOf()
            else -> mutableListOf()
        }
    }

    private fun root(sender: CommandSender, filter: String): MutableList<String> {
        val list = mutableListOf<String>()
        if (sender.hasPermission(CratesAdminPermission)) list.add("new")
        if (sender.hasPermission(CratesAdminPermission)) list.add("delete")
        if (sender.hasPermission(CratesAdminPermission)) list.add("move")
        if (sender.hasPermission(CratesAdminPermission)) list.add("add")
        if (sender.hasPermission(CratesAdminPermission)) list.add("remove")
        if (sender.hasPermission(CratesAdminPermission)) list.add("view")
        if (sender.hasPermission(CratesAdminPermission)) list.add("key")
        if (sender.hasPermission(CratesAdminPermission)) list.add("reload")
        list.retainAll { it.startsWith(filter, ignoreCase = true) }
        list.sort()
        return list
    }

    private fun new(sender: CommandSender, args: List<String>): MutableList<String> {
        if (!sender.hasPermission(CratesAdminPermission)) return mutableListOf()
        if (args.size == 1) return mutableListOf("<crate id>")
        if (args.size == 2) return mutableListOf("<crate name>")
        return mutableListOf()
    }

    private fun delete(sender: CommandSender, args: List<String>): MutableList<String> {
        if (!sender.hasPermission(CratesAdminPermission)) return mutableListOf()
        if (args.size > 1) return mutableListOf()
        return crateIds(args.getOrNull(0)).toMutableList()
    }

    private fun move(sender: CommandSender, args: List<String>): MutableList<String> {
        if (!sender.hasPermission(CratesAdminPermission)) return mutableListOf()
        if (args.size > 1) return mutableListOf()
        return crateIds(args.getOrNull(0)).toMutableList()
    }

    private fun add(sender: CommandSender, args: List<String>): MutableList<String> {
        if (!sender.hasPermission(CratesAdminPermission)) return mutableListOf()
        if (args.size > 1) return mutableListOf()
        return crateIds(args.getOrNull(0)).toMutableList()
    }

    private fun remove(sender: CommandSender, args: List<String>): MutableList<String> {
        if (!sender.hasPermission(CratesAdminPermission)) return mutableListOf()
        if (args.size > 1) return mutableListOf()
        return crateIds(args.getOrNull(0)).toMutableList()
    }

    private fun view(sender: CommandSender, args: List<String>): MutableList<String> {
        if (!sender.hasPermission(CratesAdminPermission)) return mutableListOf()
        if (args.size > 1) return mutableListOf()
        return crateIds(args.getOrNull(0)).toMutableList()
    }

    private fun manage(sender: CommandSender, args: List<String>): MutableList<String> {
        if (!sender.hasPermission(CratesAdminPermission)) return mutableListOf()
        if (args.size > 1) return mutableListOf()
        return crateIds(args.getOrNull(0)).toMutableList()
    }

    private fun key(sender: CommandSender, args: List<String>): MutableList<String> {
        if (!sender.hasPermission(CratesAdminPermission)) return mutableListOf()
        return when (args.size) {
            0 -> crateIds("").toMutableList()
            1 -> crateIds(args.getOrNull(0)).toMutableList()
            2 -> Bukkit.getOnlinePlayers().map { it.name }
                .filter { it.startsWith(args.getOrElse(1) { "" }, ignoreCase = true) }.sorted().toMutableList()
            3 -> mutableListOf("#")
            else -> mutableListOf()
        }
    }
}