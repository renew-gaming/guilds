package com.github.snarechops.renew.features.cultshop

import com.github.snarechops.renew.common.*
import com.github.snarechops.renew.features.commands.Help
import com.github.snarechops.renew.features.commands.HelpTopic
import org.bukkit.Bukkit
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import org.bukkit.inventory.ItemStack

@HelpTopic("Cult Shop")
object CultCommand : CommandExecutor {
    override fun onCommand(sender: CommandSender, command: Command, label: String, args: Array<out String>): Boolean {
        if (args.isEmpty()) return MissingSubCommand(sender, CultShopTag)
        return when (args[0].lowercase()) {
            "give" -> give(sender, args.drop(1))
            else -> InvalidCommand(sender, CultShopTag)
        }
    }

    @Help(CultAdminPermission, "$darkBlue/cult give <player> <item> <amount> $black- Gives a player the specified item")
    private fun give(sender: CommandSender, args: List<String>): Boolean {
        if (!sender.hasPermission(CultAdminPermission)) return NotAllowed(sender, CultShopTag)
        if (args.isEmpty()) return err(sender, CultShopTag, "Missing item to give")
        return when (args[0]) {
            "coins" -> giveLoot(sender, ::CultistCoin, args.drop(1))
            "cultist_helm" -> giveLoot(sender, ::CultistHelm, args.drop(1))
            "cultist_chest" -> giveLoot(sender, ::CultistChest, args.drop(1))
            "cultist_legs" -> giveLoot(sender, ::CultistLegs, args.drop(1))
            "cultist_boots" -> giveLoot(sender, ::CultistBoots, args.drop(1))
            "cultist_wings" -> giveLoot(sender, ::CultistWings, args.drop(1))
            "cultist_shield" -> giveLoot(sender, ::CultistShield, args.drop(1))
            "cultist_sword" -> giveLoot(sender, ::CultistSword, args.drop(1))
            else -> err(sender, CultShopTag, "Invalid item")
        }
    }

    private fun giveLoot(sender: CommandSender, loot: (Int) -> ItemStack, args: List<String>): Boolean {
        if (args.isEmpty()) return err(sender, CultShopTag, "Missing target: Choose a player")
        if (args.size < 2) return err(sender, CultShopTag, "Missing amount: Enter an amount to give")
        val player = Bukkit.getPlayer(args[0]) ?: return PlayerNotOnline(sender, CultShopTag)
        val amount = args[1].toInt()
        if (amount < 1) return err(sender, CultShopTag, "Amount must be greater than 0")
        val item = loot(amount)
        player.inventory.addItem(item)
        return ok(sender, CultShopTag, green("Gave"), gold(amount.toString()), *(item.itemMeta?.displayName ?: "").colorize(), green("to"), gold(player.name))    }
}