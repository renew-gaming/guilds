package com.github.snarechops.renew.features.stats

import com.github.snarechops.renew.Feature
import com.github.snarechops.renew.common.CustomEvent
import com.github.snarechops.renew.common.Database
import com.github.snarechops.renew.common.emit
import com.github.snarechops.renew.common.register
import com.mongodb.client.MongoCollection
import com.mongodb.client.model.FindOneAndReplaceOptions
import com.mongodb.client.model.ReturnDocument
import org.bukkit.entity.Player
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.player.PlayerJoinEvent
import org.bukkit.event.player.PlayerQuitEvent
import org.litote.kmongo.*
import java.time.Instant

data class StatIncrement(val player: Player, val key: String, val amount: Int = 1) : CustomEvent()
data class StatSet<T>(val player: Player, val key: String, val value: T) : CustomEvent()
data class StatChanged<T>(val player: Player, val stat: Stat<T>): CustomEvent()

fun <T> StatCollection(): MongoCollection<Stat<T>> =
    Database.getDatabase("renew").getCollection("stat", Stat::class.java) as MongoCollection<Stat<T>>

object StatsFeature : Feature, Listener {
    override fun onInit() {
        this.register()
    }

    override fun onDestroy() {}


    fun <T> getStat(uuid: String, key: String): Stat<T> =
        StatCollection<T>().findOne(and(Stat<T>::uuid eq uuid, Stat<T>::key eq key))
            ?: upsertStat(Stat(uuid, key))!!

    fun <T> getStat(player: Player, key: String): Stat<T> =
        getStat(player.uniqueId.toString(), key)

    fun <T> setStat(player: Player, key: String, value: T): Stat<T> =
        setStat(player.uniqueId.toString(), key, value)

    fun <T> setStat(uuid: String, key: String, value: T): Stat<T> {
        val stat = getStat<T>(uuid, key)
        stat.value = value
        return upsertStat(stat)
    }

    fun <T> upsertStat(stat: Stat<T>) =
        StatCollection<T>().findOneAndReplace(and(Stat<T>::uuid eq stat.uuid, Stat<T>::key eq stat.key), stat, FindOneAndReplaceOptions().upsert(true).returnDocument(ReturnDocument.AFTER))

    fun <T> appendStat(player: Player, key: String, value: T) =
        appendStat(player.uniqueId.toString(), key, value)

    fun <T> appendStat(uuid: String, key: String, value: T): Stat<List<T>> {
        val stat = getStat<List<T>>(uuid, key)
        stat.value = (stat.value ?: listOf()) + value
        return upsertStat(stat)
    }

    fun incrementStat(player: Player, key: String, amount: Int): Stat<Int> =
        incrementStat(player.uniqueId.toString(), key, amount)

    fun incrementStat(uuid: String, key: String, amount: Int): Stat<Int> {
        val stat = getStat<Int>(uuid, key)
        stat.value = (stat.value ?: 0) + amount
        return upsertStat(stat)
    }

    @EventHandler(ignoreCancelled = true)
    fun statIncrement(event: StatIncrement) {
        val stat = incrementStat(event.player, event.key, event.amount)
        emit(StatChanged(event.player, stat))
    }

    @EventHandler(ignoreCancelled = true)
    fun <T> statSet(event: StatSet<T>) {
        val stat = setStat(event.player, event.key, event.value)
        emit(StatChanged(event.player, stat))
    }

    @EventHandler(ignoreCancelled = true)
    fun playerJoin(event: PlayerJoinEvent) =
        appendStat(event.player, "joined_server", Instant.now().toEpochMilli())

    @EventHandler(ignoreCancelled = true)
    fun playerQuit(event: PlayerQuitEvent) =
        appendStat(event.player, "quit_server", Instant.now().toEpochMilli())
}