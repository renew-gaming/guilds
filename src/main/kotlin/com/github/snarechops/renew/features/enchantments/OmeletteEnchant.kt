package com.github.snarechops.renew.features.enchantments

import com.github.snarechops.renew.Renew
import com.github.snarechops.renew.common.blue
import com.github.snarechops.renew.common.err
import com.github.snarechops.renew.common.gold
import org.bukkit.GameMode
import org.bukkit.Material
import org.bukkit.entity.EntityType
import org.bukkit.entity.LivingEntity
import org.bukkit.entity.Player
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.entity.EntityDamageByEntityEvent
import org.bukkit.event.entity.EntityDamageEvent
import org.bukkit.event.entity.EntityDeathEvent
import org.bukkit.inventory.ItemStack
import org.bukkit.inventory.meta.Damageable
import org.bukkit.inventory.meta.ItemMeta
import org.bukkit.metadata.MetadataValue
import org.bukkit.plugin.Plugin

class AllowEgg(val allow: Boolean) : MetadataValue{
    override fun value(): Any = "$allow"
    override fun asInt(): Int = if(allow) 1 else 0
    override fun asFloat(): Float = if(allow)1f else 0f
    override fun asDouble(): Double = if(allow)1.0 else 0.0
    override fun asLong(): Long = if(allow)1 else 0
    override fun asShort(): Short = if (allow)1 else 0
    override fun asByte(): Byte = if(allow)0x1 else 0x0
    override fun asBoolean(): Boolean = allow
    override fun asString(): String = "$allow"
    override fun getOwningPlugin(): Plugin = Renew
    override fun invalidate() {}
}

object OmeletteEnchant: Listener, CustomEnchant(
    "omelette",
    "Omelette",
    1,
    1,
    blue
) {
    override fun description(level: Int): List<String> = listOf(
        "$gold- Killing a creature drops it's spawn egg",
        "$gold- The creature must receive damage from only",
        "$gold- this enchantment to produce an egg and this",
        "$gold- must not have any other enchantments",
    )

    override fun canEnchantItem(item: ItemStack): Boolean = item.type == Material.WOODEN_SWORD

    fun blacklist(entity: LivingEntity, player: Player) {
        err(player, EnchantTag, "This entity has received damage from an invalid source. No egg will be produced")
        entity.setMetadata("AllowEgg", AllowEgg(false))
    }

    fun invalidMob(entity: LivingEntity, player: Player) {
        err(player, EnchantTag, "This entity is not available in spawn egg form on this server. No egg will be produced")
        entity.setMetadata("AllowEgg", AllowEgg(false))
    }

    @EventHandler(ignoreCancelled = true)
    fun onDamageEntity(event: EntityDamageByEntityEvent) {
        // Must be a living entity
        if (event.entity !is LivingEntity) return
        val entity = event.entity as LivingEntity
        // Must be a player who dealt the damage
        if (event.damager !is Player) return
        val player = event.damager as Player
        // Must have this enchantment
        val tool = player.inventory.itemInMainHand
        if (has(tool) == 0) return
        // Must not be blacklisted
        val allowEgg = entity.getMetadata("AllowEgg").getOrNull(0)?.asBoolean()
        println(allowEgg.toString())
        if (allowEgg == false) return
        // Must be one of the allowed mobs
        if(!AllowedMobs.containsKey(event.entityType)) return invalidMob(entity, player)
        // Must be in survival
        if (player.gameMode != GameMode.SURVIVAL) return blacklist(entity, player)
        // Must have received it's last damage from an entity attack
        val lastDamage = entity.lastDamageCause?.cause ?: EntityDamageEvent.DamageCause.ENTITY_ATTACK
        println("$lastDamage")
        if (lastDamage != EntityDamageEvent.DamageCause.ENTITY_ATTACK || lastDamage != EntityDamageEvent.DamageCause.ENTITY_ATTACK) return blacklist(entity, player)
        // Must be allowed to drop an egg if not full health
        @Suppress("DEPRECATION")
        println("$allowEgg ${entity.health} ${entity.maxHealth}")
        @Suppress("DEPRECATION")
        if (allowEgg == null && entity.health != entity.maxHealth) return blacklist(entity, player)
        // Lower damage to 5
        event.damage = 5.0
        entity.setMetadata("AllowEgg", AllowEgg(true))
        // Repair the sword
        val meta = player.inventory.itemInMainHand.itemMeta as Damageable? ?: return
        meta.damage = 0
        player.inventory.itemInMainHand.itemMeta = meta as ItemMeta
    }

    @EventHandler(ignoreCancelled = true)
    fun onEntityDeath(event: EntityDeathEvent) {
        if (event.entity.getMetadata("AllowEgg").getOrNull(0)?.asBoolean() != true) return
        event.drops.clear()
        AllowedMobs[event.entityType]?.let{
            event.drops.add(ItemStack(it))
        }
    }
}

private val AllowedMobs = mutableMapOf(
    EntityType.BAT to Material.BAT_SPAWN_EGG,
    EntityType.BLAZE to Material.BLAZE_SPAWN_EGG,
    EntityType.CAT to Material.CAT_SPAWN_EGG,
    EntityType.CAVE_SPIDER to Material.CAVE_SPIDER_SPAWN_EGG,
    EntityType.COW to Material.COW_SPAWN_EGG,
    EntityType.CREEPER to Material.CREEPER_SPAWN_EGG,
    EntityType.DOLPHIN to Material.DOLPHIN_SPAWN_EGG,
    EntityType.DONKEY to Material.DONKEY_SPAWN_EGG,
    EntityType.DROWNED to Material.DROWNED_SPAWN_EGG,
    EntityType.ENDERMAN to Material.ENDERMAN_SPAWN_EGG,
    EntityType.ENDERMITE to Material.ENDERMITE_SPAWN_EGG,
    EntityType.EVOKER to Material.EVOKER_SPAWN_EGG,
    EntityType.FOX to Material.FOX_SPAWN_EGG,
    EntityType.GHAST to Material.GHAST_SPAWN_EGG,
    EntityType.GLOW_SQUID to Material.GLOW_SQUID_SPAWN_EGG,
    EntityType.GOAT to Material.GOAT_SPAWN_EGG,
    EntityType.GUARDIAN to Material.GUARDIAN_SPAWN_EGG,
    EntityType.HOGLIN to Material.HOGLIN_SPAWN_EGG,
    EntityType.HORSE to Material.HORSE_SPAWN_EGG,
    EntityType.HUSK to Material.HUSK_SPAWN_EGG,
    EntityType.LLAMA to Material.LLAMA_SPAWN_EGG,
    EntityType.MAGMA_CUBE to Material.MAGMA_CUBE_SPAWN_EGG,
    EntityType.MUSHROOM_COW to Material.MOOSHROOM_SPAWN_EGG,
    EntityType.MULE to Material.MULE_SPAWN_EGG,
    EntityType.OCELOT to Material.OCELOT_SPAWN_EGG,
    EntityType.PANDA to Material.PANDA_SPAWN_EGG,
    EntityType.PARROT to Material.PARROT_SPAWN_EGG,
    EntityType.PHANTOM to Material.PHANTOM_SPAWN_EGG,
    EntityType.PIG to Material.PIG_SPAWN_EGG,
    EntityType.PIGLIN to Material.PIGLIN_SPAWN_EGG,
    EntityType.PIGLIN_BRUTE to Material.PIGLIN_BRUTE_SPAWN_EGG,
    EntityType.PILLAGER to Material.PILLAGER_SPAWN_EGG,
    EntityType.POLAR_BEAR to Material.POLAR_BEAR_SPAWN_EGG,
    EntityType.RABBIT to Material.RABBIT_SPAWN_EGG,
    EntityType.RAVAGER to Material.RAVAGER_SPAWN_EGG,
    EntityType.SHEEP to Material.SHEEP_SPAWN_EGG,
    EntityType.SHULKER to Material.SHULKER_SPAWN_EGG,
    EntityType.SKELETON to Material.SKELETON_SPAWN_EGG,
    EntityType.SKELETON_HORSE to Material.SKELETON_HORSE_SPAWN_EGG,
    EntityType.SLIME to Material.SLIME_SPAWN_EGG,
    EntityType.SPIDER to Material.SPIDER_SPAWN_EGG,
    EntityType.SQUID to Material.SQUID_SPAWN_EGG,
    EntityType.STRAY to Material.STRAY_SPAWN_EGG,
    EntityType.STRIDER to Material.STRIDER_SPAWN_EGG,
    EntityType.TRADER_LLAMA to Material.TRADER_LLAMA_SPAWN_EGG,
    EntityType.TURTLE to Material.TURTLE_SPAWN_EGG,
    EntityType.VEX to Material.VEX_SPAWN_EGG,
    EntityType.VILLAGER to Material.VILLAGER_SPAWN_EGG,
    EntityType.VINDICATOR to Material.VINDICATOR_SPAWN_EGG,
    EntityType.WITCH to Material.WITCH_SPAWN_EGG,
    EntityType.WITHER_SKELETON to Material.WITHER_SKELETON_SPAWN_EGG,
    EntityType.WOLF to Material.WOLF_SPAWN_EGG,
    EntityType.ZOGLIN to Material.ZOGLIN_SPAWN_EGG,
    EntityType.ZOMBIE to Material.ZOMBIE_SPAWN_EGG,
    EntityType.ZOMBIE_VILLAGER to Material.ZOMBIE_VILLAGER_SPAWN_EGG,
    EntityType.ZOMBIFIED_PIGLIN to Material.ZOMBIFIED_PIGLIN_SPAWN_EGG,
)