package com.github.snarechops.renew.features.crates

import com.github.snarechops.renew.Feature
import com.github.snarechops.renew.common.*
import com.mongodb.client.MongoCollection
import com.mongodb.client.model.ReplaceOptions
import org.bukkit.Material
import org.bukkit.entity.Player
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.inventory.InventoryClickEvent
import org.bukkit.event.inventory.InventoryCloseEvent
import org.bukkit.event.player.PlayerInteractEvent
import org.litote.kmongo.eq
import org.litote.kmongo.findOne
import org.litote.kmongo.setValue

fun CrateViewTitle(crate: Crate) = "${lightPurple}${crate.name}"
fun CrateRemoveTitle(crate: Crate) = "${lightPurple}Crate: ${crate.id} ${lightPurple}Choose an item to remove"
fun CrateManageTitle(crate: Crate) = "${lightPurple}Manage: ${crate.id}"
val CratesTag = pluginTag("Crates")

const val CratesAdminPermission = "crates.admin"

val CratesCollection: MongoCollection<Crate>
    get() = Database.getDatabase("renew").getCollection("crate", Crate::class.java)

object CratesFeature : Feature, Listener {
    val crates = mutableListOf<Crate>()
    private val pendingCrates = mutableMapOf<Player, Crate>()

    override fun onInit() {
        reloadCrates()
        CratesCommand.register("crates", CratesCompletion)
        this.register()
    }

    override fun onDestroy() {}

    fun createCrate(id: String, name: String, player: Player): Boolean {
        val crate = Crate(id, name, SimpleLocation("world", 0, 0, 0), "")
        pendingCrates[player] = crate
        crates.add(crate)
        return true
    }

    fun deleteCrate(id: String): Boolean {
        CratesCollection.deleteOne(Crate::id eq id) ?: return false
        crates.removeIf { it.id == id }
        return true
    }

    fun moveCrate(id: String, player: Player): Boolean {
        val crate = CratesCollection.findOne(Crate::id eq id) ?: return false
        pendingCrates[player] = crate
        return true
    }

    fun addItem(id: String, player: Player): Boolean {
        try {
            val crate = crates.find { it.id == id } ?: return false
            val item = player.inventory.itemInMainHand
            if (item.type == Material.AIR) return false
            val items = crate.contents.decodeItemStacks().toMutableList()
            items.add(item)
            CratesCollection.updateOne(Crate::id eq crate.id, setValue(Crate::contents, items.encodeString()))
                ?: return false
            reloadCrates()
            return true
        } catch (err: Error) {
            err(player, CratesTag, err.message ?: "")
            throw err
        }
    }

    fun removeItem(id: String, player: Player): Boolean {
        val crate = crates.find { it.id == id } ?: return false
        val inventory = newInventory(crate.contents, 27, CrateRemoveTitle(crate))
        player.openInventory(inventory)
        return true
    }

    fun viewCrate(id: String, player: Player): Boolean{
        val crate = crates.find { it.id == id } ?: return false
        viewCrate(player, crate)
        return true
    }

    fun manageCrate(id: String, player: Player): Boolean {
        val crate = crates.find { it.id == id } ?: return false
        val inventory = newInventory(crate.contents, 27, CrateManageTitle(crate))
        player.openInventory(inventory)
        return true
    }

    fun giveKey(id: String, player: Player, amount: Int): Boolean {
        val crate = crates.find { it.id == id } ?: return false
        val key = KeyFor(crate, amount)
        key.give(player)
        return true
    }

    private fun viewCrate(player: Player, crate: Crate) {
        val inventory = newInventory(crate.contents, 27, CrateViewTitle(crate))
        player.openInventory(inventory)
    }

    private fun reward(player: Player, crate: Crate) {
        val item = crate.contents.decodeItemStacks().randomOrNull() ?: return
        val result = player.inventory.addItem(item)
        if (result.isNotEmpty()) {
            err(player, CratesTag, "Not enough space in inventory for this reward")
            return
        }
        ok(player, CratesTag, green("You have received"), *item.name.colorize())
    }

    fun reloadCrates() {
        crates.clear()
        CratesCollection.find().forEach { crates.add(it) }
    }

    @EventHandler(ignoreCancelled = true)
    fun onSetCrateLocation(event: PlayerInteractEvent) {
        try {
            if (!pendingCrates.containsKey(event.player)) return
            val location = event.clickedBlock?.location ?: return
            val crate = pendingCrates[event.player] ?: return
            crate.location = location.toSimpleLocation()
            CratesCollection.replaceOne(Crate::id eq crate.id, crate, ReplaceOptions().upsert(true)) ?: return
            pendingCrates.remove(event.player)
            reloadCrates()
            event.isCancelled = true
            ok(event.player, CratesTag, green("Crate"), *crate.name.colorize(), green("location updated"))
        } catch (err: Error) {
            err(event.player, CratesTag, "Failed to update crate location")
            event.isCancelled = true
            throw err
        }
    }

    @EventHandler(ignoreCancelled = true)
    fun onOpenCrate(event: PlayerInteractEvent) {
        val location = event.clickedBlock?.location ?: return
        val crate = crates.find { it.location.same(location) } ?: return
        val mainHand = event.player.inventory.itemInMainHand
        if (!mainHand.isKeyFor(crate)) viewCrate(event.player, crate)
        else {
            reward(event.player, crate)
            event.player.inventory.itemInMainHand.amount -= 1
        }
        event.isCancelled = true
    }

    @EventHandler(ignoreCancelled = true)
    fun onCrateView(event: InventoryClickEvent) {
        if (crates.any { event.view.isCrateView(it) }) event.isCancelled = true
    }

    @EventHandler(ignoreCancelled = true)
    fun onManageCrate(event: InventoryCloseEvent) {
        try {
            val crate = crates.find { event.view.isCrateManage(it) } ?: return
            val items = event.inventory.storageContents.encodeString()
            CratesCollection.updateOne(Crate::id eq crate.id, setValue(Crate::contents, items)) ?: return
            reloadCrates()
        } catch(err: Error) {
            err(event.player, CratesTag, err.message ?: "")
            throw err
        }
    }

    @EventHandler(ignoreCancelled = true)
    fun onRemoveCrateItem(event: InventoryClickEvent) {
        try {
            val crate = crates.find { event.view.isCrateRemoval(it) } ?: return
            event.isCancelled = true
            val item = event.currentItem ?: return
            val items = crate.items().toMutableList()
            items.removeIf { it == item }
            CratesCollection.updateOne(Crate::id eq crate.id, setValue(Crate::contents, items.encodeString())) ?: return
            reloadCrates()
            event.view.close()
        } catch (err: Error) {
            err(event.whoClicked, CratesTag, "Failed to remove item from crate")
            event.isCancelled = true
            throw err
        }
    }
}