package com.github.snarechops.renew.features.enchantments

import com.github.snarechops.renew.common.blue
import com.github.snarechops.renew.common.gold
import com.github.snarechops.renew.common.isHelmet
import org.bukkit.entity.Player
import org.bukkit.inventory.ItemStack
import org.bukkit.potion.PotionEffect
import org.bukkit.potion.PotionEffectType

object MiningLampEnchant : PassiveEnchant, CustomEnchant(
    "mining_lamp",
    "Mining Lamp",
    1,
    1,
    blue,
) {
    override fun description(level: Int): List<String> = listOf(
        "$gold- Grants Night Vision while wearing",
    )

    private fun effect(): PotionEffect = PotionEffect(PotionEffectType.NIGHT_VISION, 30 * 20, 1)

    override fun canEnchantItem(item: ItemStack): Boolean = item.isHelmet()

    override fun tick(player: Player, level: Int) {
        player.addPotionEffect(effect())
    }
}