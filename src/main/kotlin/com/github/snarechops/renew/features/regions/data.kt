package com.github.snarechops.renew.features.regions

import com.github.snarechops.renew.common.SimpleLocation
import kotlinx.serialization.Serializable
import org.bukkit.Material
import org.bukkit.entity.Monster

@Serializable
data class Flags(
    var playerPlace: List<Material>?,
    var playerBreak: List<Material>?,
    var monsterSpawn: List<Monster>?,
    var monsterDamage: Boolean = true,
    var playerDamage: Boolean = true,
)

@Serializable
data class Area(
    val min: SimpleLocation,
    val max: SimpleLocation,
)