package com.github.snarechops.renew.features.hide

import org.bukkit.command.Command
import org.bukkit.command.CommandSender
import org.bukkit.command.TabCompleter
import org.bukkit.entity.Player

object HideCompletion: TabCompleter {
    override fun onTabComplete(
        sender: CommandSender,
        command: Command,
        alias: String,
        args: Array<out String>
    ): MutableList<String> {
        if (sender !is Player) return mutableListOf()
        if (!sender.hasPermission(HidePermission)) return mutableListOf()
        return mutableListOf("hide", "show")
    }
}