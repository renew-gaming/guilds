package com.github.snarechops.renew.features.chess

import org.bukkit.Location
import org.bukkit.entity.LivingEntity
import org.bukkit.entity.Player
import org.bukkit.event.EventHandler
import org.bukkit.event.HandlerList
import org.bukkit.event.Listener
import org.bukkit.event.entity.EntityDamageByEntityEvent
import org.bukkit.event.player.PlayerInteractAtEntityEvent
import org.bukkit.event.player.PlayerInteractEntityEvent

typealias SpawnFunc = (location: Location) -> LivingEntity?

abstract class Piece(val owner: Player, val team: Team, var spot: Spot, val spawner: SpawnFunc) : Listener {
    var entity: LivingEntity? = null
    val location: Location
        get() = spot.location
    val board: Board
        get() = spot.board

    val yaw: Float = when (team) {
        Team.WHITE -> 0f
        Team.BLACK -> 180f
    }

    fun spawn() {
        entity = spawner(location)
        entity?.setRotation(yaw, 0f)
        board.pieces += this
    }

    abstract fun moves(): List<Spot>

    fun highlight(bool: Boolean) {
        entity?.isGlowing = bool
    }

    open fun move(end: Spot): Boolean {
        // Save current spot for potential revert
        val from = this.spot
        // If the spot is occupied, save for later
        val capture = end.piece
        // If this move doesn't exist for this piece, cancel the move
        if (!moves().contains(end)) return false
        // Make the move in-memory
        end.piece = this
        this.spot.piece = null
        spot = end
        // Does move result in check?
        if (board.check(team)) {
            // If the move results in check, revert the move
            end.piece = null
            from.piece = this
            spot = from
            // Don't highlight the new spot, as the piece is not actually there
            end.highlightCheck(false)
            return false
        }
        // Make the in-game move
        if (entity?.teleport(end.location) != true) return false
        entity!!.setRotation(yaw, 0f)
        // Perform capture animation if needed
        capture?.kill()
        // Move complete
        return true
    }

    fun kill() {
        HandlerList.unregisterAll(this)
        this.entity?.health = 0.0
        board.pieces = board.pieces.filterNot { it == this }
    }

    fun isEnemy(team: Team) = this.team == team.opposite()

    fun walk(modX: Int, modY: Int, distance: Int = 8): List<Spot>{
        val results = mutableListOf<Spot>()
        var next: Spot? = spot.getRelative(modX, modY)
        var dist = distance
        while(true) {
            if (next == null) break
            if (dist == 0) break
            if (next.hasTeam(team)) break
            if (next.hasTeam(team.opposite())){
                if (this is Pawn) break
                results.add(next)
                break
            }
            results.add(next)
            next = next.getRelative(modX, modY)
            dist--
        }
        return results
    }

    @EventHandler(ignoreCancelled = true)
    fun onHit(event: EntityDamageByEntityEvent) {
        if (event.entity != entity) return
        event.isCancelled = true
        if (event.damager != board.turn) return
        if (event.damager != owner) board.select(spot)
        else board.select(this)
    }

    @EventHandler(ignoreCancelled = true)
    fun onSelect(event: PlayerInteractAtEntityEvent) {
        if (event.rightClicked != entity) return
        event.isCancelled = true
        if (event.player != board.turn) return
        if (event.player != owner) board.select(spot)
        else board.select(this)
    }

    @EventHandler(ignoreCancelled = true)
    fun onInteract(event: PlayerInteractEntityEvent) {
        if (event.rightClicked != entity) return
        event.isCancelled = true
    }
}