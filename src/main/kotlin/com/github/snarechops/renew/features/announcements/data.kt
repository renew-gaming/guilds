package com.github.snarechops.renew.features.announcements

import kotlinx.serialization.Serializable
import kotlinx.serialization.Transient
import org.bson.codecs.pojo.annotations.BsonId
import org.litote.kmongo.Id
import java.time.Instant

@Serializable
data class Announcement(
    @BsonId
    val id: Id<Announcement>,
    val message: String,
    var period: Int,
    var enabled: Boolean,
    @Transient var timestamp: Instant = Instant.now(),
)