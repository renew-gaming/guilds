package com.github.snarechops.renew.features.preferences

import com.github.snarechops.renew.*
import com.github.snarechops.renew.common.Database
import com.github.snarechops.renew.common.debug
import com.github.snarechops.renew.common.register
import com.mongodb.client.MongoCollection
import com.mongodb.client.model.FindOneAndReplaceOptions
import com.mongodb.client.model.ReplaceOptions
import org.bukkit.entity.Player
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.player.PlayerQuitEvent
import org.litote.kmongo.eq
import org.litote.kmongo.findOne

private val cache: MutableMap<Player, Preferences> = mutableMapOf()

val PreferenceCollection: MongoCollection<Preferences>
    get() = Database.getDatabase("guilds").getCollection("preference", Preferences::class.java)

object PreferenceFeature : Feature, Listener {
    override fun onInit() {
        this.register()
    }

    override fun onDestroy() {
        cache.values.forEach {
            PreferenceCollection.findOneAndReplace(Preferences::uuid eq it.uuid, it, FindOneAndReplaceOptions().upsert(true))
        }
    }

    @EventHandler(ignoreCancelled = true)
    fun onPlayerQuit(event: PlayerQuitEvent){
        debug("Cache: ${cache[event.player]}")
        if (cache[event.player] != null)
            PreferenceCollection.replaceOne(Preferences::uuid eq event.player.uniqueId.toString(), cache[event.player]!!, ReplaceOptions().upsert(true))
    }
}

fun Player.preference(key: String): String? {
    if (!cache.containsKey(this)) this.cachePreferences()
    return cache[this]?.pairs?.get(key)
}

fun Player.preference(key: String, value: String){
    if (cache[this] == null) cache[this] = Preferences(this.uniqueId.toString(), this.name, mutableMapOf(key to value))
    else cache[this]?.pairs?.set(key, value)
}

private fun Player.cachePreferences() {
    val value = PreferenceCollection.findOne(Preferences::uuid eq this.uniqueId.toString())
    if (value != null) cache[this] = value
}