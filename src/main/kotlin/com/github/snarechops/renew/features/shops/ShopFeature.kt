package com.github.snarechops.renew.features.shops

import com.github.snarechops.renew.Feature
import com.github.snarechops.renew.common.*
import com.github.snarechops.renew.features.crates.name
import com.mongodb.client.MongoCollection
import org.bukkit.block.Sign
import org.bukkit.entity.Player
import org.bukkit.inventory.ItemStack
import org.litote.kmongo.eq
import org.litote.kmongo.findOne

const val ShopsAdminPermission = "shops.admin"
const val SHOP_SIGN_TITLE = "[Shop]"
val ShopsTag = pluginTag("Shops")

val ShopCollection: MongoCollection<Shop>
    get() = Database.getDatabase("renew").getCollection("shop", Shop::class.java)

object ShopFeature: Feature {
    override fun onInit() {
        ShopCommand.register("shop", ShopCompletion)
    }

    override fun onDestroy() {}

    fun getShop(coord: SimpleLocation) =
        ShopCollection.findOne(Shop::location eq coord)

    fun setShopProduct(player: Player, sign: Sign, shop: Shop, item: ItemStack, amount: Int): Boolean {
        shop.product = item.let { it.amount = amount; it }.encodeString()
        sign.setLine(1, "$amount ${item.name}")
        return ok(player, ShopsTag, green("Shop product set to"), gold(amount.toString()), *item.name.colorize())
    }



}