package com.github.snarechops.renew.features.crates

import com.github.snarechops.renew.common.*
import com.github.snarechops.renew.features.commands.Help
import com.github.snarechops.renew.features.commands.HelpTopic
import org.bukkit.Bukkit
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player

@HelpTopic("Crates")
object CratesCommand : CommandExecutor {
    override fun onCommand(sender: CommandSender, command: Command, label: String, args: Array<out String>): Boolean =
        when (args.getOrNull(0)?.lowercase()) {
            "new" -> new(sender, args.drop(1))
            "delete" -> delete(sender, args.drop(1))
            "move" -> move(sender, args.drop(1))
            "add" -> add(sender, args.drop(1))
            "remove" -> remove(sender, args.drop(1))
            "view" -> view(sender, args.drop(1))
            "manage" -> manage(sender, args.drop(1))
            "key" -> key(sender, args.drop(1))
            "reload" -> reload(sender)
            null -> MissingSubCommand(sender, CratesTag)
            else -> InvalidCommand(sender, CratesTag)
        }

    @Help(CratesAdminPermission, "$darkBlue/crates new <id> <name...> $black- Creates a new Crate with the provided id and name")
    private fun new(sender: CommandSender, args: List<String>): Boolean {
        if (!sender.hasPermission(CratesAdminPermission)) return NotAllowed(sender, CratesTag)
        if (sender !is Player) return MustBePlayer(sender, CratesTag)
        val id = args.getOrNull(0) ?: return err(sender, CratesTag,"Missing crate id")
        if (args.size < 2) return err(sender, CratesTag, "Missing crate name")
        val name = args.drop(1).joinToString(" ").replace("&", "§")
        if (!CratesFeature.createCrate(id, name, sender)) return err(sender, CratesTag, "Failed to create crate")
        return ok(sender, CratesTag, green("Crate"), *name.colorize(), green("created. Click a block to set the crate's location"))
    }

    @Help(CratesAdminPermission, "$darkBlue/crates delete <id> $black- Permanently deletes a crate")
    private fun delete(sender: CommandSender, args: List<String>): Boolean {
        if (!sender.hasPermission(CratesAdminPermission)) return NotAllowed(sender, CratesTag)
        val id = args.getOrNull(0) ?: return err(sender, CratesTag, "Missing crate id")
        if (!CratesFeature.deleteCrate(id)) return err(sender, CratesTag, "Failed to delete crate")
        return ok(sender, CratesTag, green("Crate"), gold(id), green("deleted"))
    }

    @Help(CratesAdminPermission, "$darkBlue/crates move <id> $black- Moves a crates location. Run command then interact with a new block to move.")
    private fun move(sender: CommandSender, args: List<String>): Boolean {
        if (!sender.hasPermission(CratesAdminPermission)) return NotAllowed(sender, CratesTag)
        if (sender !is Player) return MustBePlayer(sender, CratesTag)
        val id = args.getOrNull(0) ?: return err(sender, CratesTag, "Missing crate id")
        if (!CratesFeature.moveCrate(id, sender)) return err(sender, CratesTag, "Failed to move crate")
        return ok(sender, CratesTag, "Click a block to set the crate's location")
    }

    @Help(CratesAdminPermission, "$darkBlue/crates add <id> $black- Add the item in your hand to the specified crate.")
    private fun add(sender: CommandSender, args: List<String>): Boolean {
        if (!sender.hasPermission(CratesAdminPermission)) return NotAllowed(sender, CratesTag)
        if (sender !is Player) return MustBePlayer(sender, CratesTag)
        val id = args.getOrNull(0) ?: return err(sender, CratesTag, "Missing crate id")
        if (!CratesFeature.addItem(id, sender)) return err(sender, CratesTag, "Failed to add item to crate")
        return ok(sender, CratesTag, "Added item to crate")
    }

    @Help(CratesAdminPermission, "$darkBlue/crates remove <id> $black- Open the specified crate and remove the item you click.")
    private fun remove(sender: CommandSender, args: List<String>): Boolean {
        if (!sender.hasPermission(CratesAdminPermission)) return NotAllowed(sender, CratesTag)
        if (sender !is Player) return MustBePlayer(sender, CratesTag)
        val id = args.getOrNull(0) ?: return err(sender, CratesTag, "Missing crate id")
        if (!CratesFeature.removeItem(id, sender)) return err(sender, CratesTag, "Crate $id not found")
        return true
    }

    @Help(CratesAdminPermission, "$darkBlue/crates view <id> $black- Open a read only view of the specified crate.")
    private fun view(sender: CommandSender, args: List<String>): Boolean {
        if (!sender.hasPermission(CratesAdminPermission)) return NotAllowed(sender, CratesTag)
        if (sender !is Player) return MustBePlayer(sender, CratesTag)
        val id = args.getOrNull(0) ?: return err(sender, CratesTag, "Missing crate id")
        if (!CratesFeature.viewCrate(id, sender)) return err(sender, CratesTag, "Crate $id not found")
        return true
    }

    @Help(CratesAdminPermission, "$darkBlue/crates manage <id> $black- Open the specified crate for editing.")
    private fun manage(sender: CommandSender, args: List<String>): Boolean {
        if (!sender.hasPermission(CratesAdminPermission)) return NotAllowed(sender, CratesTag)
        if (sender !is Player) return MustBePlayer(sender, CratesTag)
        val id = args.getOrNull(0) ?: return err(sender, CratesTag, "Missing crate id")
        if (!CratesFeature.manageCrate(id, sender)) return err(sender, CratesTag, "Crate $id not found")
        return true
    }

    @Help(CratesAdminPermission, "$darkBlue/crates key <id> <player> <amount> $black- Give the player crate key(s) for the specified crate")
    private fun key(sender: CommandSender, args: List<String>): Boolean {
        if (!sender.hasPermission(CratesAdminPermission)) return NotAllowed(sender, CratesTag)
        val id = args.getOrNull(0) ?: return err(sender, CratesTag, "Missing crate id")
        val player = Bukkit.getPlayer(args.getOrNull(1) ?: "") ?: return PlayerNotOnline(sender, CratesTag)
        val amount = args.getOrNull(2)?.toInt() ?: 1
        if (!CratesFeature.giveKey(id, player, amount)) return err(sender, CratesTag, "Crate $id not found")
        ok(player, CratesTag, green("You have received"), gold(amount.toString()), *id.colorize(), green(if (amount > 1) "Keys" else "Key"))
        return ok(sender, CratesTag, green("You have given"), gold(player.name), blue(amount.toString()), *id.colorize(), green(if (amount > 1) "Keys" else "Key"))
    }

    @Help(CratesAdminPermission, "$darkBlue/crates reload <id> $black- Reload all crates from the database.")
    private fun reload(sender: CommandSender): Boolean {
        if(!sender.hasPermission(CratesAdminPermission)) return NotAllowed(sender, CratesTag)
        CratesFeature.reloadCrates()
        return ok(sender, CratesTag, "Crates reloaded")
    }
}