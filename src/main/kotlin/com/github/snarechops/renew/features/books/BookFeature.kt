package com.github.snarechops.renew.features.books

import com.github.snarechops.renew.Feature
import com.github.snarechops.renew.common.emit
import com.github.snarechops.renew.common.nextTick
import com.github.snarechops.renew.common.register
import com.github.snarechops.renew.features.preferences.preference
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.player.PlayerJoinEvent

object BookFeature : Feature, Listener {

    override fun onInit() {
        this.register()
    }

    override fun onDestroy() {}

    @EventHandler
    fun onShowBook(event: ShowBookEvent) {
        val slot = event.player.inventory.heldItemSlot
        val item = event.player.inventory.getItem(slot)
        event.player.openBook(event.book)
        nextTick { event.player.inventory.setItem(slot, item) }
    }

    @EventHandler
    fun onPlayerJoin(event: PlayerJoinEvent) {
        if (event.player.preference("welcomed") != "true") {
            emit(ShowBookEvent(event.player, WelcomeBook))
            event.player.preference("welcomed", "true")
        }
    }
}