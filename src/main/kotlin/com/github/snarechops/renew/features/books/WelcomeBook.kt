package com.github.snarechops.renew.features.books

import com.github.snarechops.renew.common.*

val WelcomeBook = Book("Guilds", "Guilds Welcome", listOf(
    "$darkGreen${underline}Guilds 2.0\n\n"
            + "${darkAqua}Guilds has been rewritten from the ground up to improve upon the previous versions in every way. New quests and new commands have been added / reworked to improve your experience, as well as bug fixes and other improvements."
    , "$darkGreen${underline}New Commands\n\n"
            + "${darkAqua}New commands have been added, and some have been reworked to make them easier to work with and less cluttered. Additionally there is a new ${darkGreen}/commands ${darkAqua}command to open a book with command help and examples\n\n"
    , "$darkGreen${underline}New Quests\n\n"
            + "${darkAqua}New quests have been added that now are turned in at their respective guild, and the items are then available in the guild shop\n\n"
    , "$darkGreen${underline}Quest Point Shop\n\n"
            + "${darkAqua}A new shop is available for you to spend the points you earn on quests, use $darkBlue/quest shop ${darkAqua}to access this new shop after accruing some points through the new quests"
    , "$darkGreen${underline}Guild Shops\n\n"
            + "${darkAqua}At each guild location, there are now signs for getting quests, turning in quests, and purchasing items from the shop. These items are player generated, stock is only available once a player has completed a quest and deposits those items\n\n"
    , "$darkGreen${underline}Cult Shop\n\n"
            + "${darkAqua}The cult shop has been condensed into a sign that will open a GUI and display the items, allowing you to see their details and price before purchasing.\n\n"
            + "${darkAqua}Cultists have now been moved to an arena and no longer spawn on Blood Moons. If you would like to farm Cult Coins, visit the arena.\n\n"
    , "$gold${underline}Thank You!\n\n"
            + "${darkAqua}On behalf of Renew, thank you for playing and enjoying this new permanent expansion. Feedback is always appreciated as we are always trying to improve your gameplay experience.\n\n"
            + "${darkAqua}- Snare, Rob, and the Renew Community!"
))