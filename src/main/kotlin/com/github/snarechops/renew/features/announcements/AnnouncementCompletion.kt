package com.github.snarechops.renew.features.announcements

import org.bukkit.command.Command
import org.bukkit.command.CommandSender
import org.bukkit.command.TabCompleter

object AnnouncementCompletion: TabCompleter {
    override fun onTabComplete(
        sender: CommandSender,
        command: Command,
        alias: String,
        args: Array<out String>
    ): MutableList<String> = when {
        args.isEmpty() -> root(sender, "")
        args.size == 1 -> root(sender, args[0])
        args[0] == "list" -> mutableListOf()
        args[0] == "reload" -> mutableListOf()
        args[0] == "create" -> create(sender, args.drop(1))
        args[0] == "set" -> set(sender, args.drop(1))
        args[0] == "delete" -> delete(sender, args.drop(1))
        args[0] == "trigger" -> trigger(sender, args.drop(1))
        else -> mutableListOf()
    }

    private fun root(sender: CommandSender, filter: String): MutableList<String> {
        val list = mutableListOf("list")
        if (sender.hasPermission(AnnouncementAdminPermission)) {
            list.add("trigger")
            list.add("reload")
            list.add("create")
            list.add("set")
            list.add("delete")
        }
        return list.sorted().filter { it.startsWith(filter, ignoreCase = true) }.toMutableList()
    }

    private fun create(sender: CommandSender, args: List<String>): MutableList<String> {
        if (!sender.hasPermission(AnnouncementAdminPermission)) return mutableListOf()
        return when (args.size) {
            1 -> mutableListOf("<period in minutes>")
            2 -> mutableListOf("<message...>")
            else -> mutableListOf()
        }.sorted().filter { it.startsWith(args.last(), ignoreCase = true) }.toMutableList()
    }

    private fun set(sender: CommandSender, args: List<String>): MutableList<String> {
        if (!sender.hasPermission(AnnouncementAdminPermission)) return mutableListOf()
        return when (args.size) {
            1 -> AnnouncementFeature.announcements.mapIndexed { i, _ -> i.toString() }.sorted().filter { it.startsWith(args.last(), ignoreCase = true) }.toMutableList()
            2 -> mutableListOf("period", "enabled").sorted().filter { it.startsWith(args.last(), ignoreCase = true) }.toMutableList()
            3 -> when (args[1]) {
                "period" -> mutableListOf("<period in minutes>").filter { it.startsWith(args.last(), ignoreCase = true) }.toMutableList()
                "enabled" -> mutableListOf("true", "false").filter { it.startsWith(args.last(), ignoreCase = true) }.toMutableList()
                else -> mutableListOf()
            }
            else -> mutableListOf()
        }
    }

    private fun delete(sender: CommandSender, args: List<String>): MutableList<String> {
        if (!sender.hasPermission(AnnouncementAdminPermission)) return mutableListOf()
        return when(args.size) {
            1 -> AnnouncementFeature.announcements.mapIndexed { i, _ -> i.toString() }.sorted().filter { it.startsWith(args.last(), ignoreCase = true) }.toMutableList()
            else -> mutableListOf()
        }
    }

    private fun trigger(sender: CommandSender, args: List<String>): MutableList<String> {
        if (!sender.hasPermission(AnnouncementAdminPermission)) return mutableListOf()
        return when(args.size) {
            1 -> AnnouncementFeature.announcements.mapIndexed { i, _ -> i.toString() }.sorted().filter { it.startsWith(args.last(), ignoreCase = true) }.toMutableList()
            else -> mutableListOf()
        }
    }
}