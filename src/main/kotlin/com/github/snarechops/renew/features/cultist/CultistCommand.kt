package com.github.snarechops.renew.features.cultist

import com.github.snarechops.renew.common.*
import com.github.snarechops.renew.features.commands.Help
import com.github.snarechops.renew.features.commands.HelpTopic
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender

@HelpTopic("Cultists")
object CultistCommand : CommandExecutor {
    override fun onCommand(sender: CommandSender, command: Command, label: String, args: Array<out String>): Boolean {
        if (args.isEmpty()) return MissingSubCommand(sender, CultistTag)
        return when(args[0].lowercase()){
            "on" -> on(sender)
            "off" -> off(sender)
            "clear" -> clear(sender)
            else -> InvalidCommand(sender, CultistTag)
        }
    }

    @Help(CultistAdminPermission, "$darkBlue/cultist on $black- Turns on cultist spawning")
    private fun on(sender: CommandSender): Boolean {
        if (!sender.hasPermission(CultistAdminPermission)) return NotAllowed(sender, CultistTag)
        CultistFeature.active = true
        return ok(sender, CultistTag, "Cultist spawning enabled")
    }

    @Help(CultistAdminPermission, "$darkBlue/cultist off $black- Turns off cultist spawning")
    private fun off(sender: CommandSender): Boolean {
        if (!sender.hasPermission(CultistAdminPermission)) return NotAllowed(sender, CultistTag)
        CultistFeature.active = false
        return ok(sender, CultistTag, "Cultist spawning disabled")
    }

    @Help(CultistAdminPermission, "$darkBlue/cultist clear $black- Deletes all active cultists")
    private fun clear(sender: CommandSender): Boolean {
        if (!sender.hasPermission(CultistAdminPermission)) return NotAllowed(sender, CultistTag)
        CultistFeature.clear()
        return true
    }
}