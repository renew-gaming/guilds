package com.github.snarechops.renew.features.books

import com.github.snarechops.renew.common.pagify
import org.bukkit.Material
import org.bukkit.inventory.ItemStack
import org.bukkit.inventory.meta.BookMeta

@Suppress("FunctionName")
fun Book(author: String, title: String, pages: List<String>): ItemStack {
    val book = ItemStack(Material.WRITTEN_BOOK, 1)
    val meta = book.itemMeta as BookMeta
    meta.author = author
    meta.title = title
    meta.pages = pages.map { it.pagify() }.flatten()
    book.itemMeta = meta
    return book
}