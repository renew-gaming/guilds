package com.github.snarechops.renew.features.feats

import org.bukkit.Bukkit
import org.bukkit.command.Command
import org.bukkit.command.CommandSender
import org.bukkit.command.TabCompleter

object FeatsCompletion : TabCompleter {
    fun players(filter: String = ""): MutableList<String> =
        Bukkit.getOnlinePlayers().map { it.name }
            .filter { it.startsWith(filter, ignoreCase = true) }
            .sorted()
            .toMutableList()

    private fun rewards(featId: String, filter: String = ""): MutableList<String> =
        globalFeats.find { it.id == featId }?.rewards?.map { it.id }?.sorted()?.filter { it.startsWith(filter, ignoreCase = true) }?.toMutableList() ?: mutableListOf()

    override fun onTabComplete(
        sender: CommandSender,
        command: Command,
        alias: String,
        args: Array<out String>
    ): MutableList<String> = when {
        args.isEmpty() -> root(sender)
        args.size == 1 -> root(sender, args[0])
        args[0] == "show" -> show(sender, args.drop(1))
        args[0] == "credit" -> credit(sender, args.drop(1))
        args[0] == "reload" -> mutableListOf()
        args.size > 1 -> id(sender, args[0], args.drop(1))
        else -> mutableListOf()
    }

    private fun root(sender: CommandSender, filter: String = ""): MutableList<String>{
        val list = mutableListOf<String>()
        if (sender.hasPermission(FeatsAdminPermission)) {
            list.addAll(listOf("show", "credit", "reload").sorted())
            list.addAll(globalFeats.map { it.id }.sorted())
        }
        list.retainAll { it.startsWith(filter, ignoreCase = true) }
        return list
    }

    private fun show(sender: CommandSender, args: List<String>): MutableList<String> {
        if (!sender.hasPermission(FeatsAdminPermission)) return mutableListOf()
        return players(args.getOrElse(0) {""})
    }

    private fun credit(sender: CommandSender, args: List<String>): MutableList<String> = when {
        !sender.hasPermission(FeatsAdminPermission) -> mutableListOf()
        args.isEmpty() -> players("")
        args.size == 1 -> players(args[0])
        args.size > 1 -> globalFeats.map { it.id }.filter { it.startsWith(args[1], ignoreCase = true) }.toMutableList()
        else -> mutableListOf()
    }

    private fun id(sender: CommandSender, id: String, args: List<String>): MutableList<String>  {
        if (!sender.hasPermission(FeatsAdminPermission)) return mutableListOf()
        return when {
            args.isEmpty() -> mutableListOf("reward")
            args.size == 1 -> mutableListOf("reward").sorted().filter { it.startsWith(args.getOrElse(0) {""}, ignoreCase = true) }.toMutableList()
            args.size > 1 && args.getOrNull(0) == "reward" -> reward(id, args.drop(1))
            else -> mutableListOf()
        }
    }

    private fun reward(id: String, args: List<String>): MutableList<String> =
        when {
            args.isEmpty() -> mutableListOf("add", "remove")
            args.size == 1 -> mutableListOf("add", "remove").sorted().filter { it.startsWith(args.getOrElse(0) {""}, ignoreCase = true) }.toMutableList()
            args.size > 1 && args.getOrNull(0) == "add" -> rewardAdd(args.drop(1))
            args.size > 1 && args.getOrNull(0) == "remove" -> rewardRemove(id, args.drop(1))
            else -> mutableListOf()
        }

    private fun rewardAdd(args: List<String>): MutableList<String> =
        when {
            args.isEmpty() -> mutableListOf("item")
            args.size == 1 -> mutableListOf("item").sorted().filter { it.startsWith(args.getOrElse(0) {""}, ignoreCase = true) }.toMutableList()
            args.size > 1 && args.getOrNull(0) == "item" -> if (args[1].isEmpty()) mutableListOf("<reward-id>") else mutableListOf()
            else -> mutableListOf()
        }

    private fun rewardRemove(id: String, args: List<String>): MutableList<String> =
        when {
            args.isEmpty() -> rewards(id)
            args.size == 1 -> rewards(id, args[0])
            else -> mutableListOf()
        }
}