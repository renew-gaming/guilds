package com.github.snarechops.renew.features.preferences

import kotlinx.serialization.Serializable

@Serializable
data class Preferences(val uuid: String, val name: String, val pairs: MutableMap<String, String>)