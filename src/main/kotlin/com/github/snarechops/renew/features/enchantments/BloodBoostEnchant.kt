package com.github.snarechops.renew.features.enchantments

import com.github.snarechops.renew.common.*
import com.github.snarechops.renew.features.cultist.CultistDeathEvent
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.inventory.ItemStack
import org.bukkit.potion.PotionEffect
import org.bukkit.potion.PotionEffectType

object BloodBoostEnchant : Listener, CustomEnchant(
    "blood_boost",
    "Blood Boost",
    1,
    2,
    darkRed,
) {
    override fun description(level: Int): List<String> = listOf(
        "$gold- Grants speed $level for 30 seconds after",
        "$gold- killing a Cultist ${gray}Max: 2"
    )

    private fun effect(level: Int): PotionEffect = PotionEffect(PotionEffectType.SPEED, 30 * 20, level)

    override fun canEnchantItem(item: ItemStack): Boolean = item.isBoots()

    @EventHandler(ignoreCancelled = true)
    fun onCultistDeath(event: CultistDeathEvent) {
        debug("Blood Boost: onCultistDeath")
        event.killer.inventory.armorContents.filterNotNull().forEach {
            debug("armor: ${it.itemMeta?.displayName}")
            val level = has(it)
            debug("level: $level")
            if (level > 0) event.killer.addPotionEffect(effect(if (level > max) max else level))
        }
    }
}