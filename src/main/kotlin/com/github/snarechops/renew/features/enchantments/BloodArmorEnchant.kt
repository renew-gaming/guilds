package com.github.snarechops.renew.features.enchantments

import com.github.snarechops.renew.common.*
import com.github.snarechops.renew.features.cultist.isCultist
import org.bukkit.entity.Player
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.entity.EntityDamageByEntityEvent
import org.bukkit.inventory.ItemStack

object BloodArmorEnchant : Listener, CustomEnchant(
    "blood_armor",
    "Blood Armor",
    1,
    2,
    darkRed,
) {
    override fun description(level: Int): List<String> = listOf(
        "${gold}- Reduces the damage taken from",
        "${gold}- Cultists by ${(reduction(level) * 100).toInt()}% ${gray}Max: 2",
    )

    private fun reduction(level: Int): Double = level * 0.25

    override fun canEnchantItem(item: ItemStack): Boolean = item.isChestplate()

    @EventHandler(ignoreCancelled = true)
    fun onEntityDamageEntity(event: EntityDamageByEntityEvent) {
        if (event.damager.isCultist()) return
        if (event.entity !is Player) return
        val chest = (event.entity as Player).inventory.chestplate ?: return
        val level = has(chest)
        val diff = event.damage * reduction(level)
        event.damage -= diff
    }
}