package com.github.snarechops.renew.features.enchantments

import com.github.snarechops.renew.common.darkRed
import com.github.snarechops.renew.common.gold
import com.github.snarechops.renew.common.gray
import com.github.snarechops.renew.common.isSword
import com.github.snarechops.renew.features.cultist.isCultist
import org.bukkit.entity.Player
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.entity.EntityDamageByEntityEvent
import org.bukkit.inventory.ItemStack

object CleansingEnchant : Listener, CustomEnchant(
    "cleansing",
    "Cleansing",
    1,
    3,
    darkRed,
) {
    override fun description(level: Int): List<String> = listOf(
        "$gold- Increases damage to Cultists by",
        "$gold- ${(increase(level) * 100).toInt() - 100}% ${gray}Max: 3"
    )

    private fun increase(level: Int): Double = when (level) {
        2 -> 2.0
        3 -> 2.5
        else -> 1.5
    }

    override fun canEnchantItem(item: ItemStack): Boolean = item.isSword()

    @EventHandler(ignoreCancelled = true)
    fun onEntityDamageByEntity(event: EntityDamageByEntityEvent) {
        if (event.damager !is Player) return
        val player = event.damager as Player
        if (event.entity.isCultist()) return
        val weapon = player.inventory.itemInMainHand
        if (!isSword(weapon.type)) return
        val level = has(weapon)
        event.damage = event.damage * increase(level)
    }
}