package com.github.snarechops.renew.features.cultist

import com.github.snarechops.renew.common.emit
import com.github.snarechops.renew.features.cultshop.CultistCoin
import com.github.snarechops.renew.features.regions.Region
import org.bukkit.Location
import org.bukkit.entity.Monster
import org.bukkit.entity.Player
import org.bukkit.event.EventHandler
import org.bukkit.event.entity.CreatureSpawnEvent
import org.bukkit.event.entity.EntityDeathEvent
import kotlin.random.Random

class CultistRegion(min: Location, max: Location) : Region(min, max) {

    @EventHandler(ignoreCancelled = true)
    fun onEntityDeath(event: EntityDeathEvent) {
        if (!contains(event.entity.location)) return
        if (event.entity !is Monster) return
        if (!event.entity.isCultist()) return
        if (event.entity.killer !is Player) return
        emit(CultistDeathEvent(event.entity.killer as Player, event.entity))
        val amount = Random.nextInt(1, 6)
        event.drops.add(CultistCoin(amount))
    }

    @EventHandler(ignoreCancelled = true)
    fun onCreatureSpawn(event: CreatureSpawnEvent) {
        if (event.entity !is Monster) return
        if (!event.entity.isCultist()) return
        if (event.spawnReason != CreatureSpawnEvent.SpawnReason.NATURAL && event.spawnReason != CreatureSpawnEvent.SpawnReason.REINFORCEMENTS) return
        if (contains(event.location)) event.isCancelled = true
    }
}