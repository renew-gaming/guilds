package com.github.snarechops.renew.features.valentines

import com.github.snarechops.renew.common.*
import com.github.snarechops.renew.features.commands.Help
import com.github.snarechops.renew.features.commands.HelpTopic
import org.bukkit.Bukkit
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender

@HelpTopic("Valentines")
object ValentinesCommand : CommandExecutor {
    override fun onCommand(sender: CommandSender, command: Command, label: String, args: Array<out String>): Boolean {
        if (args.isEmpty()) return MissingSubCommand(sender, ValentinesTag)
        return when (args[0]) {
            "rose" -> rose(sender, args.drop(1))
            else -> InvalidCommand(sender, ValentinesTag)
        }
    }

    @Help(ValentinesAdminPermission, "$darkBlue/valentines rose <player> $black- Gives a Valentine's day rose to the specified player")
    private fun rose(sender: CommandSender, args: List<String>): Boolean {
        if (!sender.hasPermission(ValentinesAdminPermission)) return NotAllowed(sender, ValentinesTag)
        if (args.isEmpty()) return err(sender, ValentinesTag, "Missing player name")
        val player = Bukkit.getServer().getPlayer(args[0]) ?: return PlayerNotOnline(sender, ValentinesTag)
        player.inventory.addItem(ValentineWitherRose())
        ok(sender, ValentinesTag, green("Gave"), darkRed("Love's Passionate Rose"), green("to"), gold(args[0]))
        ok(player, ValentinesTag, green("Received"), darkRed("Love's Passionate Rose"))
        return true
    }
}