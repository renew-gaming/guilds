package com.github.snarechops.renew.features.cultist

import com.github.snarechops.renew.*
import com.github.snarechops.renew.common.*
import org.bukkit.Bukkit
import org.bukkit.entity.Entity
import org.bukkit.entity.Monster
import org.bukkit.entity.Player
import org.bukkit.event.Listener

const val CultistAdminPermission = "cultist.admin"
val CultistTag = pluginTag("Cultist")

data class CultistDeathEvent(val killer: Player, val cultist: Entity) : CustomEvent()

object CultistFeature : Feature, Listener {
    private val region = CultistRegion(Config.cult.min.toLocation(), Config.cult.max.toLocation())
    private val spawner = CultistSpawner(region)
    var active = true

    override fun onInit() {
        CultistCommand.register("cultist", CultistCompletion)
        this.register()
        region.register()
        repeatingTask(0, 2) { if (active) spawner.spawn() }
    }

    override fun onDestroy() {}

    fun clear() {
        Bukkit.getWorld("world")?.entities?.forEach {
            if (it.isCultist()) it.remove()
        }
    }
}

fun Entity.isCultist(): Boolean = this is Monster && this.customName?.matches(Regex("${darkRed}Cultist.*")) ?: false
