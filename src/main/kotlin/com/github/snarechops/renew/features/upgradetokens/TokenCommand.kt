package com.github.snarechops.renew.features.upgradetokens

import com.github.snarechops.renew.common.*
import com.github.snarechops.renew.features.commands.Help
import com.github.snarechops.renew.features.commands.HelpTopic
import com.github.snarechops.renew.features.crates.name
import org.bukkit.Bukkit
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player
import org.bukkit.inventory.ItemStack

@HelpTopic("Upgrade Tokens")
object TokenCommand : CommandExecutor {
    override fun onCommand(sender: CommandSender, command: Command, label: String, args: Array<out String>): Boolean =
        when {
            args.isEmpty() -> err(sender, TokensTag, "Missing token name")
            Tokens.containsKey(args[0]) -> give(sender, Tokens[args[0]]!!, args.drop(1))
            else -> err(sender, TokensTag, "Invalid token name")
        }

    @Help(TokensAdminPermission, "$darkBlue/token <name> <player> $black- Gives the specified upgrade token to a player")
    private fun give(sender: CommandSender, token: ItemStack, args: List<String>): Boolean {
        if (!sender.hasPermission(TokensAdminPermission)) return NotAllowed(sender, TokensTag)
        if (sender !is Player) return MustBePlayer(sender, TokensTag)
        if (args.isEmpty()) {
            val map = sender.inventory.addItem(token)
            if (map.size == 0) return ok(sender, TokensTag, green("Received"), *token.name.colorize())
            return NoSpace(sender, TokensTag)
        }
        val player = Bukkit.getPlayer(args[0]) ?: return err(sender, TokensTag, "Player is not online")
        val map = player.inventory.addItem(token)
        if (map.size == 0) return ok(sender, TokensTag, green("Gave"), gold(args[0]), *token.name.colorize())
        return NoSpace(sender, TokensTag)
    }
}