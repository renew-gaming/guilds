package com.github.snarechops.renew.features.guilds

import kotlinx.serialization.Serializable
import org.bukkit.Bukkit

enum class GuildName {
    MINER,
    CARPENTRY,
    GARDENING,
    ARTISAN,
    BLACKSMITH,
    STONEMASON,
    WIZARD,
    FISHING,
    FARMER,
    ADVENTURER,
    ENGINEERING;

    companion object {
        fun fromString(value: String?): GuildName? {
            if (value == null) return null
            return try {
                valueOf(value)
            } catch (e: IllegalArgumentException) {
                null
            }
        }
    }
}

val GuildNames = GuildName.values()
    .map { it.toString() }
    .map { it.lowercase() }


@Serializable
class Guild(
    val id: String,
    val name: String,
    val description: String,
    var location: Location?,
)

@Serializable
class Location(
    val world: String,
    val x: Double,
    val y: Double,
    val z: Double,
    val pitch: Float,
    val yaw: Float,
)

fun Location.toBukkitLocation(): org.bukkit.Location = org.bukkit.Location(
    Bukkit.getWorld(world),
    x,
    y,
    z,
    yaw,
    pitch,
)

fun LocationFromBukkit(bukkit: org.bukkit.Location): Location {
    return Location(
        bukkit.world?.name ?: "world",
        bukkit.x,
        bukkit.y,
        bukkit.z,
        bukkit.pitch,
        bukkit.yaw,
    )
}