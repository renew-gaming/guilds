package com.github.snarechops.renew.features.feats

import com.github.snarechops.renew.common.*
import kotlinx.serialization.Serializable
import org.bukkit.Material
import org.bukkit.entity.Player
import org.bukkit.inventory.ItemStack

@Serializable
data class GlobalFeat(
    val id: String,
    val name: String,
    val icon: Material,
    val weight: Int = 0,
    val description: List<String>,
    var rewards: List<Reward>,
)

@Serializable
data class Reward(val id: String, val name: String, val item: String)

fun Reward.itemStack(): ItemStack? =
    this.item.decodeItemStack()

fun List<Reward>.itemStacks(): List<ItemStack> =
    this.mapNotNull { it.itemStack() }

fun List<Reward>.give(player: Player) =
    this.itemStacks().forEach { it.give(player) }

@Serializable
data class Feats(
    val uuid: String,
    var completed: List<String>,
    var rewarded: List<String>,
)

fun Feats.complete(id: String) {
    if (this.isComplete(id)) return
    this.completed += id
}

fun Feats.isComplete(id: String): Boolean =
    this.completed.contains(id)

fun Feats.partition() =
    this.globals().partition { this.completed.contains(it.id) }

fun Feats.reward(id: String) {
    if (this.isRewarded(id)) return
    this.rewarded += id
}

fun Feats.isRewarded(id: String): Boolean =
    this.rewarded.contains(id)

fun Feats.globals(): List<GlobalFeat> =
    globalFeats

fun Feats.global(id: String): GlobalFeat? =
    this.globals().find { it.id == id }

fun Feats.completed(): List<GlobalFeat> =
    this.globals().filter { this.completed.contains(it.id) }

fun Feats.incompleted(): List<GlobalFeat> =
    this.globals().filter { !this.completed.contains(it.id) }

fun GlobalFeat.displayItem(complete: Boolean, showId: Boolean = false): ItemStack {
    val item = ItemStack(this.icon, 1)
    item.itemMeta = item.itemMeta?.let { meta ->
        meta.setDisplayName(this.name)
        val lore = mutableListOf<String>()
        if (showId) lore.add("$gray${this.id}")
        if (complete) lore.add("$green${bold}Completed")
        lore.addAll(this.description)
        lore.add("$bold${gold}Rewards")
        lore.addAll(this.rewards.map {
            val amount = it.itemStack()?.amount ?: 1
            (if (amount > 1) "$gold$amount " else "") + it.name
        })
        meta.lore = lore
        meta
    }
    return item
}