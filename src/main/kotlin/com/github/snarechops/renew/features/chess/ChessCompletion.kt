package com.github.snarechops.renew.features.chess

import org.bukkit.command.Command
import org.bukkit.command.CommandSender
import org.bukkit.command.TabCompleter

object ChessCompletion: TabCompleter {
    override fun onTabComplete(
        sender: CommandSender,
        command: Command,
        alias: String,
        args: Array<out String>
    ): MutableList<String> {
        if (args.size < 2) return root(sender, args.getOrElse(0) { "" })
        return mutableListOf()
    }

    private fun root(sender: CommandSender, filter: String): MutableList<String>{
        val list = mutableListOf<String>()
        if (sender.hasPermission(ChessAdminPermission)) {
            list.add("setup")
            list.add("clear")
            list.add("on")
            list.add("off")
        }
        list.retainAll { it.startsWith(filter, ignoreCase = true) }
        list.sort()
        return list
    }
}