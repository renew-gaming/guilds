package com.github.snarechops.renew.features.enchantments

import com.github.snarechops.renew.common.*
import org.bukkit.GameMode
import org.bukkit.Material
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.block.BlockBreakEvent
import org.bukkit.inventory.ItemStack

object KeepSpawnerEnchant:  Listener, CustomEnchant(
    "spawnmaker",
    "Spawnmaker",
    1 ,
    1,
    blue
) {
    override fun description(level: Int): List<String>  = listOf(
        "$gold- Drops a spawner when mined"
    )

    override fun canEnchantItem(item: ItemStack): Boolean = item.isPickaxe()

    @EventHandler(ignoreCancelled = true)
    fun onBlockBreak(event: BlockBreakEvent){
        if (event.block.type != Material.SPAWNER) return
        if (event.player.gameMode != GameMode.SURVIVAL) return
        val tool = event.player.inventory.itemInMainHand
        if (has(tool) == 0) {
            event.isCancelled = true
            err(event.player, EnchantTag, red("Pickaxe with"), *"$color$name".colorize(), red("required to break this spawner."))
            return
        }
        event.expToDrop = 0
        event.block.world.dropItemNaturally(event.block.location, ItemStack(Material.SPAWNER))
    }
}