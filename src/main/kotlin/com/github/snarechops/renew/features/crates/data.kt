package com.github.snarechops.renew.features.crates

import com.github.snarechops.renew.common.SimpleLocation
import com.github.snarechops.renew.common.decodeItemStacks
import com.github.snarechops.renew.common.lightPurple
import kotlinx.serialization.Serializable
import org.bukkit.Material
import org.bukkit.enchantments.Enchantment
import org.bukkit.inventory.ItemStack

fun SimpleLocation.same(loc: org.bukkit.Location): Boolean =
    this.world == loc.world?.name && this.x == loc.blockX && this.y == loc.blockY && this.z == loc.blockZ

@Serializable
data class Crate(
    val id: String,
    var name: String,
    var location: SimpleLocation,
    var contents: String,
)

fun Crate.items(): List<ItemStack> = this.contents.decodeItemStacks()

fun KeyFor(crate: Crate, amount: Int): ItemStack {
    val item = ItemStack(Material.TRIPWIRE_HOOK, amount)
    item.addUnsafeEnchantment(Enchantment.MENDING, 1)
    val meta = item.itemMeta
    meta?.setDisplayName("${crate.name} Key")
    val lore = meta?.lore
    lore?.add("${lightPurple}Use on the ${crate.name} ${lightPurple}to receive a reward")
    meta?.lore = lore
    item.itemMeta = meta
    return item
}

fun ItemStack.isKeyFor(crate: Crate): Boolean =
    this.type == Material.TRIPWIRE_HOOK && this.itemMeta?.displayName == "${crate.name} Key" && this.containsEnchantment(Enchantment.MENDING)