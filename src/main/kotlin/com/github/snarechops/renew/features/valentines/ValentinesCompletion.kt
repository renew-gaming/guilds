package com.github.snarechops.renew.features.valentines

import org.bukkit.Bukkit
import org.bukkit.command.Command
import org.bukkit.command.CommandSender
import org.bukkit.command.TabCompleter
import org.bukkit.entity.Player

object ValentinesCompletion : TabCompleter {
    override fun onTabComplete(
        sender: CommandSender,
        command: Command,
        alias: String,
        args: Array<out String>
    ): MutableList<String> {
        if (sender !is Player) return mutableListOf()
        return when {
            args.isEmpty() -> root(sender, "")
            args.size == 1 -> root(sender, args[0])
            args[0] == "rose" -> rose(sender, args.drop(1))
            else -> mutableListOf()
        }
    }

    private fun root(sender: CommandSender, filter: String): MutableList<String> {
        val list = mutableListOf<String>()
        if (sender.hasPermission(ValentinesAdminPermission) && sender is Player) {
            list.add("rose")
        }
        list.retainAll { it.startsWith(filter, ignoreCase = true) }
        list.sort()
        return list
    }

    private fun rose(sender: CommandSender, args: List<String>): MutableList<String> {
        if (!sender.hasPermission(ValentinesAdminPermission)) return mutableListOf()
        val list = Bukkit.getServer().onlinePlayers.map { it.name }.toMutableList()
        list.retainAll { it.startsWith(args[0], ignoreCase = true) }
        list.sort()
        return list
    }
}