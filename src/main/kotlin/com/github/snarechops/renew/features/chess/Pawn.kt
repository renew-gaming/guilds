package com.github.snarechops.renew.features.chess

import org.bukkit.entity.Player

class Pawn(player: Player, team: Team, spot: Spot) : Piece(
    player, team, spot, when (team) {
        Team.WHITE -> ::WhitePawn
        Team.BLACK -> ::BlackPawn
    }
) {
    var isFirstMove = true

    override fun moves(): List<Spot>{
        val direction = when(team){
            Team.WHITE -> 1
            Team.BLACK -> -1
        }
        val result = mutableListOf<Spot>()
        result.addAll(walk(0, direction, if(isFirstMove) 2 else 1))
        spot.getRelative(1, direction)?.let {
            if (it.hasTeam(team.opposite())) result.add(it)
        }
        spot.getRelative(-1, direction)?.let {
            if (it.hasTeam(team.opposite())) result.add(it)
        }
        return result
    }

    override fun move(end: Spot): Boolean {
        if (!super.move(end)) return false
        isFirstMove = false
        return true
    }
}