package com.github.snarechops.renew.features.quests

import com.github.snarechops.renew.features.guilds.GuildName
import org.bukkit.Bukkit
import org.bukkit.command.Command
import org.bukkit.command.CommandSender
import org.bukkit.command.TabCompleter

object QuestCompletion : TabCompleter {
    override fun onTabComplete(
        sender: CommandSender,
        command: Command,
        alias: String,
        args: Array<out String>
    ): MutableList<String> = when {
        args.isEmpty() -> root(sender, "")
        args.size == 1 -> root(sender, args[0])
        args[0] == "credit" -> credit(args.drop(1))
        args[0] == "expire" -> expire(args.drop(1))
        else -> mutableListOf()
    }

    private fun root(sender: CommandSender, filter: String): MutableList<String> {
        val list = mutableListOf<String>()
        if (sender.hasPermission(QuestsAdminPermission)) list.addAll(listOf("credit", "expire"))
        GuildName.values().forEach { list.add(it.toString().lowercase()) }
        list.retainAll { it.startsWith(filter, ignoreCase = true) }
        list.sort()
        return list
    }

    private fun credit(args: List<String>): MutableList<String> {
        val list = when (args.size) {
            1 -> Bukkit.getOnlinePlayers().map { it.name }.toMutableList()
            2 -> GuildName.values().map { it.name.lowercase() }.toMutableList()
            else -> mutableListOf()
        }
        list.retainAll { it.startsWith(args.last(), ignoreCase = true) }
        list.sort()
        return list
    }

    private fun expire(args: List<String>): MutableList<String> {
        val list = when (args.size) {
            1 -> Bukkit.getOnlinePlayers().map { it.name }.toMutableList()
            2 -> GuildName.values().map { it.name.lowercase() }.toMutableList()
            else -> mutableListOf()
        }
        list.retainAll { it.startsWith(args.last(), ignoreCase = true) }
        list.sort()
        return list
    }
}