package com.github.snarechops.renew.features.cultist

import com.github.snarechops.renew.common.darkRed
import org.bukkit.Material
import org.bukkit.attribute.Attribute
import org.bukkit.enchantments.Enchantment
import org.bukkit.entity.Monster
import org.bukkit.inventory.ItemStack
import org.bukkit.potion.PotionEffect
import org.bukkit.potion.PotionEffectType

fun Cultist(mob: Monster): Monster = mob
    .let { setHealth(it, 55.0) }
    .let { setName(it, "Cultist") }

fun ZombieCultist(mob: Monster): Monster = Cultist(mob)
    .let { giveSword(it) }
    .let { giveHelmet(it) }
    .let { giveSpeed(it) }

fun SkeletonCultist(mob: Monster): Monster = Cultist(mob)
    .let { giveBow(it) }
    .let { giveHelmet(it) }
    .let { giveSpeed(it) }

fun setHealth(mob: Monster, health: Double): Monster {
    val attr = mob.getAttribute(Attribute.GENERIC_MAX_HEALTH)
    attr?.baseValue = health
    mob.health = health
    return mob
}

fun setName(mob: Monster, name: String): Monster {
    mob.customName = "$darkRed$name"
    mob.isCustomNameVisible = true
    return mob
}

fun giveSword(mob: Monster): Monster {
    mob.equipment?.let {
        it.setItemInMainHand(ItemStack(Material.NETHERITE_SWORD))
        it.itemInMainHandDropChance = 0f
    }
    return mob
}

fun giveBow(mob: Monster): Monster {
    val bow = ItemStack(Material.BOW)
    bow.addEnchantment(Enchantment.ARROW_KNOCKBACK, 1)
    bow.addEnchantment(Enchantment.ARROW_FIRE, 1)
    mob.equipment?.let {
        it.setItemInMainHand(bow)
        it.itemInMainHandDropChance = 0f
    }
    return mob
}

fun giveHelmet(mob: Monster): Monster {
    val helmet = ItemStack(Material.LEATHER_HELMET)
    helmet.addEnchantment(Enchantment.DURABILITY, 3)
    mob.equipment?.let {
        it.helmet = helmet
        it.helmetDropChance = 0f
    }
    return mob
}

fun giveSpeed(mob: Monster): Monster {
    mob.addPotionEffect(PotionEffect(PotionEffectType.SPEED, 10000, 1))
    return mob
}
