package com.github.snarechops.renew.features.deaths

import com.github.snarechops.renew.common.*
import com.github.snarechops.renew.features.commands.Help
import com.github.snarechops.renew.features.commands.HelpTopic
import org.bukkit.Bukkit
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player

@HelpTopic("Deaths")
object DeathCommand: CommandExecutor{
    override fun onCommand(sender: CommandSender, command: Command, label: String, args: Array<out String>): Boolean = when {
            args.isEmpty() -> MissingSubCommand(sender, DeathTag)
            args.size == 1 -> show(sender as Player, args[0])
            else -> InvalidCommand(sender, DeathTag)
        }

    @Help(DeathAdminPermission, "$darkBlue/deaths <player> $black- Gets the players last death inventory")
    private fun show(sender: CommandSender, arg: String): Boolean{
        if(!sender.hasPermission(DeathAdminPermission)) return NotAllowed(sender, DeathTag)
        if(sender !is Player) return MustBePlayer(sender, DeathTag)
        val target = Bukkit.getServer().getPlayer(arg) ?: return PlayerNotOnline(sender, DeathTag)
        DeathFeature.ui(sender, target, 1)
        return true
    }
}