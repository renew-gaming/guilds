package com.github.snarechops.renew.features.locker

import com.github.snarechops.renew.common.darkPurple
import com.github.snarechops.renew.common.decodeItemStacks
import kotlinx.serialization.Serializable
import org.bukkit.Bukkit
import org.bukkit.inventory.Inventory

@Serializable
data class Locker(
    val uuid: String,
    val name: String,
    val items: String,
)

fun Locker.toInventory(): Inventory {
    val inventory = Bukkit.createInventory(null, 54, "${darkPurple}Locker: ${this.name}")
    this.items.decodeItemStacks().forEach { inventory.addItem(it) }
    return inventory
}

