package com.github.snarechops.renew.features.reports

import org.bukkit.command.Command
import org.bukkit.command.CommandSender
import org.bukkit.command.TabCompleter

object BugCompletion: TabCompleter {
    override fun onTabComplete(
        sender: CommandSender,
        command: Command,
        alias: String,
        args: Array<out String>
    ): MutableList<String> {
        if(args.isEmpty()) return mutableListOf("<message>")
        return mutableListOf()
    }
}