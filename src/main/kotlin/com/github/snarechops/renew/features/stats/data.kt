package com.github.snarechops.renew.features.stats

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonElement

@Serializable
data class Stat<T>(val uuid: String, val key: String, var value: T? = null)