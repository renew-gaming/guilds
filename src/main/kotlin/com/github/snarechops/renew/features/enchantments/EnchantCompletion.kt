package com.github.snarechops.renew.features.enchantments

import org.bukkit.command.Command
import org.bukkit.command.CommandSender
import org.bukkit.command.TabCompleter

object EnchantCompletion : TabCompleter {
    override fun onTabComplete(
        sender: CommandSender,
        command: Command,
        alias: String,
        args: Array<out String>
    ): MutableList<String> = when {
        args.isEmpty() -> Enchants.keys.toMutableList()
        args.size == 1 -> Enchants.keys.filter { it.startsWith(args[0], ignoreCase = true) }.sorted().toMutableList()
        args.size == 2 -> Enchants[args[0]]?.let{ it.min..it.max }?.map { it.toString() }?.toMutableList()
        else -> null
    } ?: mutableListOf()
}