package com.github.snarechops.renew.features.deaths

import org.bukkit.Bukkit
import org.bukkit.command.Command
import org.bukkit.command.CommandSender
import org.bukkit.command.TabCompleter
import org.bukkit.entity.Player

object DeathCompletion: TabCompleter {
    override fun onTabComplete(
            sender: CommandSender,
            command: Command,
            alias: String,
            args: Array<out String>
    ): MutableList<String> {
        if (!sender.hasPermission(DeathAdminPermission)) return mutableListOf()
        if (sender !is Player) return mutableListOf()
        return when {
            args.isEmpty() -> root("")
            args.size == 1 -> root(args[0])
            args.size == 2 -> mutableListOf("[page #]")
            else -> mutableListOf()
        }
    }

    private fun root(filter: String): MutableList<String> {
        val list = Bukkit.getOnlinePlayers().map{ it.name }.toMutableList()
        list.retainAll{it.startsWith(filter, ignoreCase = true)}
        list.sort()
        return list
    }
}