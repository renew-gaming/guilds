package com.github.snarechops.renew.features.enchantments

import com.github.snarechops.renew.common.darkRed
import com.github.snarechops.renew.common.gold
import com.github.snarechops.renew.common.gray
import com.github.snarechops.renew.common.isHelmet
import com.github.snarechops.renew.features.cultist.CultistDeathEvent
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.inventory.ItemStack
import org.bukkit.potion.PotionEffect
import org.bukkit.potion.PotionEffectType

object BloodRegenEnchant : Listener, CustomEnchant(
    "blood_regen",
    "Blood Regen",
    1,
    2,
    darkRed,
) {
    override fun description(level: Int): List<String> = listOf(
        "$gold- Grants Health Regeneration $level for 30",
        "$gold- seconds after killing a Cultist ${gray}Max: 2"
    )

    private fun effect(level: Int) = PotionEffect(PotionEffectType.REGENERATION, 30 * 20, level, false, false)

    override fun canEnchantItem(item: ItemStack): Boolean = item.isHelmet()

    @EventHandler(ignoreCancelled = true)
    fun onCultistDeath(event: CultistDeathEvent) {
        event.killer.inventory.armorContents.filterNotNull().forEach {
            val level = has(it)
            if (level > 0) event.killer.addPotionEffect(effect(if (level > max) max else level))
        }
    }
}