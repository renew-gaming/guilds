package com.github.snarechops.renew.features.feats

import com.github.snarechops.renew.common.*
import com.github.snarechops.renew.features.commands.Help
import com.github.snarechops.renew.features.commands.HelpTopic
import org.bukkit.Bukkit
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player

const val FeatsAdminPermission = "feats.admin"

@HelpTopic("Feats")
object FeatsCommand : CommandExecutor {
    @Help("", "$darkBlue/feats $black- Open your current feats")
    override fun onCommand(sender: CommandSender, command: Command, label: String, args: Array<out String>): Boolean =
        when {
            args.isEmpty() -> show(sender, listOf())
            args[0] == "credit" -> credit(sender, args.drop(1))
            args[0] == "reload" -> reload(sender)
            else -> manage(sender, args.toList())
        }

    @Help(FeatsAdminPermission, "$darkBlue/feats <player> $black- Open the feats of the specified player")
    private fun show(sender: CommandSender, args: List<String>): Boolean {
        if (sender !is Player) return MustBePlayer(sender, RewardsTag)
        val target = Bukkit.getPlayer(args.getOrElse(0) { "" }) ?: sender
        if (target != sender && !sender.hasPermission(FeatsAdminPermission)) return NotAllowed(sender, RewardsTag)
        FeatsFeature.show(sender, target, 1)
        return true
    }

    @Help(FeatsAdminPermission, "$darkBlue/feats credit <player> <id> $black- Credit the specified feat to the player")
    private fun credit(sender: CommandSender, args: List<String>): Boolean {
        if (!sender.hasPermission(FeatsAdminPermission)) return NotAllowed(sender, RewardsTag)
        val target = Bukkit.getPlayer(args.getOrElse(0) { "" }) ?: return err(sender, RewardsTag, "Missing player name")
        val id = args.getOrNull(1) ?: return err(sender, RewardsTag, "Missing feat id")
        if (!FeatsFeature.credit(target, id)) return err(
            sender,
            RewardsTag,
            red("Unable to credit"),
            gold(target.name),
            red("with feat"),
            gold(id)
        )
        return true
    }

    private fun reload(sender: CommandSender): Boolean {
        if (!sender.hasPermission(FeatsAdminPermission)) return NotAllowed(sender, RewardsTag)
        FeatsFeature.reload()
        return ok(sender, RewardsTag, "Feats reloaded")
    }

    private fun manage(sender: CommandSender, args: List<String>): Boolean {
        val id = args.getOrNull(0) ?: return err(sender, RewardsTag, "Missing feat id")
        val feat = globalFeats.find { it.id == id}  ?: return err(sender, RewardsTag, "Invalid feat id $id")
        return when (args.getOrNull(1)) {
            "reward" -> reward(sender, feat, args.drop(2))
            else -> InvalidCommand(sender, RewardsTag)
        }
    }

    private fun reward(sender: CommandSender, feat: GlobalFeat, args: List<String>): Boolean = when (args.getOrNull(0)) {
        "add" -> addFeatReward(sender, feat, args.drop(1))
        "remove" -> removeFeatReward(sender, feat, args.drop(1))
        else -> InvalidCommand(sender, RewardsTag)
    }

    private fun addFeatReward(sender: CommandSender, feat: GlobalFeat, args: List<String>): Boolean =
        when (args.getOrNull(0)) {
            "item" -> addFeatRewardItem(sender, feat, args.drop(1))
            else -> InvalidCommand(sender, RewardsTag)
        }

    @Help(FeatsAdminPermission, "$darkBlue/feats <id> reward add item <reward-id> $black- Adds the current item in hand as a reward for the specified feat")
    private fun addFeatRewardItem(sender: CommandSender, feat: GlobalFeat, args: List<String>): Boolean {
        if (!sender.hasPermission(FeatsAdminPermission)) return NotAllowed(sender, RewardsTag)
        if (sender !is Player) return MustBePlayer(sender, RewardsTag)
        val id = args.getOrNull(0) ?: return err(sender, RewardsTag, "Missing reward id")
        if (!FeatsFeature.addFeatRewardItem(sender, feat, id)) return err(sender, RewardsTag, "Failed to add reward item")
        return ok(sender, RewardsTag, "Reward item added to feat ${feat.id}")
    }

    @Help(FeatsAdminPermission, "$darkBlue/feats <id> reward remove <reward-id> $black- Removes the specified reward from the feat")
    private fun removeFeatReward(sender: CommandSender, feat: GlobalFeat, args: List<String>): Boolean {
        if(!sender.hasPermission(FeatsAdminPermission)) return NotAllowed(sender, RewardsTag)
        val id = args.getOrNull(0) ?: return err(sender, RewardsTag, "Missing reward id")
        if (!FeatsFeature.removeFeatReward(feat, id)) return err(sender, RewardsTag, "Failed to remove reward")
        return ok(sender, RewardsTag, "Reward $id removed from feat ${feat.id}")
    }
}