package com.github.snarechops.renew.features.upgradetokens

import com.github.snarechops.renew.Feature
import com.github.snarechops.renew.common.*
import com.github.snarechops.renew.features.enchantments.CustomEnchant
import com.github.snarechops.renew.features.enchantments.Enchants
import org.bukkit.Material
import org.bukkit.enchantments.Enchantment
import org.bukkit.event.EventHandler
import org.bukkit.event.EventPriority
import org.bukkit.event.Listener
import org.bukkit.event.inventory.PrepareAnvilEvent
import org.bukkit.inventory.ItemStack

const val TokensAdminPermission = "tokens.admin"
val TokensTag = pluginTag("Upgrade Tokens")

object UpgradeTokenFeature : Feature, Listener {
    override fun onInit() {
        TokenCommand.register("token", TokenCompletion)
        this.register()
    }

    override fun onDestroy() {}

    @EventHandler(priority = EventPriority.HIGH, ignoreCancelled = true)
    fun onPrepareAnvil(event: PrepareAnvilEvent) {
        // Ge the items in the anvil
        val item = event.inventory.getItem(0) ?: return debug("No item in slot 0")
        val modifier = event.inventory.getItem(1) ?: return debug("No item in slot 1")
        // Must be upgrade token
        if (modifier.type != Material.NETHER_STAR) return debug("Item is not nether star")
        val lore = modifier.itemMeta?.lore ?: return debug("Nether start has no lore")
        // Prevent wasting tokens
        if (modifier.amount > 1) {
            event.result = Air()
            debug("Cannot use more than one upgrade token at a time")
            return
        }
        // Check which type of upgrade this is
        val vanilla = lore.find { it.startsWith(UPGRADE_VANILLA_PREFIX) }?.let { Enchantment.getByName(it.removePrefix(UPGRADE_VANILLA_PREFIX)) }
        val custom = lore.find { it.startsWith(UPGRADE_CUSTOM_PREFIX) }?.let { Enchants[it.removePrefix(UPGRADE_CUSTOM_PREFIX)] }
        // Perform the upgrade
        event.result = when {
            vanilla != null -> upgradeVanillaEnchantment(item, vanilla)
            custom != null -> upgradeCustomEnchantment(item, custom)
            else -> Air()
        }
        // Set the cost
        nextTick { event.inventory.repairCost = 3 }
    }

    private fun upgradeVanillaEnchantment(item: ItemStack, enchantment: Enchantment): ItemStack =
        item.clone().let {
            val meta = it.itemMeta ?: return@let Air()
            if (!meta.hasEnchant(enchantment)) return@let Air()
            val level = meta.getEnchantLevel(enchantment)
            if (level == 10) return@let Air()
            meta.addEnchant(enchantment, level + 1, true)
            it.itemMeta = meta
            return@let it
        }

    private fun upgradeCustomEnchantment(item: ItemStack, enchant: CustomEnchant): ItemStack {
        debug("CustomEnchant: $enchant")
        val level = enchant.has(item)
        debug("Level: $level")
        if (level == 0) return Air()
        if (level >= enchant.max) return Air()
        return item.clone()
            .let { enchant.remove(it) }
            .let { enchant.enchant(it, level + 1) }
    }
}