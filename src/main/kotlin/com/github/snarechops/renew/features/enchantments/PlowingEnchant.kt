package com.github.snarechops.renew.features.enchantments

import com.github.snarechops.renew.common.*
import org.bukkit.GameMode
import org.bukkit.Material
import org.bukkit.block.Block
import org.bukkit.entity.Player
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.block.BlockBreakEvent
import org.bukkit.inventory.ItemStack

object PlowingEnchant : Listener, CustomEnchant(
    "plowing",
    "Plowing",
    1,
    3,
    blue
) {
    var ignore = setOf<Player>()
    fun amount(level: Int): Int = 3 * level

    override fun description(level: Int): List<String>  = listOf(
        "$gold- Digs a plane of blocks 3 wide by ${amount(level)} long",
        "${gray}Max: 3"
    )

    override fun canEnchantItem(item: ItemStack): Boolean = item.isShovel()

    @EventHandler(ignoreCancelled = true)
    fun onBlockBreak(event: BlockBreakEvent) {
        if (ignore.contains(event.player)) return
        if (event.player.gameMode != GameMode.SURVIVAL) return
        if (!event.block.isDiggable()) return
        val tool = event.player.inventory.itemInMainHand
        val level = has(tool)
        if (level == 0) return
        val location = event.player.location
        val axis = if (location.isFacingNorth() || location.isFacingSouth()) Axis.X else Axis.Z
        val startingBlock = when(axis) {
            Axis.X -> event.block.getRelative(1, 0, 0)
            else -> event.block.getRelative(0, 0, 1)
        }
        val direction = location.toHorizontalDirection()
        val blocks = startingBlock.planeScan(direction, axis, 3, amount(level)) { !it.isDiggable() }
        dig(event.player, blocks, tool)
    }

    private fun dig(player: Player, blocks: List<Block>, tool: ItemStack){
        val speed = 3
        if (blocks.isEmpty()) return
        ignore = ignore + player
        blocks.subList(0, if (blocks.size < speed) blocks.size else speed).forEach {
            val event = BlockBreakEvent(it, player)
            emit(event)
            if (!event.isCancelled) it.breakNaturally(tool)
        }
        ignore = ignore - player
        nextTick { dig(player, blocks.drop(speed), tool) }
    }
}

fun Block.isDiggable(): Boolean = listOf(
    Material.GRASS_BLOCK,
    Material.DIRT_PATH,
    Material.DIRT,
    Material.COARSE_DIRT,
    Material.PODZOL,
    Material.SAND,
    Material.GRAVEL,
    Material.RED_SAND,
    Material.SNOW_BLOCK,
    Material.CLAY,
    Material.SOUL_SAND,
    Material.SOUL_SOIL,
    Material.MYCELIUM,
    Material.FARMLAND,
    Material.MOSS_BLOCK,
    Material.ROOTED_DIRT,
    Material.POWDER_SNOW,
    Material.WHITE_CONCRETE_POWDER,
    Material.ORANGE_CONCRETE_POWDER,
    Material.MAGENTA_CONCRETE_POWDER,
    Material.LIGHT_BLUE_CONCRETE_POWDER,
    Material.YELLOW_CONCRETE_POWDER,
    Material.LIME_CONCRETE_POWDER,
    Material.PINK_CONCRETE_POWDER,
    Material.GRAY_CONCRETE_POWDER,
    Material.LIGHT_GRAY_CONCRETE_POWDER,
    Material.CYAN_CONCRETE_POWDER,
    Material.PURPLE_CONCRETE_POWDER,
    Material.BLUE_CONCRETE_POWDER,
    Material.BROWN_CONCRETE_POWDER,
    Material.GREEN_CONCRETE_POWDER,
    Material.RED_CONCRETE_POWDER,
    Material.BLACK_CONCRETE_POWDER,
).contains(this.type)