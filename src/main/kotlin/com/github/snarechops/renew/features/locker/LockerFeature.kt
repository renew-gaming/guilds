package com.github.snarechops.renew.features.locker

import com.github.snarechops.renew.Feature
import com.github.snarechops.renew.common.*
import com.mongodb.client.MongoCollection
import com.mongodb.client.model.ReplaceOptions
import org.bukkit.Bukkit
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.inventory.InventoryCloseEvent
import org.litote.kmongo.eq
import org.litote.kmongo.findOne

const val LockerUsePermission = "locker.use"
const val LockerAdminPermission = "locker.admin"
val LOCKER_PREFIX = "${darkPurple}Locker: "
val LockerTag = pluginTag("Locker")

val LockerCollection: MongoCollection<Locker>
    get() = Database.getDatabase("renew").getCollection("locker", Locker::class.java)

object LockerFeature: Feature, Listener {
    override fun onInit() {
        LockerCommand.register("locker", LockerCompletion)
        this.register()
    }

    fun getOne(uuid: String): Locker? = LockerCollection.findOne(Locker::uuid eq uuid)

    fun save(locker: Locker) =
        LockerCollection.replaceOne(Locker::uuid eq locker.uuid, locker, ReplaceOptions().upsert(true))

    override fun onDestroy() {}

    @EventHandler(ignoreCancelled = true)
    fun onInventoryClose(event: InventoryCloseEvent) {
        if (!event.view.title.startsWith(LOCKER_PREFIX)) return
        val name = event.view.title.removePrefix(LOCKER_PREFIX)
        @Suppress("DEPRECATION")
        val player = Bukkit.getOfflinePlayer(name)
        val items = event.inventory.storageContents.encodeString()
        save(Locker(player.uniqueId.toString(), name, items))
    }
}