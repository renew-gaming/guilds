package com.github.snarechops.renew.features.chess

import org.bukkit.entity.Player

class King(player: Player, team: Team, spot: Spot) : Piece(player, team, spot, when (team) {
    Team.WHITE -> ::WhiteKing
    Team.BLACK -> ::BlackKing
}) {
    var hasMoved: Boolean = false

    override fun moves(): List<Spot> {
        return board.spots.flatten().map { Move(spot, it) }
            .filter { it.deltaX * it.deltaY == 1 || it.deltaX + it.deltaY == 1 }
            .filter { it.to.piece?.team != team }
            .map { it.to }
    }

    override fun move(end: Spot): Boolean {
        hasMoved = true
        return super.move(end)
    }
}