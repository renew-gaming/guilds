package com.github.snarechops.renew.features.enchantments

import com.github.snarechops.renew.common.debug
import org.bukkit.entity.Player
import org.bukkit.inventory.ItemStack

abstract class CustomEnchant(
    val id: String,
    val name: String,
    val min: Int,
    val max: Int,
    val color: String,
) {
    fun enchant(item: ItemStack, level: Int): ItemStack{
        val meta = item.itemMeta ?: return item
        debug("Item meta: $meta")
        val lore = (meta.lore ?: listOf()).toMutableList()
        lore += "$color$name $level"
        for(i in description(level)){
            lore += i
        }
        debug("Lore: $lore")
        meta.lore = lore.toList()
        item.itemMeta = meta
        return item
    }

    fun remove(item: ItemStack): ItemStack{
        val meta = item.itemMeta ?: return item
        if (!meta.hasLore()) return item
        val lore = meta.lore!!.toMutableList()
        val level = has(item)
        if (level == 0) return item
        val idx = meta.lore!!.indexOf("$color$name $level")
        val len = description(level).size + 1
        for(i in ((len + idx) - 1) downTo ((len + idx) - len)){
            lore.removeAt(i)
        }
        meta.lore = lore.toList()
        item.itemMeta = meta
        return item
    }

    fun has(item: ItemStack): Int{
        val meta = item.itemMeta ?: return 0
        val lore = meta.lore ?: return 0
        for(l in lore){
            if(l.startsWith("$color$name")){
                val level = l.removePrefix("$color$name").trim()
                return level.toIntOrNull() ?: 0
            }
        }
        return 0
    }

    abstract fun description(level: Int): List<String>

    abstract fun canEnchantItem(item: ItemStack): Boolean
}

interface PassiveEnchant {
    fun has(item: ItemStack): Int
    fun tick(player: Player, level: Int)
}

fun List<CustomEnchant>.toMap() =
    this.fold(mapOf<String, CustomEnchant>()) { map, it -> map + Pair(it.id, it) }