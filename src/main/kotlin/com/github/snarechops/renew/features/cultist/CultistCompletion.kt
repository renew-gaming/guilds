package com.github.snarechops.renew.features.cultist

import org.bukkit.command.Command
import org.bukkit.command.CommandSender
import org.bukkit.command.TabCompleter

object CultistCompletion : TabCompleter {
    override fun onTabComplete(
        sender: CommandSender,
        command: Command,
        alias: String,
        args: Array<out String>
    ): MutableList<String> = root(sender, args.getOrElse(0) { "" })

    fun root(sender: CommandSender, filter: String): MutableList<String> {
        val list = mutableListOf<String>()
        if (sender.hasPermission(CultistAdminPermission)) {
            list.add("on")
            list.add("off")
        }
        if (sender.hasPermission(CultistAdminPermission)) list.add("clear")
        list.retainAll { it.startsWith(filter, ignoreCase = true) }
        list.sort()
        return list
    }
}