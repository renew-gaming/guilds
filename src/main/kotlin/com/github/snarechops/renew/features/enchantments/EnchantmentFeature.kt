package com.github.snarechops.renew.features.enchantments

import com.github.snarechops.renew.Feature
import com.github.snarechops.renew.common.*
import org.bukkit.Bukkit
import org.bukkit.event.Listener

const val EnchantAdminPermission = "enchant.admin"
val EnchantTag = pluginTag("Custom Enchants")

val Enchants = listOf(
    BloodArmorEnchant,
    BloodBashEnchant,
    BloodBoostEnchant,
    BloodDropEnchant,
    BloodLustEnchant,
    BloodRegenEnchant,
    CleansingEnchant,
    MiningLampEnchant,
    JumpBoostEnchant,
    SnorkelEnchant,
    KeepSpawnerEnchant,
    MassHarvestEnchant,
    SwiftEnchantment,
    PlowingEnchant,
    OmeletteEnchant,
    TunnelEnchant,
).toMap()

object EnchantmentFeature : Feature, Listener {

    private val passives: List<PassiveEnchant>
        get() = Enchants.values.filter { it is PassiveEnchant }.map { it as PassiveEnchant }

    override fun onInit() {
        EnchantCommand.register("ce", EnchantCompletion)
        this.register()
        Enchants.values.filter { it is Listener }.forEach { (it as Listener).register() }
        repeatingTask(0, 60) {
            Bukkit.getOnlinePlayers().forEach { player ->
                player.inventory.armorContents.filterNotNull().forEach { item ->
                    passives.forEach {
                        val level = it.has(item)
                        if (level > 0) it.tick(player, level)
                    }
                    item.enchantments.forEach { (enchant, level) ->
                        if (enchant is PassiveEnchant) enchant.tick(player, level)
                    }
                }
            }
        }
    }

    override fun onDestroy() {}

//    @EventHandler(priority = EventPriority.HIGH, ignoreCancelled = true)
//    fun onPrepareAnvil(event: PrepareAnvilEvent) {
//        val item1 = event.inventory.getItem(0) ?: return debug("CustomEnchanting: No item in slot 0")
//        val item2 = event.inventory.getItem(1) ?: return debug("CustomEnchanting: Not item in slot 1")
//        var result = item1.clone()
//        if (item2.type != Material.NETHER_STAR) return debug("CustomEnchanting: Item in slot 1 is not a nether star")
//        val lore = item2.itemMeta?.lore ?: return debug("CustomEnchanting: Nether star does not have lore")
//        if (lore.isEmpty()) return debug("CustomEnchanting: Nether star lore is empty")
//        val regex = Regex("${green}Upgrades the (.*) enchantment")
//        val match = regex.find(lore[0]) ?: return debug("CustomEnchanting: No regex match")
//        val name = match.groupValues[1]
//        val enchant = Enchants[name.lowercase().replace(" ", "_")] ?: return debug("CustomEnchanting: Cannot find enchantment $name")
//        if (item2.amount > 1) {
//            event.result = Nothing()
//            return debug("CustomEnchanting: Must use only 1 token at a time")
//        }
//        val level = enchant.has(item1)
//        if (level == 0) {
//            event.result = Nothing()
//            return debug("CustomEnchanting: Item does not already have this enchantment")
//        }
//        if (level >= enchant.max) {
//            event.result = Nothing()
//            return debug("CustomEnchanting: Item is already at max level")
//        }
//        result = enchant.remove(result)
//        result = enchant.enchant(result, level + 1)
//
//        event.inventory.repairCost = 3
//        debug("Result: $result")
//        event.result = result
//    }
}