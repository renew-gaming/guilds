package com.github.snarechops.renew.features.cultshop

import com.github.snarechops.renew.common.*
import com.github.snarechops.renew.features.enchantments.*
import org.bukkit.Material
import org.bukkit.enchantments.Enchantment
import org.bukkit.inventory.ItemStack

const val CULTIST_COIN_NAME = "${darkPurple}Cultist Coin"
val CULTIST_COIN_LORE = listOf(
    "${green}Currency of the Cultists",
    "${gold}Use at the hidden cultist shop"
)

fun CultistCoin(amount: Int): ItemStack {
    val item = ItemStack(Material.IRON_NUGGET, amount)
    val meta = item.itemMeta!!
    meta.setDisplayName(CULTIST_COIN_NAME)
    meta.lore = CULTIST_COIN_LORE
    item.itemMeta = meta
    return item
}

data class ShopItem(val ctor: (Int) -> ItemStack, val amount: Int, val id: Int, val cost: Int)
fun ShopItem.displayItem(): ItemStack {
    val result = ctor(amount)
    val meta = result.itemMeta
    val lore = meta?.lore ?: mutableListOf()
    lore.add("${green}Cost: $cost $CULTIST_COIN_NAME")
    lore.add("${obfuscated}$id")
    meta?.lore = lore
    result.itemMeta = meta
    return result
}


const val CULTIST_HELM_ID = 100
const val CULTIST_HELM_COST = 576
fun CultistHelm(amount: Int): ItemStack {
    val result = ItemStack(Material.NETHERITE_HELMET, amount)
    result.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 5)
    result.addUnsafeEnchantment(Enchantment.DURABILITY, 4)
    result.addEnchantment(Enchantment.MENDING, 1)
    BloodRegenEnchant.enchant(result, 1)
    val meta = result.itemMeta ?: return result
    meta.setDisplayName("${darkRed}Cultist Helm")
    result.itemMeta = meta
    return result
}

const val CULTIST_CHEST_ID = 101
const val CULTIST_CHEST_COST = 576
fun CultistChest(amount: Int): ItemStack {
    val result = ItemStack(Material.NETHERITE_CHESTPLATE, amount)
    result.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 5)
    result.addUnsafeEnchantment(Enchantment.DURABILITY, 4)
    result.addEnchantment(Enchantment.MENDING, 1)
    BloodArmorEnchant.enchant(result, 1)
    val meta = result.itemMeta ?: return result
    meta.setDisplayName("${darkRed}Cultist Chest")
    result.itemMeta = meta
    return result
}

const val CULTIST_LEGS_ID = 102
const val CULTIST_LEGS_COST = 576
fun CultistLegs(amount: Int): ItemStack {
    val result = ItemStack(Material.NETHERITE_LEGGINGS, amount)
    result.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 5)
    result.addUnsafeEnchantment(Enchantment.DURABILITY, 4)
    result.addEnchantment(Enchantment.MENDING, 1)
    BloodLustEnchant.enchant(result, 1)
    val meta = result.itemMeta ?: return result
    meta.setDisplayName("${darkRed}Cultist Legs")
    result.itemMeta = meta
    return result
}

const val CULTIST_BOOTS_ID = 103
const val CULTIST_BOOTS_COST = 576
fun CultistBoots(amount: Int): ItemStack {
    val result = ItemStack(Material.NETHERITE_BOOTS, amount)
    result.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 5)
    result.addUnsafeEnchantment(Enchantment.DURABILITY, 4)
    result.addEnchantment(Enchantment.MENDING, 1)
    BloodBoostEnchant.enchant(result, 1)
    val meta = result.itemMeta ?: return result
    meta.setDisplayName("${darkRed}Cultist Boots")
    result.itemMeta = meta
    return result
}

const val CULTIST_WINGS_ID = 104
const val CULTIST_WINGS_COST = 576
fun CultistWings(amount: Int): ItemStack {
    val result = ItemStack(Material.ELYTRA, amount)
    result.addUnsafeEnchantment(Enchantment.DURABILITY, 4)
    result.addEnchantment(Enchantment.MENDING, 1)
    BloodDropEnchant.enchant(result, 1)
    val meta = result.itemMeta ?: return result
    meta.setDisplayName("${darkRed}Cultist Wings")
    result.itemMeta = meta
    return result
}

const val CULTIST_SWORD_ID = 105
const val CULTIST_SWORD_COST = 576
fun CultistSword(amount: Int): ItemStack {
    val result = ItemStack(Material.NETHERITE_SWORD, amount)
    result.addUnsafeEnchantment(Enchantment.DAMAGE_ALL, 4)
    result.addUnsafeEnchantment(Enchantment.DAMAGE_ARTHROPODS, 4)
    result.addUnsafeEnchantment(Enchantment.DAMAGE_UNDEAD, 4)
    result.addUnsafeEnchantment(Enchantment.SWEEPING_EDGE, 4)
    result.addUnsafeEnchantment(Enchantment.DURABILITY, 4)
    result.addEnchantment(Enchantment.MENDING, 1)
    CleansingEnchant.enchant(result, 1)
    val meta = result.itemMeta ?: return result
    meta.setDisplayName("${darkRed}Cultist Sword")
    result.itemMeta = meta
    return result
}

const val CULTIST_SHIELD_ID = 106
const val CULTIST_SHIELD_COST = 576
fun CultistShield(amount: Int): ItemStack {
    val result = ItemStack(Material.SHIELD, amount)
    result.addUnsafeEnchantment(Enchantment.DURABILITY, 4)
    result.addEnchantment(Enchantment.MENDING, 1)
    BloodBashEnchant.enchant(result, 1)
    val meta = result.itemMeta ?: return result
    meta.setDisplayName("${darkRed}Cultist Shield")
    result.itemMeta = meta
    return result
}