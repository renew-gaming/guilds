package com.github.snarechops.renew.features.cultshop

import org.bukkit.Bukkit
import org.bukkit.command.Command
import org.bukkit.command.CommandSender
import org.bukkit.command.TabCompleter
import org.bukkit.entity.Player

object CultCompletion : TabCompleter {
    override fun onTabComplete(
        sender: CommandSender,
        command: Command,
        alias: String,
        args: Array<out String>
    ): MutableList<String> {
        return when {
            args.isEmpty() -> root(sender, "")
            args.size == 1 -> root(sender, args[0])
            args[0] == "give" -> give(sender, args.drop(1))
            else -> mutableListOf()
        }
    }

    private fun root(sender: CommandSender, filter: String): MutableList<String> {
        val list = mutableListOf<String>()
        if (sender.hasPermission(CultAdminPermission) && sender is Player) {
            list.add("give")
        }
        list.retainAll { it.startsWith(filter, ignoreCase = true) }
        list.sort()
        return list
    }

    private fun give(sender: CommandSender, args: List<String>): MutableList<String> {
        if (!sender.hasPermission(CultAdminPermission) || sender !is Player) return mutableListOf()
        fun items(filter: String): MutableList<String> {
            val list = mutableListOf(
                "coins",
                "cultist_helm",
                "cultist_chest",
                "cultist_legs",
                "cultist_boots",
                "cultist_wings",
                "cultist_shield",
                "cultist_sword",
            )
            list.retainAll { it.startsWith(filter, ignoreCase = true) }
            list.sort()
            return list
        }

        fun players(filter: String): MutableList<String> {
            val list = Bukkit.getOnlinePlayers().map { it.name }.toMutableList()
            list.retainAll { it.startsWith(filter, ignoreCase = true) }
            list.sort()
            return list
        }

        return when (args.size) {
            1 -> items(args[0])
            2 -> players(args[1])
            3 -> mutableListOf("#")
            else -> items("")
        }
    }
}