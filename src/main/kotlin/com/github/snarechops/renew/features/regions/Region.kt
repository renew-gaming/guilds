package com.github.snarechops.renew.features.regions

import org.bukkit.Location
import org.bukkit.event.Listener

open class Region(val min: Location, val max: Location) : Listener{
    fun contains(loc: Location): Boolean {
        if (loc.world != min.world) return false
        if (loc.x < min.x || loc.x > max.x) return false
        if (loc.y < min.y || loc.y > max.y) return false
        if (loc.z < min.z || loc.z > max.z) return false
        return true
    }
}