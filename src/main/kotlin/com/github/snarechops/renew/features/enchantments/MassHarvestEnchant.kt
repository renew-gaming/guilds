package com.github.snarechops.renew.features.enchantments

import com.github.snarechops.renew.common.*
import org.bukkit.GameMode
import org.bukkit.Material
import org.bukkit.block.Block
import org.bukkit.entity.Player
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.block.BlockBreakEvent
import org.bukkit.inventory.ItemStack

object MassHarvestEnchant : Listener, CustomEnchant(
    "mass_harvest",
    "Mass Harvest",
    1,
    3,
    blue
) {
    var ignore = setOf<Player>()
    fun amount(level: Int): Int = 64 * level


    override fun description(level: Int): List<String> = listOf(
        "$gold- Harvests all blocks connected to this block",
        "$gold- Maximum ${amount(level)} blocks ${gray}Max: 3"
    )

    override fun canEnchantItem(item: ItemStack): Boolean = item.isHoe()

    @EventHandler(ignoreCancelled = true)
    fun onBlockBreak(event: BlockBreakEvent) {
        if (ignore.contains(event.player)) return
        if (event.player.gameMode != GameMode.SURVIVAL) return
        if (!event.block.isHarvestable()) return
        val tool = event.player.inventory.itemInMainHand
        val level = has(tool)
        if (level == 0) return
        val blocks = event.block.floodScan(amount(level)) { it.isHarvestable() }
        harvest(event.player, blocks, tool)
    }

    private fun harvest(player: Player, blocks: List<Block>, tool: ItemStack) {
        if (blocks.isEmpty()) return
        ignore = ignore + player
        val block = blocks.first()
        val event = BlockBreakEvent(block, player)
        emit(event)
        if (!event.isCancelled) block.breakNaturally(tool)

        ignore.minus(player)
        nextTick { harvest(player, blocks.drop(1), tool) }
    }
}

fun Block.isHarvestable(): Boolean = listOf(
    Material.WHEAT,
    Material.CARROTS,
    Material.BEETROOTS,
    Material.POTATOES,
).contains(this.type)