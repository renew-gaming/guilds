package com.github.snarechops.renew.features.commands

import com.github.snarechops.renew.common.MustBePlayer
import com.github.snarechops.renew.common.emit
import com.github.snarechops.renew.features.books.Book
import com.github.snarechops.renew.features.books.ShowBookEvent
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import org.bukkit.command.TabCompleter
import org.bukkit.entity.Player

object CommandsCommand : CommandExecutor {
    override fun onCommand(sender: CommandSender, command: Command, label: String, args: Array<out String>): Boolean {
        if (sender !is Player) return MustBePlayer(sender, CommandsTag)
        val pages = CommandFeature.pages.map { it.toString(sender) }.filterNot { it == "" }.toMutableList()
        emit(ShowBookEvent(sender, Book("Commands", "Commands", pages)))
        return true
    }
}

object CommandsCompletion : TabCompleter {
    override fun onTabComplete(sender: CommandSender, command: Command, alias: String, args: Array<out String>): MutableList<String> = mutableListOf()
}