package com.github.snarechops.renew.features.commands

import com.github.snarechops.renew.Feature
import com.github.snarechops.renew.Renew
import com.github.snarechops.renew.RenewPlugin
import com.github.snarechops.renew.common.*
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import org.bukkit.command.TabCompleter
import org.bukkit.entity.Player

val CommandsTag = pluginTag("Commands")

@Target(AnnotationTarget.FUNCTION)
@Repeatable
annotation class Help(val permission: String = "", val text: String)

@Target(AnnotationTarget.CLASS)
annotation class HelpTopic(val name: String)

data class HelpPage(val title: String, val items: List<HelpItem>)
data class HelpItem(val permission: String, val text: String)

object CommandFeature : Feature {
    val pages: MutableList<HelpPage> = mutableListOf()

    override fun onInit() {
        CommandsCommand.register("commands", CommandsCompletion)
    }

    override fun onDestroy() {}
}

fun CommandExecutor.extractHelp(): HelpPage? {
    if (!this::class.java.isAnnotationPresent(HelpTopic::class.java)) return null
    val topic = this::class.java.getAnnotation(HelpTopic::class.java)
    val items = this::class.java.declaredMethods.map {
        if (it.isAnnotationPresent(Help::class.java))
            return@map it.getAnnotation(Help::class.java)
        else
            return@map null
    }.filterNotNull().map { HelpItem(it.permission, it.text) }
    return HelpPage(topic.name, items)
}

fun HelpPage.toString(player: Player): String {
    val items = this.items.filter { it.permission == "" || player.hasPermission(it.permission) }.map { it.text }
    if (items.isEmpty()) return ""
    return "$gold$underline${this.title}\n${items.joinToString("\n\n")}"
}