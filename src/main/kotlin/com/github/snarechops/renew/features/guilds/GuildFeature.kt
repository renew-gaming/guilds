package com.github.snarechops.renew.features.guilds

import com.github.snarechops.renew.Feature
import com.github.snarechops.renew.Renew
import com.github.snarechops.renew.common.Database
import com.github.snarechops.renew.common.register
import com.mongodb.client.MongoCollection
import org.bukkit.permissions.Permission
import org.litote.kmongo.SetTo
import org.litote.kmongo.eq
import org.litote.kmongo.findOne
import org.litote.kmongo.set

const val GuildsAdminPermission = "guilds.admin"
const val GuildsTeleportBypassPermission = "guilds.teleport.bypass"
fun GuildsTeleportPermission(id: String) = "guilds.teleport.$id"

val GuildCollection: MongoCollection<Guild>
    get() = Database.getDatabase("guilds").getCollection("guild", Guild::class.java)

object GuildFeature: Feature {
    override fun onInit() {
        GuildCommand.register("guild", GuildCompletion)
        get().forEach {
            Renew.server.pluginManager.permissions += Permission(GuildsTeleportPermission(it.id))
        }
    }

    override fun onDestroy() {}

    fun get(): List<Guild> =
        GuildCollection.find().toList()

    fun getOne(id: String): Guild? =
        GuildCollection.findOne(Guild::id eq id)

    fun save(guild: Guild) =
        GuildCollection.updateOne(
        Guild::id eq guild.id, set(
            SetTo(Guild::name, guild.name),
            SetTo(Guild::description, guild.description),
            SetTo(Guild::location, guild.location),
        )
    )
}