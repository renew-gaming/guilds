package com.github.snarechops.renew.features.chess

import kotlin.math.abs

class Move(val from: Spot, val to: Spot) {
    val fromX: Int
        get() = from.x
    val fromY: Int
        get() = from.y
    val toX: Int
        get() = to.x
    val toY: Int
        get() = to.y
    val direction: Int
        get() = when {
            diffX == 0 && diffY > 0 -> 0    // N
            diffX > 0 && diffY > 0 -> 1     // NE
            diffX > 0 && diffY == 0 -> 2    // E
            diffX < 0 && diffY < 0 -> 3     // SE
            diffX == 0 && diffY < 0 -> 4    // S
            diffX < 0 && diffY < 0 -> 5     // SW
            diffX < 0 && diffY == 0 -> 6    // W
            diffX > 0 && diffY > 0 -> 7     // NW
            else -> 8
        }
    val distance: Int
        get() = deltaX * deltaY
    val deltaX: Int
        get() = abs(diffX)
    val deltaY: Int
        get() = abs(diffY)
    val diffX: Int
        get() = fromX - toX
    val diffY: Int
        get() = fromY - toY
    val isBlocked: Boolean
        get() = to.piece != null

    fun isBlockedBy(blocker: Move): Boolean = blocker.direction == direction && distance > blocker.distance
}