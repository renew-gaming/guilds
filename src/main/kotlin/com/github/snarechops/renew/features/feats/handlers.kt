package com.github.snarechops.renew.features.feats

import com.github.snarechops.renew.features.stats.StatChanged
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener

class QuestFeat(val feature: FeatsFeature, private val baseId: String): Listener {
    @EventHandler(ignoreCancelled = true)
    fun onStatChanged(event: StatChanged<Int>) {
        println("Quest Stat Changed: ${event.stat.key} ${event.stat.value}")
        if (event.stat.key == baseId && event.stat.value != null)
            feature.credit(event.player, "${baseId}_${event.stat.value}")
    }
}
