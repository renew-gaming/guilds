package com.github.snarechops.renew.features.enchantments

import com.github.snarechops.renew.common.darkRed
import com.github.snarechops.renew.common.gold
import com.github.snarechops.renew.common.gray
import org.bukkit.Material
import org.bukkit.entity.Player
import org.bukkit.inventory.ItemStack
import org.bukkit.potion.PotionEffect
import org.bukkit.potion.PotionEffectType

object BloodDropEnchant : PassiveEnchant, CustomEnchant(
    "blood_drop",
    "Blood Drop",
    1,
    1,
    darkRed,
) {
    override fun description(level: Int): List<String> = listOf(
        "$gold- Grants Slow Falling while wearing",
        "$gold- this Elytra ${gray}Max: 1"
    )

    private fun effect(level: Int) = PotionEffect(PotionEffectType.SLOW_FALLING, 6 * 20, level, false, false)

    override fun canEnchantItem(item: ItemStack): Boolean = when (item.type) {
        Material.ELYTRA -> true
        else -> false
    }

    override fun tick(player: Player, level: Int) {
        player.addPotionEffect(effect(if (level > max) max else level))
    }
}