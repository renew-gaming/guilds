package com.github.snarechops.renew.features.chess

import org.bukkit.Location
import org.bukkit.Material
import org.bukkit.block.Block
import org.bukkit.event.EventHandler
import org.bukkit.event.HandlerList
import org.bukkit.event.Listener
import org.bukkit.event.player.PlayerInteractEvent

enum class Team {
    WHITE,
    BLACK
}

fun Team.opposite(): Team = when(this) {
    Team.WHITE -> Team.BLACK
    Team.BLACK -> Team.WHITE
}

fun Location.getRelative(modX: Int, modY: Int, modZ: Int): Location {
    return Location(this.world, this.x + modX, this.y + modY, this.z + modZ)
}

class Spot(val board: Board, val x: Int, val y: Int) : Listener {
    val location = board.origin.getRelative(2 * x, 0, 2 * y)
    var piece: Piece? = null
    private var checkHighlight: Boolean = false
    private val type = location.block.getRelative(0, -1, 0).type


    fun hasTeam(team: Team) = piece?.team == team
    val isOccupied: Boolean
        get() = piece != null

    val blocks: List<Block>
        get() = listOf(
            location.block.getRelative(0, -1, 0),
            location.block.getRelative(-1, -1, 0),
            location.block.getRelative(0, -1, -1),
            location.block.getRelative(-1, -1, -1),
        )

    fun highlight(bool: Boolean, team: Team) {
        println("checkHighlight: $checkHighlight")
        println("isEnemy: ${piece?.isEnemy(team)}")
        if (checkHighlight) return
        blocks.forEach {
            it.type = when {
                !bool -> type
                piece?.isEnemy(team) == true -> Material.SHROOMLIGHT
                else -> Material.AMETHYST_BLOCK
            }
        }
    }

    fun highlightCheck(bool: Boolean) {
        checkHighlight = bool
        blocks.forEach { it.type = if (bool) Material.REDSTONE_BLOCK else type }
    }

    fun cleanup(){
        highlightCheck(false)
        highlight(false, Team.WHITE)
        piece?.kill()
        HandlerList.unregisterAll(this)
    }

    @EventHandler(ignoreCancelled = true)
    fun onPlayerInteract(event: PlayerInteractEvent) {
        if (!blocks.contains(event.clickedBlock)) return
        event.isCancelled = true
        if (event.player != board.piece?.owner) return
        board.select(this)
    }
}

fun Spot.getRelative(modX: Int, modY: Int): Spot? {
    val x = this.x + modX
    val y = this.y + modY
    if (x < 0 || x > 7) return null
    if (y < 0 || y > 7) return null
    return board.spots[x][y]
}










