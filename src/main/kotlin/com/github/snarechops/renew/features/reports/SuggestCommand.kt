package com.github.snarechops.renew.features.reports

import com.github.snarechops.renew.common.black
import com.github.snarechops.renew.common.darkBlue
import com.github.snarechops.renew.common.err
import com.github.snarechops.renew.common.ok
import com.github.snarechops.renew.features.commands.Help
import com.github.snarechops.renew.features.commands.HelpTopic
import kotlinx.coroutines.runBlocking
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player

@HelpTopic("Suggestions")
object SuggestCommand: CommandExecutor {

    @Help("", "$darkBlue/suggest <message...> $black- Logs your suggestion in the #suggestions channel in discord. Check there for updates, and discuss in #suggestions-discussion")
    override fun onCommand(sender: CommandSender, command: Command, label: String, args: Array<out String>): Boolean {
        if (args.isEmpty()) return err(sender, ReportsTag, "Please describe your suggestion. /suggest <message>")

        runBlocking {
            val player = if(sender is Player) sender.name else "Unknown"
            val message = args.joinToString(separator = " ")
            ReportFeature.createSuggestion(player, message)
        }
        return ok(sender, ReportsTag, "Suggestion sent!")
    }
}