package com.github.snarechops.renew.features.chess

import org.bukkit.Location
import org.bukkit.entity.*

fun WhitePawn(location: Location): LivingEntity? {
    val pawn = location.world?.spawnEntity(location, EntityType.VILLAGER) as Villager? ?: return null
    pawn.setAI(false)
    pawn.setBaby()
    pawn.isSilent = true
    pawn.profession = Villager.Profession.NITWIT
    pawn.setRotation(0f, 0f)
    pawn.ageLock = true
    pawn.customName = "Pawn"
    return pawn
}

fun WhiteRook(location: Location): LivingEntity? {
    val rook = location.world?.spawnEntity(location, EntityType.VILLAGER) as Villager? ?: return null
    rook.setAI(false)
    rook.setAdult()
    rook.isSilent = true
    rook.profession = Villager.Profession.MASON
    rook.setRotation(0f, 0f)
    rook.customName = "Rook"
    return rook
}

fun WhiteKnight(location: Location): LivingEntity? {
    val knight = location.world?.spawnEntity(location, EntityType.HORSE) as Horse? ?: return null
    knight.setAI(false)
    knight.setAdult()
    knight.isSilent = true
    knight.color = Horse.Color.BLACK
    knight.setRotation(0f, 0f)
    knight.customName = "Knight"
    return knight
}

fun WhiteBishop(location: Location): LivingEntity? {
    val bishop = location.world?.spawnEntity(location, EntityType.VILLAGER) as Villager? ?: return null
    bishop.setAI(false)
    bishop.setAdult()
    bishop.isSilent = true
    bishop.profession = Villager.Profession.CLERIC
    bishop.setRotation(0f, 0f)
    bishop.customName = "Bishop"
    return bishop
}

fun WhiteQueen(location: Location): LivingEntity? {
    val queen = location.world?.spawnEntity(location, EntityType.VILLAGER) as Villager? ?: return null
    queen.setAI(false)
    queen.setAdult()
    queen.isSilent = true
    queen.profession = Villager.Profession.BUTCHER
    queen.villagerType = Villager.Type.SWAMP
    queen.setRotation(0f, 0f)
    queen.customName = "Queen"
    return queen
}

fun WhiteKing(location: Location): LivingEntity? {
    val king = location.world?.spawnEntity(location, EntityType.WANDERING_TRADER) as WanderingTrader? ?: return null
    king.setAI(false)
    king.setAdult()
    king.isSilent = true
    king.setRotation(0f, 0f)
    king.despawnDelay = -1
    king.customName = "King"
    return king
}