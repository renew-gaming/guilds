package com.github.snarechops.renew.features.upgradetokens

import com.github.snarechops.renew.common.gold
import com.github.snarechops.renew.common.gray
import com.github.snarechops.renew.common.green
import com.github.snarechops.renew.common.obfuscated
import com.github.snarechops.renew.features.enchantments.*
import org.bukkit.Material
import org.bukkit.enchantments.Enchantment
import org.bukkit.inventory.ItemStack

const val UPGRADE_VANILLA_PREFIX = "${obfuscated}upgrade_vanilla:"
const val UPGRADE_CUSTOM_PREFIX = "${obfuscated}upgrade_custom:"

fun UpgradeToken(name: String, enchantment: Enchantment): ItemStack {
    val item = ItemStack(Material.NETHER_STAR)
    val meta = item.itemMeta!!
    meta.lore = listOf(
        "${green}Upgrades the $name enchantment",
        "${green}level of an item.",
        "${gray}Maximum level: X",
        "${gold}Use at an anvil",
        "$UPGRADE_VANILLA_PREFIX${enchantment.name}"
    )
    meta.setDisplayName("$name Upgrade Token")
    meta.addEnchant(enchantment, 1, false)
    item.itemMeta = meta
    return item
}

fun CustomUpgradeToken(enchant: CustomEnchant): ItemStack {
    val item = ItemStack(Material.NETHER_STAR)
    val meta = item.itemMeta!!
    meta.lore = listOf(
        "${green}Upgrades the ${enchant.name} enchantment",
        "${green}level of an item.",
        "${gray}Maximum level: ${enchant.max}",
        "${gold}Use at an anvil",
        "$UPGRADE_CUSTOM_PREFIX${enchant.id}"
    )
    meta.setDisplayName("${enchant.name} Upgrade Token")
    item.itemMeta = meta
    return item
}

val Tokens = mapOf(
    "fortune" to UpgradeToken("Fortune", Enchantment.LOOT_BONUS_BLOCKS),
    "efficiency" to UpgradeToken("Efficiency", Enchantment.DIG_SPEED),
    "looting" to UpgradeToken("Looting", Enchantment.LOOT_BONUS_MOBS),
    "unbreaking" to UpgradeToken("Unbreaking", Enchantment.DURABILITY),
    "sharpness" to UpgradeToken("Sharpness", Enchantment.DAMAGE_ALL),
    "power" to UpgradeToken("Power", Enchantment.ARROW_DAMAGE),
    "punch" to UpgradeToken("Punch", Enchantment.ARROW_KNOCKBACK),
    "bane" to UpgradeToken("Bane of Arthropods", Enchantment.DAMAGE_ARTHROPODS),
    "smite" to UpgradeToken("Smite", Enchantment.DAMAGE_UNDEAD),
    "blood_armor" to CustomUpgradeToken(BloodArmorEnchant),
    "blood_bash" to CustomUpgradeToken(BloodBashEnchant),
    "blood_boost" to CustomUpgradeToken(BloodBoostEnchant),
    "blood_drop" to CustomUpgradeToken(BloodDropEnchant),
    "blood_lust" to CustomUpgradeToken(BloodLustEnchant),
    "blood_regen" to CustomUpgradeToken(BloodRegenEnchant),
    "cleansing" to CustomUpgradeToken(CleansingEnchant),
    "jump_boost" to CustomUpgradeToken(JumpBoostEnchant),
    "mass_harvest" to CustomUpgradeToken(MassHarvestEnchant),
    "plowing" to CustomUpgradeToken(PlowingEnchant),
    "snorkel" to CustomUpgradeToken(SnorkelEnchant),
    "swift" to CustomUpgradeToken(SnorkelEnchant),
    "tunnel" to CustomUpgradeToken(TunnelEnchant),
)

//val FortuneUpgradeToken = UpgradeToken("Fortune", Enchantment.LOOT_BONUS_BLOCKS)
//val EfficiencyUpgradeToken = UpgradeToken("Efficiency", Enchantment.DIG_SPEED)
//val LootingUpgradeToken = UpgradeToken("Looting", Enchantment.LOOT_BONUS_MOBS)
//val UnbreakingUpgradeToken = UpgradeToken("Unbreaking", Enchantment.DURABILITY)
//val SharpnessUpgradeToken = UpgradeToken("Sharpness", Enchantment.DAMAGE_ALL)
//val PowerUpgradeToken = UpgradeToken("Power", Enchantment.ARROW_DAMAGE)
//val PunchUpgradeToken = UpgradeToken("Punch", Enchantment.ARROW_KNOCKBACK)
//val BaneUpgradeToken = UpgradeToken("Bane of Arthropods", Enchantment.DAMAGE_ARTHROPODS)
//val SmiteUpgradeToken = UpgradeToken("Smite", Enchantment.DAMAGE_UNDEAD)