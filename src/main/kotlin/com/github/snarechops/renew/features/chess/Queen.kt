package com.github.snarechops.renew.features.chess

import org.bukkit.entity.Player

class Queen(player: Player, team: Team, spot: Spot) : Piece(player, team, spot, when (team) {
    Team.WHITE -> ::WhiteQueen
    Team.BLACK -> ::BlackQueen
}) {

    override fun moves(): List<Spot> {
        val result = mutableListOf<Spot>()
        result.addAll(walk(1, 0))
        result.addAll(walk(-1,0))
        result.addAll(walk(0, 1))
        result.addAll(walk(0, -1))
        result.addAll(walk(1, 1))
        result.addAll(walk(1, -1))
        result.addAll(walk(-1, -1))
        result.addAll(walk(-1, 1))
        return result
    }


}