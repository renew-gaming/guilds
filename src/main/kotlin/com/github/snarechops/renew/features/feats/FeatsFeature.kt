package com.github.snarechops.renew.features.feats

import com.github.snarechops.renew.Feature
import com.github.snarechops.renew.Renew
import com.github.snarechops.renew.common.*
import com.github.snarechops.renew.features.crates.name
import com.mongodb.client.MongoCollection
import com.mongodb.client.model.FindOneAndReplaceOptions
import org.bukkit.Bukkit
import org.bukkit.entity.Player
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.inventory.InventoryClickEvent
import org.bukkit.inventory.ItemStack
import org.litote.kmongo.eq
import org.litote.kmongo.findOne
import kotlin.math.ceil

const val FEATS_PREFIX = "${lightPurple}Feats: $gold"
val RewardsTag = pluginTag("Rewards")
var globalFeats: List<GlobalFeat> = listOf()

val FeatCollection: MongoCollection<GlobalFeat>
    get() = Database.getDatabase("renew").getCollection("feat", GlobalFeat::class.java)

val PlayerFeatCollection: MongoCollection<Feats>
    get() = Database.getDatabase("renew").getCollection("feat.player", Feats::class.java)

object FeatsFeature : Feature, Listener {
    private val pages: Int
        get() = ceil(globalFeats.size.toDouble() / 45.0).toInt()

    override fun onInit() {
        FeatsCommand.register("feats", FeatsCompletion)
        this.register()
        reload()
        listOf(
            QuestFeat(this, "quests_complete_adventurer"),
            QuestFeat(this, "quests_complete_artisan"),
            QuestFeat(this, "quests_complete_blacksmith"),
            QuestFeat(this, "quests_complete_carpentry"),
            QuestFeat(this, "quests_complete_engineer"),
            QuestFeat(this, "quests_complete_farmer"),
            QuestFeat(this, "quests_complete_fishing"),
            QuestFeat(this, "quests_complete_gardening"),
            QuestFeat(this, "quests_complete_miner"),
            QuestFeat(this, "quests_complete_stonemason"),
            QuestFeat(this, "quests_complete_wizard"),
        ).forEach { it.register() }
    }

    override fun onDestroy() {}

    fun reload() {
        globalFeats = FeatCollection.find().toList()
    }

    fun getGlobalFeat(id: String): GlobalFeat? =
        FeatCollection.findOne(GlobalFeat::id eq id)

    private fun save(global: GlobalFeat): GlobalFeat? =
        FeatCollection.let {
            nextTick { reload() }
            it.findOneAndReplace(GlobalFeat::id eq global.id, global, FindOneAndReplaceOptions().upsert(true))
        }

    fun getFeats(player: Player): Feats =
        PlayerFeatCollection.findOne(Feats::uuid eq player.uniqueId.toString())
            ?: Feats(player.uniqueId.toString(), listOf(), listOf())

    private fun save(playerFeats: Feats): Feats? =
        PlayerFeatCollection.findOneAndReplace(
            Feats::uuid eq playerFeats.uuid,
            playerFeats,
            FindOneAndReplaceOptions().upsert(true)
        )

    fun show(player: Player, target: Player, page: Int) {
        val inventory = Bukkit.createInventory(player, 54, "$FEATS_PREFIX${player.name}")
        getUI(target, page).forEach { inventory.addItem(it) }
        inventory.setItem(45, if(page <= 1) NoPageButton() else PageButton(page - 1))
        inventory.setItem(53, if(page >= pages) NoPageButton() else PageButton(page + 1))
        player.openInventory(inventory)
    }

    fun credit(player: Player, id: String): Boolean {
        val feats = getFeats(player)
        val feat = feats.globals().find { it.id == id } ?: return false
        feats.complete(id)
        feats.reward(id)
        save(feats)
        feat.rewards.give(player)
        return ok(player, RewardsTag, "Feat ${feat.name} Completed!")
    }

    private fun getUI(player: Player, page: Int): List<ItemStack> {
        val feats = getFeats(player)
        val showId = player.hasPermission(FeatsAdminPermission)
        val (completed, active) = feats.partition()
        val completedItems = completed.map { it.displayItem(true, showId) }
        val activeItems = active.map { it.displayItem(false, showId) }
        return activeItems.plus(completedItems).drop((page-1) * 45).take(45)
    }

    fun addFeatRewardItem(player: Player, feat: GlobalFeat, rewardId: String): Boolean {
        val item = player.inventory.itemInMainHand
        if (item.isAir()) return err(player, RewardsTag, "Invalid item to add as a reward")
        feat.rewards += Reward(
            "${feat.id}_$rewardId",
            item.name,
            item.encodeString(),
        )
        if (save(feat) == null) return false
        return true
    }

    fun removeFeatReward(feat: GlobalFeat, rewardId: String): Boolean {
        feat.rewards = feat.rewards.filterNot { it.id == rewardId }
        if (save(feat) == null) return false
        return true
    }

    @EventHandler(ignoreCancelled = true)
    fun onFeatsView(event: InventoryClickEvent) {
        if (event.view.title.startsWith(FEATS_PREFIX)) event.isCancelled = true
        val name = event.view.title.removePrefix(FEATS_PREFIX)
        val target = Bukkit.getPlayer(name) ?: return
        for (page in 1..pages) {
            if (event.currentItem != PageButton(page)) continue
            show(event.whoClicked as Player, target, page)
        }
    }
}