package com.github.snarechops.renew.features.shops

import com.github.snarechops.renew.common.SimpleLocation
import org.bukkit.Bukkit
import java.util.*

data class Shop(
    val location: SimpleLocation,
    val uuid: String,
    var product: String,
    var cost: String,
)

val Shop.owner
    get() = Bukkit.getOfflinePlayer(UUID.fromString(this.uuid))