package com.github.snarechops.renew.features.enchantments

import com.github.snarechops.renew.common.darkRed
import com.github.snarechops.renew.common.gold
import com.github.snarechops.renew.common.gray
import org.bukkit.Material
import org.bukkit.entity.Player
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.entity.EntityDamageByEntityEvent
import org.bukkit.inventory.ItemStack

object BloodBashEnchant : Listener, CustomEnchant(
    "blood_bash",
    "Blood Bash",
    1,
    3,
    darkRed,
) {
    override fun description(level: Int): List<String> = listOf(
        "$gold- Throws an enemy backwards when they attack",
        "$gold- while blocking with this shield ${gray}Max: 3"
    )

    override fun canEnchantItem(item: ItemStack): Boolean = item.type == Material.SHIELD

    @EventHandler(ignoreCancelled = true)
    fun onEntityDamageByEntity(event: EntityDamageByEntityEvent) {
        if (event.entity !is Player) return
        val player = event.entity as Player
        if (!player.isBlocking) return
        val shield = when {
            player.inventory.itemInMainHand.type == Material.SHIELD -> player.inventory.itemInMainHand
            player.inventory.itemInOffHand.type == Material.SHIELD -> player.inventory.itemInOffHand
            else -> return
        }
        val level = has(shield)
        val vector = event.damager.location.toVector()
        val pos = player.location.toVector()
        val velocity = vector.subtract(pos)
        event.damager.velocity = velocity.normalize().multiply(level)
    }
}