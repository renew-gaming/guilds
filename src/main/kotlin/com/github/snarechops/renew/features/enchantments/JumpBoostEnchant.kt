package com.github.snarechops.renew.features.enchantments

import com.github.snarechops.renew.common.blue
import com.github.snarechops.renew.common.gold
import com.github.snarechops.renew.common.gray
import com.github.snarechops.renew.common.isBoots
import org.bukkit.entity.Player
import org.bukkit.inventory.ItemStack
import org.bukkit.potion.PotionEffect
import org.bukkit.potion.PotionEffectType

object JumpBoostEnchant : PassiveEnchant, CustomEnchant(
    "jump_boost",
    "Jump Boost",
    1,
    2,
    blue,
) {
    override fun description(level: Int): List<String> = listOf(
        "$gold- Grants Leaping $level while wearing ${gray}Max: 2"
    )

    private fun effect(level: Int): PotionEffect = PotionEffect(PotionEffectType.JUMP, 30 * 20, level)

    override fun canEnchantItem(item: ItemStack): Boolean = item.isBoots()

    override fun tick(player: Player, level: Int) {
        player.addPotionEffect(effect(level))
    }
}