package com.github.snarechops.renew.features.locker

import org.bukkit.Bukkit
import org.bukkit.command.Command
import org.bukkit.command.CommandSender
import org.bukkit.command.TabCompleter
import org.bukkit.entity.Player

object LockerCompletion : TabCompleter {
    override fun onTabComplete(
        sender: CommandSender,
        command: Command,
        alias: String,
        args: Array<out String>
    ): MutableList<String> {
        if (sender !is Player) return mutableListOf()
        if (!sender.hasPermission(LockerAdminPermission)) return mutableListOf()
        return Bukkit.getOnlinePlayers().map {it.name}
            .let{ list -> list.filter { it.startsWith(args.getOrElse(0) { "" }, ignoreCase = true) }}
            .sorted()
            .toMutableList()

    }
}