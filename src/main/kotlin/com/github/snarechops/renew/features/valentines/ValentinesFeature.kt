package com.github.snarechops.renew.features.valentines

import com.github.snarechops.renew.Feature
import com.github.snarechops.renew.Renew
import com.github.snarechops.renew.common.*
import com.mongodb.client.MongoCollection
import com.mongodb.client.model.ReplaceOptions
import org.bukkit.block.Sign
import org.bukkit.enchantments.Enchantment
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.block.BlockBreakEvent
import org.bukkit.event.block.SignChangeEvent
import org.bukkit.event.player.PlayerInteractEvent
import org.bukkit.inventory.ItemStack
import org.bukkit.potion.PotionEffect
import org.bukkit.potion.PotionEffectType
import org.litote.kmongo.eq
import org.litote.kmongo.findOne

const val VALENTINES_DAY_SIGN_TITLE = "[Valentines Day]"
const val VALENTINES_DAY_ROSE_NAME = "${darkRed}Lover's Passionate Rose"
const val ValentinesAdminPermission = "valentines.admin"
val ValentinesTag = pluginTag("Valentines")

val ValentineCollection: MongoCollection<Valentine>
    get() = Database.getDatabase("renew").getCollection("valentine", Valentine::class.java)

object ValentinesFeature: Feature, Listener {
    override fun onInit() {
        ValentinesCommand.register("valentines", ValentinesCompletion)
        this.register()
        Renew.server.scheduler.runTaskTimer(Renew, Runnable {
            Renew.server.onlinePlayers.forEach { player ->
                if (!isValentineRose(player.inventory.itemInMainHand) && !isValentineRose(player.inventory.itemInOffHand)) return@forEach
                player.addPotionEffect(PotionEffect(PotionEffectType.REGENERATION, 200, 1))
            }
        }, 120, 120)
    }

    override fun onDestroy() {}

    fun getOne(uuid: String): Valentine? =
        ValentineCollection.findOne(Valentine::uuid eq uuid)

    fun save(valentine: Valentine) {
        ValentineCollection.replaceOne(Valentine::uuid eq valentine.uuid, valentine, ReplaceOptions().upsert(true))
    }

    @EventHandler
    fun onSignInteract(event: PlayerInteractEvent) {
        val block = event.clickedBlock ?: return
        if (!block.isSign()) return
        val sign = block.state as Sign
        if (sign.getLine(0) != VALENTINES_DAY_SIGN_TITLE) return
        val valentine = getOne(event.player.uniqueId.toString()) ?: Valentine(event.player.uniqueId.toString(), false)
        if (valentine.parkourReward) {
            err(event.player, ValentinesTag, "You have already received this reward")
            return
        }
        val console = Renew.server.consoleSender
        // 10 free town plots
        Renew.server.dispatchCommand(console, "ta givebonus ${event.player.name} 10")
        // 2 Mythic Keys
        Renew.server.dispatchCommand(console, "crate givekey ${event.player.name} mythic 2")
        // Wither Rose
        event.player.inventory.addItem(ValentineWitherRose())
        valentine.parkourReward = true
        save(valentine)
        ok(event.player, ValentinesTag, "Congratulations on completing the Valentine's Day Parkour!")
        ok(event.player, ValentinesTag, "You have been rewarded 10 free town plots for your town,")
        ok(event.player, ValentinesTag, "2 Mythic Keys, and 1 Love's Passionate Rose")
        return
    }

    @EventHandler(ignoreCancelled = true)
    fun onSignChange(event: SignChangeEvent) {
        if (event.getLine(0) != VALENTINES_DAY_SIGN_TITLE) return
        if (event.player.hasPermission(ValentinesAdminPermission)) return
        event.isCancelled = true
        err(event.player, ValentinesTag, "Cannot create ${event.lines[0]} signs")
    }

    @EventHandler(ignoreCancelled = true)
    fun onBlockBreak(event: BlockBreakEvent) {
        if(!event.block.isSign()) return
        val sign = event.block.state as Sign
        if(sign.getLine(0) != VALENTINES_DAY_SIGN_TITLE) return
        if (event.player.hasPermission(ValentinesAdminPermission)) return
        event.isCancelled = true
        err(event.player, ValentinesTag, "Cannot destroy ${sign.getLine(0)} signs")
    }

    private fun isValentineRose(item: ItemStack): Boolean {
        if (!item.hasItemMeta()) return false
        val meta = item.itemMeta ?: return false
        if (meta.displayName != VALENTINES_DAY_ROSE_NAME) return false
        if (!meta.hasEnchant(Enchantment.MENDING)) return false
        return true
    }
}



