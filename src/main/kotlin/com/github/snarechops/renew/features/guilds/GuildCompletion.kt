package com.github.snarechops.renew.features.guilds

import org.bukkit.command.Command
import org.bukkit.command.CommandSender
import org.bukkit.command.TabCompleter
import org.bukkit.entity.Player

object GuildCompletion : TabCompleter {
    override fun onTabComplete(
        sender: CommandSender,
        command: Command,
        alias: String,
        args: Array<out String>
    ): MutableList<String> {
        if (sender !is Player) return mutableListOf()
        return when {
            args.isEmpty() -> root(sender, "")
            args.size == 1 -> root(sender, args[0])
            args[0] == "set" -> set(sender, args.drop(1))
            else -> mutableListOf()
        }
    }

    private fun root(sender: CommandSender, filter: String): MutableList<String> {
        val list = mutableListOf("help")
        if (sender.hasPermission(GuildsAdminPermission)) list.add("set")
        GuildNames.forEach { if (sender.hasPermission(GuildsTeleportBypassPermission) || sender.hasPermission(GuildsTeleportPermission(it))) list.add(it) else {} }
        list.retainAll { it.startsWith(filter, ignoreCase = true) }
        list.sort()
        return list
    }

    private fun set(sender: Player, args: List<String>): MutableList<String> {
        if (!sender.hasPermission(GuildsAdminPermission)) return mutableListOf()
        val list = GuildNames.toMutableList()
        list.retainAll { it.startsWith(args[0], ignoreCase = true) }
        list.sort()
        return list
    }
}

