package com.github.snarechops.renew.features.cultist

import org.bukkit.Bukkit
import org.bukkit.Location
import org.bukkit.Material
import org.bukkit.block.Block
import org.bukkit.entity.EntityType
import org.bukkit.entity.Player
import org.bukkit.entity.Skeleton
import org.bukkit.entity.Zombie
import kotlin.math.cos
import kotlin.math.sin
import kotlin.random.Random

class CultistSpawner(val region: CultistRegion) {

    val world = Bukkit.getWorld("world")
    val mobCap = 30

    fun spawn() {
        val player = world?.players?.randomOrNull() ?: return
        if (world.entities.filter { it.isCultist() }.size >= mobCap) return
        val location = randomPoint(player) ?: return
        if (!region.contains(location)) return
        when (val mob = world.spawnEntity(location, randomType())) {
            is Zombie -> ZombieCultist(mob)
            is Skeleton -> SkeletonCultist(mob)
            else -> {
            }
        }
    }

    private fun randomType(): EntityType {
        val rand = Random.nextDouble()
        return when {
            rand < 0.75 -> EntityType.ZOMBIE
            else -> EntityType.SKELETON
        }
    }

    private fun randomPoint(player: Player): Location? {
        val radius = Random.nextInt(30, 75)
        val deg = Random.nextInt(0, 360)
        val rad = deg.toDouble() * (Math.PI / 180.toDouble())
        val x = player.location.blockX.toDouble() + radius.toDouble() * sin(rad)
        val z = player.location.blockZ.toDouble() + radius.toDouble() * cos(rad)
        val block = world?.getHighestBlockAt(x.toInt(), z.toInt()) ?: return null
        if (!isSpawnable(block)) return null
        return Location(world, x, block.location.y + 1, z)
    }

    private fun isSpawnable(block: Block): Boolean = listOf(
        Material.GRASS_BLOCK,
        Material.DIRT,
        Material.COARSE_DIRT,
        Material.SAND,
        Material.RED_SAND,
        Material.TERRACOTTA,
        Material.YELLOW_TERRACOTTA,
        Material.LIGHT_GRAY_TERRACOTTA,
        Material.WHITE_TERRACOTTA,
        Material.RED_TERRACOTTA,
        Material.DIRT_PATH,
        Material.FARMLAND,
    ).contains(block.type)
}