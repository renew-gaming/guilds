package com.github.snarechops.renew.features.announcements

import com.github.snarechops.renew.Feature
import com.github.snarechops.renew.common.*
import com.mongodb.client.MongoCollection
import com.mongodb.client.model.FindOneAndReplaceOptions
import com.mongodb.client.model.ReturnDocument
import org.litote.kmongo.Id
import org.litote.kmongo.eq
import java.time.Instant
import kotlin.random.Random

const val AnnouncementAdminPermission = "announcement.admin"
val AnnouncerTag = pluginTag("Announcer")

val AnnouncementCollection: MongoCollection<Announcement>
    get() = Database.getDatabase("renew").getCollection("announcement", Announcement::class.java)

object AnnouncementFeature: Feature {
    var announcements: List<Announcement> = listOf()

    private val scheduled: List<Announcement>
        get() = announcements.filter { it.enabled }

    override fun onInit() {
        AnnouncementCommand.register("announce", AnnouncementCompletion)
        reload()
        repeatingTask(0, (60 * 20).toLong()) {
            scheduled.filter { it.enabled && it.timestamp < Instant.now() }
                .forEach { announce(it); it.timestamp = Instant.now().plusSeconds((it.period * 60).toLong()) }
        }
    }

    override fun onDestroy() {}

    fun announce(announcement: Announcement) {
        val padding = "----------------------------------------"
        broadcast(gold("$padding\n"), *announcement.message.colorize(), gold("\n$padding"))
    }

    fun reload() {
        announcements = get().map {
            if (it.enabled) {
                it.timestamp = Instant.now().plusSeconds(Random.nextLong(1, it.period.toLong()) * 60 )
                it
            }
            else
                it
        }
    }

    fun get(enabled: Boolean = false): List<Announcement> =
        if (enabled)
            AnnouncementCollection.find(Announcement::enabled eq true).toList()
        else
            AnnouncementCollection.find().toList()

    fun save(announcement: Announcement): Announcement? =
        AnnouncementCollection.findOneAndReplace(Announcement::id eq announcement.id, announcement, FindOneAndReplaceOptions().upsert(true).returnDocument(ReturnDocument.AFTER))

    fun delete(id: Id<Announcement>): Announcement? =
        AnnouncementCollection.findOneAndDelete(Announcement::id eq id)
}