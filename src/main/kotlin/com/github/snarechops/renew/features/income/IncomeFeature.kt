package com.github.snarechops.renew.features.income

import com.github.snarechops.renew.Feature
import com.github.snarechops.renew.Renew
import com.github.snarechops.renew.common.*
import net.milkbowl.vault.economy.Economy
import org.bukkit.GameMode
import org.bukkit.entity.*
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.block.BlockBreakEvent
import org.bukkit.event.block.BlockPlaceEvent
import org.bukkit.event.enchantment.EnchantItemEvent
import org.bukkit.event.entity.EntityDamageByEntityEvent
import org.bukkit.event.entity.EntityDeathEvent
import org.bukkit.event.hanging.HangingBreakByEntityEvent
import org.bukkit.event.hanging.HangingPlaceEvent
import org.bukkit.event.inventory.CraftItemEvent
import org.bukkit.event.player.PlayerFishEvent
import org.bukkit.event.player.PlayerShearEntityEvent

val IncomeTag = pluginTag("Income")

data class AttemptPurchaseEvent(val player: Player, val price: Double, val success: () -> Unit) : CustomEvent()

object IncomeFeature : Feature, Listener {
    val economy = Renew.server.pluginManager.getPlugin("Vault")?.let {
        Renew.server.servicesManager.getRegistration(Economy::class.java)?.provider
    }

    override fun onInit() {
        this.register()
    }

    override fun onDestroy() {}

    fun pay(player: Player) = economy?.depositPlayer(player, 0.75)
    fun pay(player: Player, entity: EntityType) = economy?.depositPlayer(player, 0.75)

    @EventHandler(ignoreCancelled = true)
    fun onAttemptPurchase(event: AttemptPurchaseEvent): Any =
        if (economy?.has(event.player, event.price) == true) {
            economy.withdrawPlayer(event.player, event.price)
            event.success()
        }
        else err(event.player, IncomeTag, "Insufficient funds")

    @EventHandler(ignoreCancelled = true)
    fun onBlockBreak(event: BlockBreakEvent) {
        if (event.player.gameMode != GameMode.SURVIVAL) return
        pay(event.player)
    }

    @EventHandler(ignoreCancelled = true)
    fun onBlockPlace(event: BlockPlaceEvent) {
        if (event.player.gameMode != GameMode.SURVIVAL) return
        pay(event.player)
    }

    @EventHandler(ignoreCancelled = true)
    fun onCraftItem(event: CraftItemEvent) {
        if (event.whoClicked.gameMode != GameMode.SURVIVAL) return
        if (event.whoClicked !is Player) return
        pay(event.whoClicked as Player)
    }

    @EventHandler(ignoreCancelled = true)
    fun onEnchantItem(event: EnchantItemEvent) {
        if (event.enchanter.gameMode != GameMode.SURVIVAL) return
        pay(event.enchanter)
    }

    @EventHandler(ignoreCancelled = true)
    fun onEntityDeath(event: EntityDeathEvent) {
        // Must know where the damage came from
        val cause = event.entity.lastDamageCause ?: return
        // Must have been damaged by another entity
        if (cause !is EntityDamageByEntityEvent) return
        // Must have been a living entity
        if (cause.entity !is LivingEntity) return
        // Get player that "did the damage"
        val damager = when (cause.damager) {
            is Player -> cause.damager as Player
            is Tameable -> {
                val pet = cause.damager as Tameable
                if (pet.isTamed && pet.owner is Player) pet.owner as Player else null
            }
            is Projectile -> {
                val projectile = cause.damager as Projectile
                if (projectile.shooter is Player) projectile.shooter as Player else null
            }
            else -> null
        } ?: return
        // Must not be an NPC
        if (damager.hasMetadata("NPC")) return
        if (damager.gameMode != GameMode.SURVIVAL) return
        pay(damager, event.entity.type)
    }

    @EventHandler(ignoreCancelled = true)
    fun onHangingBreak(event: HangingBreakByEntityEvent) {
        if (event.remover !is Player) return
        if ((event.remover as Player).gameMode != GameMode.SURVIVAL) return
        pay(event.remover as Player, event.entity.type)
    }

    @EventHandler(ignoreCancelled = true)
    fun onHangingPlace(event: HangingPlaceEvent) {
        if (event.player?.gameMode != GameMode.SURVIVAL) return
        pay(event.player as Player, event.entity.type)
    }

    @EventHandler(ignoreCancelled = true)
    fun onFish(event: PlayerFishEvent) {
        if (event.player.gameMode != GameMode.SURVIVAL) return
        if (event.state != PlayerFishEvent.State.CAUGHT_FISH) return
        if (event.caught !is Item) return
        pay(event.player)
    }

    @EventHandler(ignoreCancelled = true)
    fun onPlayerShearEntity(event: PlayerShearEntityEvent) {
        if (event.player.gameMode != GameMode.SURVIVAL) return
        pay(event.player)
    }
}