package com.github.snarechops.renew.features.quests

import com.github.snarechops.renew.common.*
import com.github.snarechops.renew.features.commands.Help
import com.github.snarechops.renew.features.commands.HelpTopic
import com.github.snarechops.renew.features.guilds.GuildName
import org.bukkit.Bukkit
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player

@HelpTopic("Quests")
object QuestCommand: CommandExecutor {
    override fun onCommand(sender: CommandSender, command: Command, label: String, args: Array<out String>): Boolean =
        when {
            args.isEmpty() -> show(sender)
            args[0] == "credit" -> credit(sender, args.drop(1))
            args[0] == "expire" -> expire(sender, args.drop(1))
            else -> guild(sender, args.toList())
        }

    @Help("", "$darkBlue/quests $black- Displays your current active quests")
    private fun show(sender: CommandSender): Boolean {
        if (sender !is Player) return MustBePlayer(sender, QuestsTag)
        QuestFeature.showQuests(sender)
        return true
    }

    @Help(QuestsAdminPermission, "$darkBlue/quest credit <player> <guild> $black- Credit the player's active quest for the provided guild, completing the quest so they can turn it in. Use this in the event a player can't complete a quest because of a technical issue")
    private fun credit(sender: CommandSender, args: List<String>): Boolean {
        if (!sender.hasPermission(QuestsAdminPermission)) return NotAllowed(sender, QuestsTag)
        val player = Bukkit.getPlayer(args.getOrNull(0) ?: "") ?: return err(sender, QuestsTag, "Player is not online")
        val guild = GuildName.fromString(args.getOrNull(1)?.uppercase()) ?: return err(sender, QuestsTag, "Invalid guild name")
        val quest = QuestFeature.creditQuest(player, guild) ?: return err(sender, QuestsTag, "Unable to credit quest. No matching player and guild quest combination")
        return ok(sender, QuestsTag, green("Quest"), gold(quest.name), green("has been credited to"), gold(quest.player.name))
    }

    @Help(QuestsAdminPermission, "$darkBlue/quest expire <player> <guild> $black- Expire the player's active quest for the provided guild, allowing them to get a new quest. Use this in the event of some technical error")
    private fun expire(sender: CommandSender, args: List<String>): Boolean {
        if (!sender.hasPermission(QuestsAdminPermission)) return NotAllowed(sender, QuestsTag)
        val player = Bukkit.getPlayer(args.getOrNull(0) ?: "") ?: return PlayerNotOnline(sender, QuestsTag)
        val guild = GuildName.fromString(args.getOrNull(1)?.uppercase()) ?: return err(sender, QuestsTag, "Invalid guild name")
        val quest = QuestFeature.expireQuest(player, guild) ?: return err(sender, QuestsTag, "Unable to expire quest. No matching player and guild quest combination")
        return ok(sender, QuestsTag, green("Quest"), gold(quest.name), green("has been expired for"), gold(quest.player.name))
    }

    private fun guild(
        sender: CommandSender,
        args: List<String>,
    ): Boolean {
        if (sender !is Player) return err(sender, QuestsTag, "Must be a player to view quests")
        val guild = GuildName.fromString(args.getOrNull(0)?.uppercase()) ?: return err(sender, QuestsTag, "Invalid guild name")
        if (args.size > 1 && !sender.hasPermission(QuestsAdminPermission)) return NotAllowed(sender, QuestsTag)
        val player = Bukkit.getPlayer(args.getOrNull(1) ?: "") ?: return PlayerNotOnline(sender, QuestsTag)
        QuestFeature.showQuest(sender, guild, player)
        return true
    }
}