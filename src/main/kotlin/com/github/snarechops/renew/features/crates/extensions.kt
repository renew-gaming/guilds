package com.github.snarechops.renew.features.crates

import org.bukkit.inventory.InventoryView
import org.bukkit.inventory.ItemStack

fun InventoryView.isCrateView(crate: Crate): Boolean =
    this.title == CrateViewTitle(crate)

fun InventoryView.isCrateRemoval(crate: Crate): Boolean =
    this.title == CrateRemoveTitle(crate)

fun InventoryView.isCrateManage(crate: Crate): Boolean =
    this.title == CrateManageTitle(crate)

val ItemStack.name: String
    get() = if ((this.itemMeta?.displayName ?: "") == "") this.type.name else this.itemMeta?.displayName ?: ""