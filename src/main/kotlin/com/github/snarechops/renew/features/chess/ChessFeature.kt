package com.github.snarechops.renew.features.chess

import com.github.snarechops.renew.Feature
import com.github.snarechops.renew.common.*
import com.mongodb.client.MongoCollection
import com.mongodb.client.model.FindOneAndReplaceOptions
import org.bukkit.Location
import org.bukkit.block.Sign
import org.bukkit.entity.Player
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.block.BlockBreakEvent
import org.bukkit.event.block.SignChangeEvent
import org.bukkit.event.player.PlayerInteractEvent
import org.bukkit.event.player.PlayerQuitEvent
import org.litote.kmongo.EMPTY_BSON
import org.litote.kmongo.findOne

const val CHESS_SIGN_TITLE = "[Chess]"
const val CHESS_VILLAGER_SIGN = "Villagers"
const val CHESS_ZOMBIE_SIGN = "Zombies"
const val ChessAdminPermission = "chess.admin"
val ChessTag = pluginTag("Chess")
fun CONTROL_OBTAIN(team: String) = arrayOf(ChessTag, green("You are now in control of the"), gold(team), green("chess pieces"))
fun CONTROL_LOSS(team: String) = arrayOf(ChessTag, yellow("You are no longer controlling the"), gold(team), yellow("chess pieces"))
fun MATCH_FORFEITED(winner: String, loser: String) = arrayOf(ChessTag, gold(winner), green("has won the chess match!"), gold(loser), green("has forfeited."))
val ADMIN_CLEARED = arrayOf(ChessTag, yellow("Chess game cleared by an admin"))

data class Chess(var enabled: Boolean, val location: PreciseLocation)

val ChessCollection: MongoCollection<Chess>
    get() = Database.getDatabase("renew").getCollection("chess", Chess::class.java)

object ChessFeature : Feature, Listener {
    var board: Board? = null
    var villagers: Player?
        set(value) {
            if (_villagers != null) ok(_villagers!!, *CONTROL_LOSS("Villager"))
            _villagers = value
            if (value != null) ok(value, *CONTROL_OBTAIN("Villager"))
        }
        get() = _villagers

    var zombies: Player?
        set(value) {
            if (_zombies != null) ok(_zombies!!, *CONTROL_LOSS("Zombie"))
            _zombies = value
            if (value != null) ok(value, *CONTROL_OBTAIN("Zombie"))
        }
        get() = _zombies

    var _villagers: Player? = null
    var _zombies: Player? = null

    override fun onInit() {
        ChessCommand.register("chess", ChessCompletion)
        this.register()
    }

    override fun onDestroy() = clear()

    fun setup(location: Location) {
        location.x = location.blockX.toDouble()
        location.y = location.blockY.toDouble()
        location.z = location.blockZ.toDouble()
        updateConfig(Chess(true, location.toPreciseLocation()))
    }

    fun startGameVillager(player: Player){
        if (zombies == player) zombies = null
        villagers = player
        if (villagers == null) return
        startGame(villagers!!, zombies!!)
    }

    fun startGameZombie(player: Player) {
        if (villagers == player) villagers = null
        zombies = player
        if (zombies == null) return
        startGame(villagers!!, zombies!!)
    }

    fun startGame(villagers: Player, zombies: Player) {
        val chess = ChessCollection.findOne() ?: return
        board = Board(this, chess.location.toLocation(), villagers, zombies)
    }

    fun forfeit(player: Player) {
        val winner = (if (player == villagers) zombies?.name else villagers?.name) ?: "Unknown"
        val loser = player.name
        broadcast(*MATCH_FORFEITED(winner, loser))
        clear()
    }

    fun clear(forced: Boolean = false){
        board?.end()
        board = null
        if (forced && villagers != null) villagers?.spigot()?.sendMessage(*ADMIN_CLEARED)
        if (forced && zombies != null) zombies?.spigot()?.sendMessage(*ADMIN_CLEARED)
        villagers = null
        zombies = null
    }

    fun enabled(bool: Boolean): Boolean{
        val config = getConfig() ?: return false
        config.enabled = bool
        if (updateConfig(config) == null) return false
        return true
    }

    private fun getConfig(): Chess? =
        ChessCollection.findOne()
    private fun updateConfig(chess: Chess): Chess? =
        ChessCollection.findOneAndReplace(EMPTY_BSON, chess, FindOneAndReplaceOptions().upsert(true))

    @EventHandler(ignoreCancelled = true)
    fun onSignInteract(event: PlayerInteractEvent) {
        val block = event.clickedBlock ?: return
        if (!block.isSign()) return
        val sign = block.state as Sign
        if (sign.getLine(0) != CHESS_SIGN_TITLE) return
        event.isCancelled = false
        if (board == null && sign.getLine(1) == CHESS_VILLAGER_SIGN) return startGameVillager(event.player)
        if (board == null && sign.getLine(1) == CHESS_ZOMBIE_SIGN) return startGameZombie(event.player)
        if (board!!.villagers == event.player) return forfeit(event.player)
        if (board!!.zombies == event.player) return forfeit(event.player)
    }

    @EventHandler(ignoreCancelled = true)
    fun onSignChange(event: SignChangeEvent){
        if (event.getLine(0) != CHESS_SIGN_TITLE) return
        if (event.player.hasPermission(ChessAdminPermission)) return
        event.isCancelled = true
        CannotCreateSign(event.player, ChessTag, CHESS_SIGN_TITLE)
    }

    @EventHandler(ignoreCancelled = true)
    fun onBlockBreak(event:BlockBreakEvent) {
        if (!event.block.isSign()) return
        val sign = event.block.state as Sign
        if (sign.getLine(0) != CHESS_SIGN_TITLE) return
        if (event.player.hasPermission(ChessAdminPermission)) return
        event.isCancelled = true
        CannotDestroySign(event.player, ChessTag, CHESS_SIGN_TITLE)
    }

    @EventHandler(ignoreCancelled = true)
    fun onPlayerQuit(event: PlayerQuitEvent) {
        if (board == null) return
        if (board!!.villagers == event.player || board!!.zombies == event.player) {
            forfeit(event.player)
            return
        }
    }
}