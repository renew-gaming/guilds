package com.github.snarechops.renew.features.deaths

import com.github.snarechops.renew.common.*
import kotlinx.serialization.Serializable
import org.bson.codecs.pojo.annotations.BsonId
import org.bukkit.inventory.ItemStack
import org.litote.kmongo.Id
import java.time.Instant

@Serializable
data class Death(
    @BsonId
    val id: Id<Death>,
    val uuid: String,
    val name: String,
    val inventory: String,
    val level: Int = 0,
    val location: Coordinate = Coordinate(0, 0, 0),
    val cause: String = "",
    val time: Long = Instant.now().toEpochMilli(),
)

@Serializable
data class Coordinate(val x: Int, val y: Int, val z: Int)

fun Death.displayItem(): ItemStack =
    this.inventory.decodeItemStacks().let { stacks ->
        CustomItem(
            material = stacks.first().type,
            name = Instant.ofEpochMilli(this.time).toString(),
            lore = listOf(
                "$darkGray${this.id}",
                "$gold${stacks.size} Items",
                "${green}Cause: ${this.cause}",
                "${green}Level: ${this.level}",
                "${this.location.x} ${this.location.y} ${this.location.z}"
            )
        )
    }