package com.github.snarechops.renew.features.quests

import com.github.snarechops.renew.*
import com.github.snarechops.renew.common.*
import com.github.snarechops.renew.features.books.Book
import com.github.snarechops.renew.features.books.ShowBookEvent
import com.github.snarechops.renew.features.guilds.GuildName
import com.mongodb.client.MongoCollection
import com.mongodb.client.model.FindOneAndReplaceOptions
import org.bukkit.block.Sign
import org.bukkit.entity.Player
import org.bukkit.event.EventHandler
import org.bukkit.event.EventPriority
import org.bukkit.event.HandlerList
import org.bukkit.event.Listener
import org.bukkit.event.block.BlockBreakEvent
import org.bukkit.event.block.SignChangeEvent
import org.bukkit.event.player.PlayerInteractEvent
import org.bukkit.event.player.PlayerJoinEvent
import org.bukkit.event.player.PlayerQuitEvent
import org.litote.kmongo.and
import org.litote.kmongo.eq
import org.litote.kmongo.match
import org.litote.kmongo.sample
import java.time.Instant

const val QUEST_SIGN_TITLE = "[Guild Quest]"
const val QUEST_DEPOSIT_SIGN = "[Quest Deposit]"
const val QuestsAdminPermission = "quests.admin"
val QuestsTag = pluginTag("Quests")

data class QuestCompletedEvent(val player: Player, val quest: ActiveQuest) : CustomEvent()
data class TaskCompletedEvent(val quest: ActiveQuest, val task: ActiveTask) : CustomEvent()

val QuestCollection: MongoCollection<Quest>
    get() = Database.getDatabase("guilds").getCollection("quest", Quest::class.java)

val ActiveQuestCollection: MongoCollection<PlayerQuest>
    get() = Database.getDatabase("guilds").getCollection("quest.player", PlayerQuest::class.java)

object QuestFeature: Feature, Listener {
    val active = mutableMapOf<Player, MutableMap<GuildName, ActiveQuest>>()

    override fun onInit() {
        QuestCommand.register("quest", QuestCompletion)
        this.register()
    }

    override fun onDestroy() {
        saveAllActiveQuests(active.values.flatMap { it.values })
    }

    fun getQuests(guildName: GuildName): List<Quest>? =
        QuestCollection.find(Quest::guild eq guildName).toList()

    fun getQuests(): List<Quest>? =
        QuestCollection.find().toList()

    private fun getRandomQuest(guildName: GuildName): Quest? =
        QuestCollection.aggregate(
            listOf(
                match(Quest::guild eq guildName),
                sample(1)
            )
        ).first()

    fun insertOneQuest(quest: Quest) =
        QuestCollection.insertOne(quest)

    private fun getActiveQuests(uuid: String): List<ActiveQuest> =
        ActiveQuestCollection.find(PlayerQuest::uuid eq uuid).toList().map { ActiveQuest(it) }

    fun getActiveQuests(): List<ActiveQuest> =
        ActiveQuestCollection.find().toList().map { ActiveQuest(it) }

    private fun saveAllActiveQuests(quests: List<ActiveQuest>) = quests.map { it.toPlayerQuest() }.forEach {
        ActiveQuestCollection.findOneAndReplace(
            and(PlayerQuest::uuid eq it.uuid, PlayerQuest::guild eq it.guild),
            it,
            FindOneAndReplaceOptions().upsert(true),
        )
    }

    @EventHandler(priority = EventPriority.LOW, ignoreCancelled = true)
    fun onJoin(event: PlayerJoinEvent) =
        getActiveQuests(event.player.uniqueId.toString())
            .forEach { registerQuest(it) }

    @EventHandler(priority = EventPriority.LOW, ignoreCancelled = true)
    fun onQuit(event: PlayerQuitEvent) =
        if (active.containsKey(event.player)) {
            saveAllActiveQuests(active[event.player]!!.values.toList())
            active.remove(event.player)
        } else null

    @EventHandler(ignoreCancelled = true)
    fun onQuestComplete(event: QuestCompletedEvent) {
        unregisterQuest(event.quest)
    }

    @EventHandler(ignoreCancelled = true)
    fun onRequestQuest(event: PlayerInteractEvent) {
        val block = event.clickedBlock ?: return
        if (!block.isSign()) return
        val sign = block.state as Sign
        if (sign.getLine(0) != QUEST_SIGN_TITLE) return
        val guild = GuildName.valueOf(sign.getLine(1).uppercase())
        val existing = active[event.player]?.get(guild) ?: return assignNewQuest(event.player, guild)
        if (existing.isExpired) abandonQuest(event.player, existing)
        else err(event.player, QuestsTag, red("You already have an active"), *guild.toString().toProperCase().colorize(), red("quest"))
    }

    @EventHandler(ignoreCancelled = true)
    fun onPreventSignBreak(event: BlockBreakEvent) {
        if (!event.block.isSign()) return
        val sign = event.block.state as Sign
        if (!listOf(QUEST_SIGN_TITLE, QUEST_DEPOSIT_SIGN).contains(sign.getLine(0))) return
        if (event.player.hasPermission(QuestsAdminPermission)) return
        event.isCancelled = true
    }

    @EventHandler(ignoreCancelled = true)
    fun onPreventSignCreate(event: SignChangeEvent){
        if (!listOf(QUEST_SIGN_TITLE, QUEST_DEPOSIT_SIGN).contains(event.getLine(0))) return
        if (event.player.hasPermission(QuestsAdminPermission)) return
        event.isCancelled = true
    }

    fun showQuests(player: Player) {
        val actives = active[player] ?: return
        val pages = actives.values.map { it.bookPage() }
        emit(ShowBookEvent(player, Book(player.name, "Guild Quests", pages)))
    }

    fun showQuest(player: Player, guildName: GuildName, target: Player = player) {
        val actives = active[target] ?: return
        val quest = actives[guildName] ?: return
        emit(ShowBookEvent(player, Book(target.name, "Guild Quest", listOf(quest.bookPage()))))
    }

    fun creditQuest(player: Player, guildName: GuildName): ActiveQuest? {
        val quests = active[player] ?: return null
        val quest = quests[guildName] ?: return null
        quest.tasks.forEach { it.credit(it.total - it.progress) }
        return quest
    }

    fun expireQuest(player: Player, guildName: GuildName): ActiveQuest? {
        val quests = active[player] ?: return null
        val quest = quests[guildName] ?: return null
        quest.expires = Instant.now()
        return quest
    }

    private fun registerQuest(quest: ActiveQuest): ActiveQuest {
        if (!active.containsKey(quest.player)) active[quest.player] = mutableMapOf(quest.guild to quest)
        else active[quest.player]!![quest.guild] = quest
        quest.register("Registering ${quest.name} for ${quest.player.name}")
        quest.tasks.forEach { it.register() }
        return quest
    }

    private fun unregisterQuest(quest: ActiveQuest) {
        if (!active.containsKey(quest.player)) return
        active[quest.player]!!.remove(quest.guild)
        HandlerList.unregisterAll(quest)
    }

    private fun assignNewQuest(player: Player, guildName: GuildName) = getRandomQuest(guildName).let {
        debug("Assigning new quest $guildName to ${player.name}")
        if (it == null) err(player, QuestsTag, yellow("No current"), gold(guildName.toString().toProperCase()), yellow("quests available. Check back later!"))
        else {
            val quest = registerQuest(ActiveQuest(player, it))
            emit(ShowBookEvent(player, Book("Guilds", "Quests", listOf(quest.bookPage()))))
        }
        Unit
    }

    private fun abandonQuest(player: Player, quest: ActiveQuest) {
        debug("Abandoning quest ${quest.name} for ${player.name}")
        if (quest.canAbandon) {
            unregisterQuest(quest)
            ok(player, QuestsTag, "Quest ${quest.name} has been abandoned")
        }
        else {
            ok(player, QuestsTag, yellow("To abandon ${quest.name}, click the sign again."))
            quest.timeout = Instant.now().plusSeconds(10)
        }
    }
}