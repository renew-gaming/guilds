package com.github.snarechops.renew

import com.github.snarechops.renew.common.connectDatabase
import com.github.snarechops.renew.common.fetchConfig
import com.github.snarechops.renew.features.announcements.AnnouncementFeature
import com.github.snarechops.renew.features.books.BookFeature
import com.github.snarechops.renew.features.chess.ChessFeature
import com.github.snarechops.renew.features.commands.CommandFeature
import com.github.snarechops.renew.features.crates.CratesFeature
import com.github.snarechops.renew.features.cultist.CultistFeature
import com.github.snarechops.renew.features.cultshop.CultShopFeature
import com.github.snarechops.renew.features.deaths.DeathFeature
import com.github.snarechops.renew.features.enchantments.EnchantmentFeature
import com.github.snarechops.renew.features.guilds.GuildFeature
import com.github.snarechops.renew.features.hide.HideFeature
import com.github.snarechops.renew.features.locker.LockerFeature
import com.github.snarechops.renew.features.preferences.PreferenceFeature
import com.github.snarechops.renew.features.quests.QuestFeature
import com.github.snarechops.renew.features.recipes.RecipeFeature
import com.github.snarechops.renew.features.reports.ReportFeature
import com.github.snarechops.renew.features.feats.FeatsFeature
import com.github.snarechops.renew.features.stats.StatsFeature
import com.github.snarechops.renew.features.upgradetokens.UpgradeTokenFeature
import com.github.snarechops.renew.features.valentines.ValentinesFeature
import org.bukkit.plugin.java.JavaPlugin
import org.bukkit.Bukkit
import org.bukkit.event.HandlerList
import org.bukkit.event.Listener
import org.bukkit.plugin.Plugin
import java.util.logging.Level

const val PLUGIN_FOLDER = "plugins/Renew"

lateinit var Renew: RenewPlugin

class RenewPlugin : JavaPlugin(), Listener {
    // features
    lateinit var features: List<Feature>

    override fun onEnable() {
        Renew = this
        Bukkit.getLogger().log(Level.INFO, "Starting enable for Renew")
        System.setProperty("org.litote.mongo.test.mapping.service", "org.litote.kmongo.jackson.JacksonClassMappingTypeService")

        // Setup Config
        if (!fetchConfig()){
            isEnabled = false
            return
        }
        // Setup Database
        if (!connectDatabase()){
            isEnabled = false
            return
        }

        features = listOf(
            AnnouncementFeature,
            BookFeature,
            ChessFeature,
            CommandFeature,
            CratesFeature,
            CultistFeature,
            CultShopFeature,
            DeathFeature,
            EnchantmentFeature,
            GuildFeature,
            HideFeature,
            LockerFeature,
            PreferenceFeature,
            QuestFeature,
            RecipeFeature,
            ReportFeature,
            UpgradeTokenFeature,
            FeatsFeature,
            StatsFeature,
            ValentinesFeature,
        )
        features.init()
        this.isEnabled = true
    }

    override fun onDisable(){
        HandlerList.unregisterAll(this as Plugin)
        features.destroy()
    }
}
