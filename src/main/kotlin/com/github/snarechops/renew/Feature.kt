package com.github.snarechops.renew

interface Feature {
    fun onInit(): Unit
    fun onDestroy(): Unit
}

fun List<Feature>.init() =
    this.forEach { it.onInit() }

fun List<Feature>.destroy() =
    this.forEach { it.onDestroy() }