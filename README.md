# Guilds

## Commands

| Command | Description | Permission | Example |
|---|---|---|---|
| `/bloodmoon show` | Shows the Blood Moon UI and sounds | none | `/bloodmoon show` |
| `/bloodmoon hide` | Hides the Blood Moon UI and sounds | none | `/bloodmoon hide` |
| `/bloodmoon on` | Turns on the Blood Moon features | bloodmoon.manage | `/bloodmoon on` |
| `/bloodmoon off` | Turns off the Blood Moon features | bloodmoon.manage | `/bloodmoon off` |
| `/bloodmoon always` | Switches the Blood Moon into "every night" mode | bloodmoon.manage | `/bloodmoon always` |
| `/bloodmoon fullmoon` | Switches the Blood Moon to "full moon only" mode | bloodmoon.manage | `/bloodmoon fullmoon` |
| `/bloodmoon difficulty <#>` | Changes the difficulty of blood moons | bloodmoon.manage | `/bloodmoon difficulty 1` |
| `/cult give <item> <player> <amount>` | Gives the player cultist items | cult.give | `/cult give coins SnareChops 25` |
| `/token <type> <player>` | Gives the player an upgrade token | guilds.token.give | `/token fortune SnareChops` |

[comment]: <> (Spawner Pickaxe)

[comment]: <> (Havesting Hoe)
Smelter Pickaxe
Tunnel Pickaxe
Waterbreather Helmet
Nightvision Helmet
Saturation Chest
Jump Boots?
Speed Boots?
Lightning Axe
Stripping Axe
Plowing Shovel
